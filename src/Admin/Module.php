<?php

namespace Simpco\Admin;

use Simpco\Admin\Console\CompileFrontCommand;
use Simpco\Admin\Console\CreateUserCommand;
use Simpco\Admin\Models\Repository\UserRepository;
use Simpco\Admin\Models\User;
use Simpco\Admin\Routes\Dashboard;
use Simpco\Utils\Routes\ModifyDefinition;
use Simpco\Admin\Routes\Login;
use Simpco\Admin\Routes\Post\Login as PostLogin;
use Simpco\Admin\Routes\User\Grid;
use Simpco\Admin\Security\Authentication;
use Simpco\Admin\Security\AuthenticationInterface;
use Simpco\Admin\Security\Firewall;
use Simpco\Admin\Security\PasswordHash;
use Simpco\Admin\Security\PasswordHashInterface;
use Simpco\Admin\Security\UserTokenFactory;
use Simpco\Console\CommandPoolInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifier\ArrayPush;
use Simpco\Framework\DependencyInjection\ServiceFactory;
use Simpco\Framework\FrameworkModule;
use Simpco\Framework\Models\RepositoryFactory;
use Simpco\Framework\Module\EmptyModuleTrait;
use Simpco\Framework\ModuleInterface;
use Simpco\Framework\RequestInterface;
use Simpco\Framework\RouterInterface;
use Simpco\Framework\View\HeadTag\ScriptTag;
use Simpco\Framework\View\HeadTagPoolInterface;
use Simpco\MithrilIntegration\Module as MithrilIntegrationModule;
use Simpco\Security\Hash\HS256;

class Module implements ModuleInterface
{
    use EmptyModuleTrait;

    public function getServices(): array
    {
        return [
            ...ServiceFactory::bashFactory(
                require_once __DIR__ . '/config/user_grid.php'
            ),
            ServiceFactory::createFactory(
                PasswordHash::class,
                [
                    HS256::class,
                    '%app.security.hash.secret%'
                ],
                PasswordHashInterface::class
            ),
            ServiceFactory::createFactory(
                UserTokenFactory::class,
                ['%app.security.hash.secret%']
            ),
            ServiceFactory::createFactory(
                Authentication::class,
                [],
                AuthenticationInterface::class
            ),
            RepositoryFactory::createFactory(
                UserRepository::class,
                User::class
            )
        ];
    }

    public function getRoutes(): array
    {
        return [
            RequestInterface::GET => ModifyDefinition::prefix(
                '/admin',
                [
                    '/login' => Login::class,
                    '/' => Dashboard::class,
                    '/users' => Grid::class
                ]
            ),
            RequestInterface::POST => ModifyDefinition::prefix(
                '/admin',
                [
                    '/login' => PostLogin::class
                ]
            )
        ];
    }

    public function getModelsDir(): ?string
    {
        return __DIR__ . '/Models';
    }

    public function getViewDir(): ?string
    {
        return __DIR__ . '/view';
    }

    public function getDependencies(): array
    {
        return [
            FrameworkModule::class,
            MithrilIntegrationModule::class
        ];
    }

    public function getServiceModifiers(): array
    {
        return [
            HeadTagPoolInterface::class => [
                ArrayPush::create('headTags', [
                    ScriptTag::createForLocalFile('toinclude/admin.js')
                ])
            ],
            RouterInterface::class => [
                ArrayPush::create('routeModifiers', [
                    Firewall::class
                ])
            ],
            CommandPoolInterface::class => [
                ArrayPush::create('commands', [
                    CreateUserCommand::class,
                    CompileFrontCommand::class
                ])
            ]
        ];
    }
}
