<?php

namespace Simpco\Admin\Routes\Post;

use Simpco\Admin\Models\Repository\UserRepository;
use Simpco\Admin\Models\User;
use Simpco\Admin\Security\PasswordHashInterface;
use Simpco\Admin\Security\UserTokenFactory;
use Simpco\Framework\RequestInterface;
use Simpco\Framework\ResponseInterface;
use Simpco\Framework\SessionInterface;
use Simpco\Framework\ViewFactory;
use Simpco\MithrilIntegration\Route\MithrilRoute;

class Login extends MithrilRoute
{
    private PasswordHashInterface $passwordHashInterface;

    private UserTokenFactory $userTokenFactory;

    private SessionInterface $session;

    private UserRepository $userRepository;

    public function __construct(
        RequestInterface $request,
        ViewFactory $viewFactory,
        ResponseInterface $response,
        PasswordHashInterface $passwordHashInterface,
        UserTokenFactory $userTokenFactory,
        SessionInterface $session,
        UserRepository $userRepository
    ) {
        $this->passwordHashInterface = $passwordHashInterface;
        $this->userTokenFactory = $userTokenFactory;
        $this->session = $session;
        $this->userRepository = $userRepository;
        parent::__construct($request, $viewFactory, $response);
    }

    public function getData(): array
    {
        $username = $this->request->getBody()->getData('username');
        $password = $this->request->getBody()->getData('password');

        $repo = $this->userRepository;

        $user = $repo->findOneByUsername($username);

        if (!$user instanceof User) {
            return [
                'success' => false,
                'message' => 'User could not be found'
            ];
        }

        if (!$this->passwordHashInterface->isValid($user->getPassword(), $password)) {
            return [
                'success' => false,
                'message' => 'Wrong password'
            ];
        }

        $token = $this->userTokenFactory->getTokenFromUser($user);
        $this->session->set('admin.token', $token->__toString());

        return [
            'success' => true,
            'message' => 'Successfully logged in'
        ];
    }
}
