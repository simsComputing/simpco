<?php

namespace Simpco\Admin\Routes\User;

use Simpco\Framework\RequestInterface;
use Simpco\Framework\ResponseInterface;
use Simpco\Framework\ViewFactory;
use Simpco\Grid\Grid as GridModel;
use Simpco\MithrilIntegration\Route\MithrilRoute;

class Grid extends MithrilRoute
{
    private GridModel $userGridModel;

    public function __construct(
        RequestInterface $request,
        ViewFactory $viewFactory,
        ResponseInterface $response,
        GridModel $userGridModel
    ) {
        $this->userGridModel = $userGridModel;
        parent::__construct($request, $viewFactory, $response);
    }

    public function getData(): array
    {
        return [
            'grid' => $this->userGridModel->toArray()
        ];
    }
}
