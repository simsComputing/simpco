<?php

namespace Simpco\Admin\Routes;
use Simpco\MithrilIntegration\Route\MithrilRoute;

class Dashboard extends MithrilRoute
{
    public function getData(): array
    {
        return ['ok' => 'ok'];
    }
}
