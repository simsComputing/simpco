import { registerMenuConfig, getFinalMenuConfig, clearMenuConfig } from "../menuConfig";

const config =
    {text: 'Test', items: [{text: 'Produits', href: '#'}]}

test('Registration does not throw an error', () => {
    clearMenuConfig();
    expect(() => registerMenuConfig('Test', config, 10, null)).not.toThrow();
});

test('Final config has proper formatting', () => {
    clearMenuConfig();
    registerMenuConfig('Test', config, 10, null);
    registerMenuConfig('Test.subtest', {text: 'Caca', href:'/caca'}, 10, 'Test');
    const menuConfig = getFinalMenuConfig();

    expect(menuConfig[0].text).toBe('Test');
    expect(menuConfig[0].items.length).toBe(1);
});
