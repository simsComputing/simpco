let menuConfig = [];

const registerMenuConfig = (id, config, sortOrder, parentId = null) => {
    menuConfig.push({id, config, sortOrder, parentId});
}

const getFinalMenuConfig = () => {
    const parentsConfig = menuConfig.filter(config => config.parentId === null);
    parentsConfig.sort(compareMenuItems);
    parentsConfig.forEach(createMenuTree);
    const finalConfig = parentsConfig.map(formatTreeForDisplay);
    return finalConfig;
}

const formatTreeForDisplay = ({config, items, ...rest}) => {
    items = items.map(formatTreeForDisplay);
    return {
        ...config,
        items
    }
}

const createMenuTree = parentItem => {
    parentItem.items = [];
    menuConfig.forEach(config => parentItem.id === config.parentId ? parentItem.items.push(config) : null);
    parentItem.items.sort(compareMenuItems);
    parentItem.items.forEach(createMenuTree);
}

const compareMenuItems = (a, b) => a.sortOrder < b.sortOrder ? -1 : 1;

const clearMenuConfig = () => {menuConfig = []};

export {
    getFinalMenuConfig,
    registerMenuConfig,
    clearMenuConfig
}
