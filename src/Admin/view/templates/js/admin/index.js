import './routes';
import './blocks';
import './index.scss';
import './base.scss';

import { simpcoRegisterDefaultRoute } from 'simpco-mithril/base';

simpcoRegisterDefaultRoute('/admin');
