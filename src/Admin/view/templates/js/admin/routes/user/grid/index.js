import { simpcoRegisterBlock, simpcoRegisterRoute } from 'simpco-mithril/base';
import UserGrid from './grid';
import { registerMenuConfig } from 'simpco-admin/menuConfig';

simpcoRegisterBlock('simpco.admin.route.user.grid', UserGrid);
simpcoRegisterRoute('/admin/users', 'simpco.admin.route.user.grid');
registerMenuConfig(
  'admin.configuration.users',
  {
    text: 'Users',
    href: '/admin/users',
  },
  20,
  'admin.configuration'
);
