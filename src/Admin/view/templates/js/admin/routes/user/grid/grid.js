import { m } from 'mithril';
import { simpcoDisplayBlock } from 'simpco-mithril/base';

const UserGrid = () => {
  return {
    view: ({ attrs: { grid } }) => {
      return simpcoDisplayBlock('admin.organism.layout', {
        content: [
          m('div', 'Tu veux ajouter un user baby ?'),
          simpcoDisplayBlock('admin.grid', { grid }),
        ],
      });
    },
  };
};

export default UserGrid;
