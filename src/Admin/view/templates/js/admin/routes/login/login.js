var m = require('mithril');
import { simpcoDisplayBlock } from 'simpco-mithril/base';

const LoginComponent = (vnode) => {
    return {
        view: (vnode) => {
            const {loginResult, onsubmit} = vnode.attrs;

            return m('div', {class: 'login-container relative'}, [
                m('div', {class: 'full-center'}, [
                    m('div', {class: 'login-container__title text-center'}, simpcoDisplayBlock('admin.atom.logo', {width: 50, height: 50})),
                    m('div', {class: 'login'}, [
                        m('form', {class: 'pure-form pure-form-stacked full-center login__form', onsubmit}, [
                            m('fieldset', {}, [
                                m('legend', {}, 'Connexion admin'),
                                simpcoDisplayBlock('admin.atom.input.text', {display: 'secondary', placeholder: 'Username', name: "username"}),
                                simpcoDisplayBlock('admin.atom.input.password', {display: 'secondary', placeholder: 'Password', name: "password"}),
                                simpcoDisplayBlock('admin.atom.button', {type: 'secondary', text: 'Connexion'}),
                                loginResult === null || loginResult.success !== false ? null : m('div', {}, loginResult.message)
                            ])
                        ])
                    ])
                ]),
            ])
        } 
    }
}

export default LoginComponent;