const makeLoginAction = (request) => (event) => {
    event.preventDefault();
    const data = new FormData(event.target);
    return request({
        method: 'POST',
        url: '/admin/login',
        body: {
            username: data.get('username'),
            password: data.get('password')
        },
        headers: {
            'Content-Type': 'application/json'
        }
    });
}

export default makeLoginAction;