const m = require('mithril');
import {route, request} from 'mithril';
import { simpcoRegisterBlock, simpcoRegisterRoute } from "simpco-mithril/base";
import LoginComponent from "./login";
import makeUseLogin from "./useLogin";
import makeLoginAction from './loginAction';

const useLogin = makeUseLogin(m, route, makeLoginAction(request));

simpcoRegisterBlock('admin.login', useLogin(LoginComponent));
simpcoRegisterRoute('/admin/login', 'admin.login');
