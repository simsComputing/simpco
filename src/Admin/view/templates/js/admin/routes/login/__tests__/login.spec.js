/**
 * @jest-environment jsdom
 */

import LoginComponent from "../login";
import {mithrilComponentDependencyDeclaration, makeComponentTest} from "simpco-mithril/testutils";

mithrilComponentDependencyDeclaration('admin.atom.input.text');
mithrilComponentDependencyDeclaration('admin.atom.button');
mithrilComponentDependencyDeclaration('admin.atom.input.password'); 
mithrilComponentDependencyDeclaration('admin.atom.logo');

makeComponentTest("Login component", LoginComponent, {loginResult: null, onSubmit: () => {}});