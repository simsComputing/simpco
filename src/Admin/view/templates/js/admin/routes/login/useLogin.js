const makeUseLogin = (m, route, loginAction) => (LoginComponent) => ({
    data: {loginResult: null},
    view: (vnode) => {
        const onsubmit = (e) => loginAction(e)
            .then(result => result.success === true ? route.set('/admin') : result)
            .then(result => vnode.state.data = result || vnode);

            return m(LoginComponent, {loginResult: vnode.state.data, onsubmit});
    }
})

export default makeUseLogin;
