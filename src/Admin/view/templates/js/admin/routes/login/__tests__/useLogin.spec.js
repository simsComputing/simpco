import {makeMMock, makeRouteMock} from 'simpco-mithril/testutils';
import makeUseLogin from '../useLogin';
import LoginComponent from '../login';

test('useLogin component with result success', () => {
    const emptyVnode = {
        state: {
            data: {loginResult: null}
        }
    }
    const loginActionMock = () => Promise.resolve({success: true, message: "ok"});
    const route = makeRouteMock();
    const m = makeMMock((displayComponent, props) => {
        props.onsubmit().then(() => {
            expect(route.set).toHaveBeenCalledTimes(1);
            expect(route.set).toHaveBeenCalledWith('/admin');
        });
    })
    const useLogin = makeUseLogin(m, route, loginActionMock);
    useLogin(LoginComponent).view(emptyVnode);
})

test('useLogin component with result unsuccessful', () => {
    const emptyVnode = {
        state: {
            data: {loginResult: null}
        }
    }
    const loginActionMock = () => Promise.resolve({success: false, message: "notok"});
    const route = makeRouteMock();
    const m = makeMMock((displayComponent, props) => {
        props.onsubmit().then(() => {
            expect(route.set).not.toHaveBeenCalled();
            expect(emptyVnode.state.data.success).toEqual(false);
            expect(emptyVnode.state.data.message).toEqual('notok');
        });
    })
    const useLogin = makeUseLogin(m, route, loginActionMock);
    useLogin(LoginComponent).view(emptyVnode);
})