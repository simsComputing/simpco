import {m} from 'mithril';
import { simpcoRegisterBlock, simpcoRegisterRoute, simpcoDisplayBlock } from 'simpco-mithril/base';

const DashboardComponent = {
    view: (vnode) => simpcoDisplayBlock('admin.organism.layout', {content: [m('div', 'Dashboard')]})
}
simpcoRegisterBlock('admin.dashboard', DashboardComponent);
simpcoRegisterRoute('/admin', 'admin.dashboard');
