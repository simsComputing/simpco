import {m} from 'mithril';
import { simpcoRegisterBlock, simpcoDisplayBlock } from 'simpco-mithril/base';

const MithrilLoading = {
    view: () => m('div', {class: 'relative fvh'}, 
        m('div', {class: 'full-center'}, 
            [simpcoDisplayBlock('admin.atom.spinner', {})]))
}

simpcoRegisterBlock('mithril.loading', MithrilLoading);