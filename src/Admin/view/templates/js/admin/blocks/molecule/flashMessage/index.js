import { simpcoRegisterBlock } from 'simpco-mithril/base';
import FlashMessageComponent from './flashMessage';
import './flashMessage.scss';

simpcoRegisterBlock('admin.molecule.flashMessage', FlashMessageComponent);
