import { m } from 'mithril';

const formatFlashMessageClass = (type) => {
  let cls =
    typeof type === 'string'
      ? '.flash-message.flash-message--' + type
      : '.flash-message';

  cls += '.fw';

  return cls;
};

const FlashMessageComponent = () => {
  return {
    view: (vnode) =>
      m(formatFlashMessageClass(vnode.attrs.type), vnode.attrs.text),
  };
};

export default FlashMessageComponent;
