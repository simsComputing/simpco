import { m } from 'mithril';
import { simpcoDisplayBlock, simpcoRegisterBlock } from 'simpco-mithril/base';
import { defaultValue } from 'simpco-mithril/utils';

const Fieldset = {
  view: (vnode) => {
    const inputProps = defaultValue(vnode.attrs.inputProps, {});
    return m('fiedlset.fiedlset.pure-g', [
      m(
        '.pure-u-1-1',
        simpcoDisplayBlock('admin.atom.input.label', {
          text: vnode.attrs.label,
        })
      ),
      m(
        '.pure-u-1-1',
        simpcoDisplayBlock('admin.atom.input.' + vnode.attrs.input, inputProps)
      ),
    ]);
  },
};

simpcoRegisterBlock('admin.molecule.fieldset', Fieldset);
