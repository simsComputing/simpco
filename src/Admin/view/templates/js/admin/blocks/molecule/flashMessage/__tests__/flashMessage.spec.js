/**
 * @jest-environment jsdom
 */

import { makeComponentTest } from 'simpco-mithril/testutils';
import FlashMessageComponent from '../flashMessage';

makeComponentTest('Flash Message With Type', FlashMessageComponent, {
  type: 'success',
  text: 'Félicitations !',
});
