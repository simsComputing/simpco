import {m} from 'mithril';
import { simpcoDisplayBlock, simpcoRegisterBlock } from 'simpco-mithril/base';
import { getFinalMenuConfig } from '../../../../menuConfig';

import './layout.scss';

const LayoutComponent = {
    view: (vnode) => {
        const menuConfig = getFinalMenuConfig();
        return m('div', {class: 'admin-layout'}, [
            m('div', {class: 'admin-layout__container--top'}, [
                simpcoDisplayBlock('admin.organism.navbar')
            ]),
            m('div', {class: 'admin-layout__container--bottom pure-grid'}, [
                m('div', {class: 'pure-u-4-24'}, [simpcoDisplayBlock('admin.organism.menu', {name: 'Simpco', items: menuConfig})]),
                m('div', {class: 'pure-u-20-24'}, m('div', {class: 'admin-page-content'}, vnode.attrs.content))
            ]),
        ])
    }
}

simpcoRegisterBlock('admin.organism.layout', LayoutComponent);