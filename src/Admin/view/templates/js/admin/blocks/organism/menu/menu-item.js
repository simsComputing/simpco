import { simpcoRegisterBlock } from "simpco-mithril/base";
import {m, route} from 'mithril';

const MenuItem = {
    view: (vnode) => {
        const subMenuItems = vnode.attrs.items.map(item => m('div', {class: 'pure-menu__submenu__item'}, m(route.Link, {href: item.href}, item.text)));
        return m('li', {class: 'pure-menu-item'}, [
            m('a', {class: 'pure-menu-link', href:"#"}, vnode.attrs.text),
            m('div', {class: 'pure-menu__submenu'}, subMenuItems)
        ]);
    }
}

simpcoRegisterBlock('admin.organism.menu.item', MenuItem);