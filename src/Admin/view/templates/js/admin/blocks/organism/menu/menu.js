import {m} from 'mithril';
import { simpcoDisplayBlock, simpcoRegisterBlock } from 'simpco-mithril/base';
import './menu.scss';

const MenuComponent = {
    view: (vnode) => {
        const items = vnode.attrs.items.map(item => simpcoDisplayBlock('admin.organism.menu.item', item));

        return m('div', {class: 'pure-menu admin-menu'}, [
            m('div', {class: 'pure-menu-heading'}, vnode.attrs.name),
            m('ul', {class: 'pure-menu-list'}, items)
        ]);
    }
}

simpcoRegisterBlock('admin.organism.menu', MenuComponent);