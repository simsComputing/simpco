import {m} from 'mithril';
import { simpcoDisplayBlock, simpcoRegisterBlock } from 'simpco-mithril/base';
import './navbar.scss';

const NavBarComponent = {
    view: () => m('div', {class: 'admin-navbar pure-grid'}, [
        m('div', {class: 'admin-navbar__logo pure-u-4-24'}, simpcoDisplayBlock('admin.atom.logo', {width: 50, height: 50})),
        m('div', {class: 'pure-u-20-24'}, m('div', {class: 'pure-menu pure-menu-horizontal'}, ))
    ])
}

simpcoRegisterBlock('admin.organism.navbar', NavBarComponent);