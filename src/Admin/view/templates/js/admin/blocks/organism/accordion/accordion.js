import {m} from 'mithril';
import { simpcoRegisterBlock } from "simpco-mithril/base"
import './accordion.scss';

const Accordion = (initialVnode) => {
    var showContent = false;
    const getChevronDirection = () => showContent ? 'down' : 'right';
    return {
        view: (vnode) => {
            return m('div', {class: "accordion pure-g"}, [
                    m('div', {class: 'pure-u-5-5'}, 
                        m('div', {class: 'accordion__title pure-g', onclick: () => showContent = !showContent}, [
                            m('div', {class: 'pure-u-4-5 h3'}, vnode.attrs.title),
                            m('div', {class: 'pure-u-1-5'}, 
                                m('div', {class: 'gg-push-chevron-' + getChevronDirection() + ' floatr'})
                            )
                        ])
                    ),
                    showContent ? m('div', {class: 'pure-u-5-5'}, m('div', {class: 'accordion__content'}, vnode.attrs.content)) : null,
                ])
            }
    }
}

simpcoRegisterBlock('admin.organism.accordion', Accordion)