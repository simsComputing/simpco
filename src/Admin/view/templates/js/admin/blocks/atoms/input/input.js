var m = require('mithril');
import { simpcoRegisterBlock } from 'simpco-mithril/base';

const InputComponent = {
  view: (vnode) => {
    const displayType = vnode.attrs.display;
    let inputClass = '';
    if (displayType) {
      inputClass = 'input-' + displayType;
    }

    return m('input', {
      class: inputClass,
      type: vnode.attrs.type,
      placeholder: vnode.attrs.placeholder,
      value: vnode.attrs.value,
      name: vnode.attrs.name,
    });
  },
};

simpcoRegisterBlock('admin.atom.input', InputComponent);
