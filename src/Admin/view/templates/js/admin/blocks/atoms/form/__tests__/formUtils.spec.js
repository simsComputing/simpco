import { getDataAsObject } from '../formUtils';

const makeFormDataStub = (obj) => ({
  entries: () => Object.entries(obj),
});

test('Test with form that has array values', () => {
  const formData = {
    'filter[name]': 'Man',
    'filter[type]': 'Simple',
  };

  const finalObj = getDataAsObject(makeFormDataStub(formData));
  console.dir(finalObj);
  expect(finalObj).toHaveProperty('filter.name', 'Man');
  expect(finalObj).toHaveProperty('filter.type', 'Simple');
});
