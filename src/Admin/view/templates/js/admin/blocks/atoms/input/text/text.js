import { simpcoDisplayBlock, simpcoRegisterBlock } from 'simpco-mithril/base';

const TextInput = {
    view: (vnode) => {
        return simpcoDisplayBlock('admin.atom.input', {...vnode.attrs, type: 'text'});
    }
}

simpcoRegisterBlock('admin.atom.input.text', TextInput);