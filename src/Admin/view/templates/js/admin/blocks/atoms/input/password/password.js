import { simpcoDisplayBlock, simpcoRegisterBlock } from 'simpco-mithril/base';

const PasswordInput = {
    view: (vnode) => {
        return simpcoDisplayBlock('admin.atom.input', {...vnode.attrs, type: 'password'});
    }
}

simpcoRegisterBlock('admin.atom.input.password', PasswordInput);