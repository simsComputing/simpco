const makeUseForm =
  (m, makeFormData, post, getDataAsObject) => (FormComponent) => {
    let loadingForm = false;
    const onsubmit = (event) => {
      loadingForm = true;
      event.preventDefault();
      const data = getDataAsObject(makeFormData(event.srcElement));
      post(data).then(() => {
        loadingForm = false;
        return loadingForm;
      });
      return loadingForm;
    };
    return {
      view: (vnode) => {
        return m(FormComponent, { loadingForm, onsubmit }, vnode.children);
      },
    };
  };

export default makeUseForm;
