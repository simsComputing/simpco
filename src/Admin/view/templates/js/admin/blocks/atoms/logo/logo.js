var m = require('mithril');
import { simpcoRegisterBlock } from 'simpco-mithril/base';

const LogoComponent = {
    view: (vnode) => {
        return m('img', {src: '/pub/media/simpco.jpeg', width: vnode.attrs.width, height: vnode.attrs.height, class: 'logo'})
    }
}

simpcoRegisterBlock('admin.atom.logo', LogoComponent);