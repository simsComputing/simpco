import { simpcoRegisterBlock } from 'simpco-mithril/base';
import Button from './button';
import './button.scss';

simpcoRegisterBlock('admin.atom.button', Button);
