import {m} from 'mithril';
import { simpcoRegisterBlock } from 'simpco-mithril/base';

const FormLabel = {
    view: (vnode) => m('label', {}, vnode.attrs.text)
}

simpcoRegisterBlock('admin.atom.input.label', FormLabel);