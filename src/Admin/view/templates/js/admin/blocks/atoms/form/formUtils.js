const getDataAsObject = (formData) => {
  let finalObj = {};
  for (const keyValuePair of formData.entries()) {
    finalObj = mapKeyValueOnOriginalArray(
      keyValuePair[0],
      keyValuePair[1],
      finalObj
    );
  }
  return finalObj;
};

const mapKeyValueOnOriginalArray = (key, finalValue, originalArray) => {
  const matches = key.match(/\w+|\d+\[\w+|\d+\]/g);
  let currentArray = originalArray;
  matches.forEach((match, index) => {
    if (index === matches.length - 1) {
      currentArray[match] = finalValue;
    } else if (typeof currentArray[match] === 'undefined') {
      currentArray[match] = {};
    }
    currentArray = currentArray[match];
  });
  return originalArray;
};

export { getDataAsObject };
