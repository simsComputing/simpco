import { m } from 'mithril';
import { simpcoRegisterBlock } from 'simpco-mithril/base';
import FormComponent from './form';
import makeUseForm from './useForm';
import { getDataAsObject } from './formUtils';

const useForm = makeUseForm(
  m,
  (srcElement) => {
    return new FormData(srcElement);
  },
  (...args) => window.simpco.post(...args),
  getDataAsObject
);

simpcoRegisterBlock('admin.atom.form', useForm(FormComponent));
