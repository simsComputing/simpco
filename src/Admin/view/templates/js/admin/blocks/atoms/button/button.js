const m = require('mithril');

const Button = {
    view: (vnode) => {
        const buttonType = vnode.attrs.type;
        const elementClasses = ['pure-button'];
        if (buttonType) {
            elementClasses.push('pure-button-' + buttonType);
        }
        return m('button', {class: elementClasses.join(' ')}, vnode.attrs.text)
    }
}

export default Button;
