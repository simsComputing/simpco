import {m} from 'mithril';
import { simpcoRegisterBlock } from 'simpco-mithril/base';
import './spinner.scss';

const Spinner = {
    view: () => m('div', {class: 'lds-dual-ring'})
}

simpcoRegisterBlock('admin.atom.spinner', Spinner);