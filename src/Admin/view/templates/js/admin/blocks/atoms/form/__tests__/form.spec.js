/**
 * @jest-environment jsdom
 */

import {
  makeComponentTest,
  mithrilComponentDependencyDeclaration,
} from 'simpco-mithril/testutils';
import FormComponent from '../form';

mithrilComponentDependencyDeclaration('admin.atom.spinner');

makeComponentTest('Form not loading', FormComponent, {
  loadingForm: false,
  onsubmit: () => {},
});

makeComponentTest('Form loading', FormComponent, {
  loadingForm: true,
  onsubmit: () => {},
});
