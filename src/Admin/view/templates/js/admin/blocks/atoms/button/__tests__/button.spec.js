/**
 * @jest-environment jsdom
 */

import {makeComponentTest} from "simpco-mithril/testutils";
import ButtonComponent from '../button';

makeComponentTest('Button Component', ButtonComponent, {type: 'primary', text: 'Test button'});
makeComponentTest('Button Component No Type', ButtonComponent, {text: 'Test button'});