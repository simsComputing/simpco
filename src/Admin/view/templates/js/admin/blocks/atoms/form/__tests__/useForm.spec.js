import { makeMMock } from 'simpco-mithril/testutils';
import FormComponent from '../form';
import makeUseForm from '../useForm';

const makeMakeFormDataMock = (dataObj) => (srcElement) => ({
  entries: () => Object.entries(dataObj),
});
const makePostMock = () => jest.fn((data) => Promise.resolve('is posted'));
const makeEventMock = () => ({ preventDefault: jest.fn(() => {}) });

const makeGetDataAsObject = (obj) => () => obj;

test('useForm posts data', () => {
  const eventMock = makeEventMock();
  const post = makePostMock();
  const m = makeMMock((component, props) => {
    props.onsubmit(eventMock);
    expect(eventMock.preventDefault).toHaveBeenCalledTimes(1);
    expect(post).toHaveBeenCalledTimes(1);
  });

  const useForm = makeUseForm(
    m,
    makeMakeFormDataMock({ test: 'data' }),
    post,
    makeGetDataAsObject({ test: 'data' })
  );
  const comp = useForm(FormComponent);
  comp.view({ children: [] });
});
