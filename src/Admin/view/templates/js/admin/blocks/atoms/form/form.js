import { m } from 'mithril';
import { simpcoDisplayBlock } from 'simpco-mithril/base';

const LoaderComponent = {
  view: () =>
    m(
      '.fw',
      m('.horizontal-center', simpcoDisplayBlock('admin.atom.spinner', {}))
    ),
};

const FormComponent = () => {
  return {
    view: ({ attrs: { loadingForm, onsubmit }, children }) => {
      return loadingForm
        ? m(LoaderComponent)
        : m('form', { onsubmit }, children);
    },
  };
};

export default FormComponent;
