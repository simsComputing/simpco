import {m} from 'mithril';
import { simpcoRegisterBlock } from 'simpco-mithril/base';

const CheckBox = {
    view: (vnode) => m('input', {type: 'checkbox'})
}

simpcoRegisterBlock('admin.atom.input.checkbox', CheckBox);