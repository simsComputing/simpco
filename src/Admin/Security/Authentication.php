<?php

namespace Simpco\Admin\Security;

use Simpco\Admin\Models\User;
use Simpco\Security\Jwt\TokenInterface;

class Authentication implements AuthenticationInterface
{
    private UserTokenFactory $userTokenFactory;

    public function __construct(
        UserTokenFactory $userTokenFactory
    ) {
        $this->userTokenFactory = $userTokenFactory;
    }

    public function isLoggedIn(string $token): bool
    {
        $tokenObject = $this->userTokenFactory->getTokenFromString($token);
        $signature = $this->userTokenFactory->getSignatureFromString($token);
        return $tokenObject->isValid($signature);
    }

    public function getUserId(string $token): int
    {
        $tokenObject = $this->userTokenFactory->getTokenFromString($token);
        return $tokenObject->getPayload()['sub'];
    }

    public function login(User $user): TokenInterface
    {
        return $this->userTokenFactory->getTokenFromUser($user);
    }
}