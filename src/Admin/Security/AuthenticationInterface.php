<?php

namespace Simpco\Admin\Security;

use Simpco\Admin\Models\User;
use Simpco\Security\Jwt\TokenInterface;

interface AuthenticationInterface
{
    public function isLoggedIn(string $token): bool;

    public function getUserId(string $token): int;

    public function login(User $user): TokenInterface;
}
