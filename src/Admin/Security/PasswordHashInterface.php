<?php

namespace Simpco\Admin\Security;

interface PasswordHashInterface
{
    public function hash(string $password): string;

    public function isValid(string $encryptedPassword, string $clearPassword): bool;
}

