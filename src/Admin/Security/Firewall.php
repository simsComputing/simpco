<?php

namespace Simpco\Admin\Security;
use Simpco\Framework\Exception\RedirectionException;
use Simpco\Framework\Router\RouteModifierInterface;
use Simpco\Framework\SessionInterface;

class Firewall implements RouteModifierInterface
{
    protected AuthenticationInterface $authentication;

    protected SessionInterface $session;

    public function __construct(
        AuthenticationInterface $authentication,
        SessionInterface $session
    ) {
        $this->session = $session;
        $this->authentication = $authentication;
    }

    public function modify(string $path): ?string
    {
        if (!preg_match('/^\/admin/', $path)) {
            return $path;
        }

        $token = $this->session->get('admin.token');

        if (($token === null || !$this->authentication->isLoggedIn($token)) && $path !== '/admin/login') {
            throw new RedirectionException('/admin/login');
        }

        if ($path === '/admin/login' && $token !== null && $this->authentication->isLoggedIn($token)) {
            throw new RedirectionException('/admin');
        }

        return $path;
    }
}
