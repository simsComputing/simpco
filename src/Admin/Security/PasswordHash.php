<?php

namespace Simpco\Admin\Security;

use Simpco\Security\Hash\AlgorithmInterface;

class PasswordHash implements PasswordHashInterface
{
    private AlgorithmInterface $algorithmInterface;

    private string $secret;

    public function __construct(
        AlgorithmInterface $algorithmInterface,
        string $secret
    ) {
        $this->algorithmInterface = $algorithmInterface;
        $this->secret = $secret;
    }

    public function hash(string $password): string
    {
        return $this->algorithmInterface->hash($password, $this->secret);
    }

    public function isValid(string $encryptedPassword, string $clearPassword): bool
    {
        return $this->algorithmInterface->validate($encryptedPassword, $clearPassword, $this->secret);
    }
}

