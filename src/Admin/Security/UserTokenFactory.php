<?php

namespace Simpco\Admin\Security;

use Doctrine\ORM\EntityManagerInterface;
use Simpco\Admin\Models\User;
use Simpco\Security\Hash\HS256;
use Simpco\Security\Jwt\Token;
use Simpco\Security\Jwt\TokenInterface;

class UserTokenFactory
{
    private string $secret;

    private HS256 $hS256;

    private EntityManagerInterface $entityManager;

    public function __construct(
        string $secret,
        HS256 $hS256,
        EntityManagerInterface $entityManager
    ) {
        $this->secret = $secret;
        $this->hS256 = $hS256;
        $this->entityManager = $entityManager;
    }

    /**
     * @param TokenInterface $tokenInterface
     * @return User
     */
    public function getUserFromToken(TokenInterface $tokenInterface): User
    {
        $payload = $tokenInterface->getPayload();
        $repo = $this->entityManager->getRepository(User::class);
        return $repo->findOneById($payload['sub']);
    }

    /**
     * @param User $user
     * @return TokenInterface
     */
    public function getTokenFromUser(User $user): TokenInterface
    {
        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT'
        ];
        $payload = [
            'sub' => $user->getId(),
            'name' => $user->getUsername(),
            'admin' => true,
            'expiration' => (new \DateTime('+ 2 hours'))->getTimestamp()
        ];

        $token = new Token($header, $payload, $this->secret, $this->hS256);

        return $token;
    }

    /**
     * @param string $strToken
     * @return TokenInterface
     */
    public function getTokenFromString(string $strToken): TokenInterface
    {
        [$header, $payload] = array_map([$this, 'decodeTokenData'], explode('.', $strToken));
        
        return new Token($header, $payload, $this->secret, $this->hS256);
    }

    /**
     * @param string $data
     * @return array
     */
    private function decodeTokenData(string $data): ?array
    {
        return json_decode(base64_decode($data), true);
    }

    /**
     * @param string $strToken
     * @return string
     */
    public function getSignatureFromString(string $strToken): string
    {
        return explode('.', $strToken)[2];
    }
}