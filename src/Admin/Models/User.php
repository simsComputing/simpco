<?php

namespace Simpco\Admin\Models;

use Doctrine\ORM\Mapping as ORM;
use Simpco\Admin\Models\Repository\UserRepository;

#[ORM\Entity(repositoryClass: UserRepository::class)]
class User
{
    #[ORM\Id, ORM\Column(type: "integer", options: ["unsigned" => true]), ORM\GeneratedValue(strategy: "IDENTITY")]
    private int $id;

    #[ORM\Column(type: "string", length: 32, unique: true, nullable: false)]
    private string $username;

    #[ORM\Column(type: "string", length: 250, unique: true, nullable: false)]
    private string $password;

    /**
     * @var string
     */
    private string $firstname;

    private string $lastname;

    private string $email;

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of username
     *
     * @return  self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname)
    {
        $this->firstname;
        return $this;
    }
}
