<?php

namespace Simpco\Admin\Console;

use Doctrine\ORM\EntityManagerInterface;
use Simpco\Admin\Models\User;
use Simpco\Admin\Security\PasswordHashInterface;
use Simpco\Console\AbstractCommand;
use Simpco\Console\Input\InputInterface;
use Simpco\Console\Input\OptionConfigurationInterface;
use Simpco\Console\Output\OutputInterface;

class CreateUserCommand extends AbstractCommand
{
    const NAME = 'admin:user:create';

    private EntityManagerInterface $entityManager;

    private PasswordHashInterface $passwordHash;

    public function __construct(
        EntityManagerInterface $entityManager,
        PasswordHashInterface $passwordHash
    ) {
        $this->entityManager = $entityManager;
        $this->passwordHash = $passwordHash;
    }

    protected function configure(): void
    {
        $this->setName(self::NAME);
        $this->addOption(
            'username',
            OptionConfigurationInterface::TYPE_STRING,
            OptionConfigurationInterface::IS_REQUIRED
        );
        $this->addOption(
            'password',
            OptionConfigurationInterface::TYPE_STRING,
            OptionConfigurationInterface::IS_REQUIRED
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output): void
    {
        $username = $input->getOption('username');
        $password = $input->getOption('password');

        $user = new User();

        $hashedPassword = $this->passwordHash->hash($password);
        $user->setPassword($hashedPassword);
        $user->setUsername($username);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
