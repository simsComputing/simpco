<?php

namespace Simpco\Admin\Console;

use Simpco\Console\AbstractCommand;
use Simpco\Console\Input\OptionConfigurationInterface;
use Simpco\MithrilIntegration\Javascript\CompilerFactoryInterface;

class CompileFrontCommand extends AbstractCommand
{
    private CompilerFactoryInterface $compilerFactory;

    public function __construct(
        CompilerFactoryInterface $compilerFactory
    ) {
        $this->compilerFactory = $compilerFactory;
    }

    public function configure(): void
    {
        $this
            ->setName('admin:compile:front');
        $this->addOption('watch', OptionConfigurationInterface::TYPE_STRING, OptionConfigurationInterface::IS_REQUIRED);
        $this->addOption('mode', OptionConfigurationInterface::TYPE_STRING, OptionConfigurationInterface::IS_REQUIRED);
    }

    public function execute(
        \Simpco\Console\Input\InputInterface $input,
        \Simpco\Console\Output\OutputInterface $output
    ): void {
        $compiler = $this->compilerFactory->create('admin/index.js', 'admin.js');
        $compiler->compile($input->getOption('mode'), !!(int)$input->getOption('watch'));
    }
}
