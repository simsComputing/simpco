<?php

use Simpco\Admin\Models\Repository\UserRepository;
use Simpco\Admin\Routes\User\Grid as UserGridRoute;
use Simpco\Grid\Column\Types\TextColumn;
use Simpco\Grid\Data\GridDoctrineDataProvider;
use Simpco\Grid\Grid;

return [
    [TextColumn::class, ['__Username__', '__username__'], 'admin.grid.user.column.username'],
    [GridDoctrineDataProvider::class, ['repository' => UserRepository::class], 'admin.grid.user.provider'],
    [Grid::class, [
        'columns' => ['admin.grid.user.column.username'],
        'providers' => ['admin.grid.user.provider'],
        'filters' => [],
        'sorts' => []
    ], 'admin.grid.user'],
    [UserGridRoute::class, ['userGridModel' => 'admin.grid.user'], UserGridRoute::class]
];
