<?php

namespace Simpco\Grid\Sort;

use Simpco\MithrilIntegration\Json\SerializableInterface;

interface SortInterface extends SerializableInterface
{
    /**
     * @return mixed
     */
    public function getValue(): mixed;

    /**
     * @return string
     */
    public function getCode(): string;
}
