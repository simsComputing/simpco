<?php

namespace Simpco\Grid\Sort;

use Simpco\Framework\RequestInterface;

class Sort implements SortInterface
{
    private $value;

    private string $type;

    private string $code;

    private RequestInterface $request;

    private string $label;

    public function __construct(
        string $code,
        mixed $value,
        RequestInterface $requestInterface
    ) {
        $this->value = $value;
        $this->code = $code;
        $this->request = $requestInterface;
    }

    /**
     * @return mixed
     */
    public function getValue(): mixed
    {
        if ($this->request->getBody() === null) {
            return $this->value;
        }

        $filters = $this->request->getBody()->getData('sort', []);

        if (!isset($filters[$this->getCode()])) {
            return $this->value;
        }

        return $filters[$this->getCode()];
    }

    public function toArray(): array
    {
        return [
            'value' => $this->getValue(),
            'code' => $this->getCode()
        ];
    }

    public function json(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}
