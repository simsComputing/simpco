<?php

namespace Simpco\Grid\Cell;

use Simpco\Grid\Column\ColumnInterface;

class CellFactory
{
    public function create(ColumnInterface $column, mixed $data): Cell
    {
        return new Cell($column, $data);
    }
}
