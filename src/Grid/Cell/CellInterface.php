<?php

namespace Simpco\Grid\Cell;

use Simpco\MithrilIntegration\Json\SerializableInterface;

interface CellInterface extends SerializableInterface
{
}
