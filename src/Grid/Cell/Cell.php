<?php

namespace Simpco\Grid\Cell;

use Simpco\Grid\Column\ColumnInterface;

class Cell implements CellInterface
{
    private ColumnInterface $column;

    private mixed $data;

    public function __construct(
        ColumnInterface $column,
        mixed $data
    ) {
        $this->column = $column;
        $this->data = $data;
    }

    public function json(): string
    {
        return json_encode($this->data);
    }

    public function toArray(): array
    {
        return [
            'content' => $this->column->transformDataForDisplay($this->data),
            'type' => $this->column->getType(),
        ];
    }
}
