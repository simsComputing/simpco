<?php

namespace Simpco\Grid\Filter;

use Simpco\MithrilIntegration\Json\SerializableInterface;

interface FilterInterface extends SerializableInterface
{
    /**
     * @return mixed
     */
    public function getValue(): mixed;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @return string
     */
    public function getLabel(): string;
}
