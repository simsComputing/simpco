<?php

namespace Simpco\Grid\Filter;

use Simpco\Framework\RequestInterface;
use Simpco\MithrilIntegration\Json\SerializableInterface;

class Filter implements FilterInterface
{
    private $value;

    private string $type;

    private string $code;

    private RequestInterface $request;

    private string $label;

    public function __construct(
        string $code,
        string $type,
        mixed $value,
        string $label,
        RequestInterface $requestInterface
    ) {
        $this->value = $value;
        $this->type = $type;
        $this->code = $code;
        $this->request = $requestInterface;
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getValue(): mixed
    {
        if ($this->request->getBody() === null) {
            return $this->value;
        }

        $filters = $this->request->getBody()->getData('filter', []);

        if (!isset($filters[$this->getCode()])) {
            return $this->value;
        }

        return $filters[$this->getCode()];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function toArray(): array
    {
        return [
            'type' => $this->getType(),
            'value' => $this->getValue(),
            'label' => $this->getLabel(),
            'code' => $this->getCode()
        ];
    }

    public function json(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    public function getLabel(): string
    {
        return $this->label;
    }
}
