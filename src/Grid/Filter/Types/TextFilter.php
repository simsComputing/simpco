<?php

namespace Simpco\Grid\Filter\Types;

use Simpco\Framework\RequestInterface;
use Simpco\Grid\Filter\Filter;
use Simpco\MithrilIntegration\Json\SerializableInterface;

class TextFilter extends Filter
{
    const TYPE = 'text';

    public function __construct(
        string $code,
        mixed $value,
        string $label,
        RequestInterface $requestInterface
    ) {
        parent::__construct($code, self::TYPE, $value, $label, $requestInterface);
    }
}
