<?php

namespace Simpco\Grid\Data;

use Simpco\Utils\Sort\TopologicalSort\WithClassDependenciesInterface;

interface GridDataProviderInterface extends WithClassDependenciesInterface
{
    public function getData(array $data, array $filters): array;
}
