<?php

namespace Simpco\Grid\Data;

use Doctrine\ORM\EntityRepository;
use Simpco\Grid\Data\Doctrine\Filter\StrategiesRegistry;
use Simpco\Grid\Data\GridDataProviderInterface;
use Simpco\Grid\Filter\FilterInterface;

class GridDoctrineDataProvider implements GridDataProviderInterface
{
    private StrategiesRegistry $strategiesRegistry;

    private EntityRepository $repository;

    public function __construct(
        StrategiesRegistry $strategiesRegistry,
        EntityRepository $repository
    ) {
        $this->strategiesRegistry = $strategiesRegistry;
        $this->repository = $repository;
    }

    public function getData(array $data, array $filters): array
    {
        $builder = $this->repository->createQueryBuilder('m');

        /** @var FilterInterface $filter */
        foreach ($filters as $filter) {
            $doctrineFilter = $this->strategiesRegistry->getStrategyForType($filter->getType());
            $doctrineFilter->addFilter($builder, $filter);
        }

        $result = $builder->getQuery()->getArrayResult();

        return $result;
    }

    /**
     * @return void
     */
    public function getDependencies(): array
    {
        return [];
    }
}
