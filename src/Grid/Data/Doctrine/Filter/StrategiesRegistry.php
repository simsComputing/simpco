<?php

namespace Simpco\Grid\Data\Doctrine\Filter;

use Simpco\Grid\Exception\GridBuildingException;

class StrategiesRegistry
{
    private array $strategies = [];

    public function __construct(
        array $strategies
    ) {
        /** @var FilterStrategyInterface $strategy */
        foreach ($strategies as $strategy) {
            foreach ($strategy->getSupportedTypes() as $type) {
                $this->strategies[$type] = $strategy;
            }
        }
    }

    public function getStrategyForType(string $type): FilterStrategyInterface
    {
        if (!isset($this->strategies[$type])) {
            throw new GridBuildingException('Missing doctrine filter strategy for type ' . $type);
        }

        return $this->strategies[$type];
    }
}
