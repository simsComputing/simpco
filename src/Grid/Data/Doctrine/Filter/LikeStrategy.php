<?php

namespace Simpco\Grid\Data\Doctrine\Filter;

use Doctrine\ORM\QueryBuilder;
use Simpco\Grid\Filter\FilterInterface;
use Simpco\Grid\Filter\Types\TextFilter;

class LikeStrategy implements FilterStrategyInterface
{
    /**
     * @return array
     */
    public function getSupportedTypes(): array
    {
        return [TextFilter::TYPE];
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param FilterInterface $filter
     * @return self
     */
    public function addFilter(QueryBuilder $queryBuilder, FilterInterface $filter): self
    {
        if (!$filter->getValue()) {
            return $this;
        }

        $queryBuilder->andWhere('m.' . $filter->getCode() . ' LIKE :' . $filter->getCode());
        $queryBuilder->setParameter($filter->getCode(), '%' . $filter->getValue() . '%');
        return $this;
    }
}
