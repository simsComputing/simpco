<?php

namespace Simpco\Grid\Data\Doctrine\Filter;

use Doctrine\ORM\QueryBuilder;
use Simpco\Grid\Filter\FilterInterface;

interface FilterStrategyInterface
{
    public function getSupportedTypes(): array;

    public function addFilter(QueryBuilder $queryBuilder, FilterInterface $filter): self;
}
