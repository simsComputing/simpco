<?php

namespace Simpco\Grid\Column\Types;

class TextColumn extends \Simpco\Grid\Column\Column
{
    const TYPE = 'text';

    public function __construct(
        string $title,
        string $code
    ) {
        parent::__construct($title, self::TYPE, $code);
    }

    public function transformDataForDisplay(mixed $data): mixed
    {
        return (string)$data;
    }
}
