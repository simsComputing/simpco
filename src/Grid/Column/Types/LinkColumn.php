<?php

namespace Simpco\Grid\Column\Types;

class LinkColumn extends \Simpco\Grid\Column\Column
{
    const TYPE = 'link';

    private string $displayText;

    public function __construct(
        string $displayText,
        string $title,
        string $code
    ) {
        parent::__construct($title, self::TYPE, $code);
        $this->displayText = $displayText;
    }

    public function transformDataForDisplay(mixed $data): mixed
    {
        return [
            'text' => $this->displayText,
            'link' => (string)$data
        ];
    }
}
