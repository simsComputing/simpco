<?php

namespace Simpco\Grid\Column;

class Column implements ColumnInterface
{
    private string $title;

    private string $type;

    private string $code;

    /**
     * @param string $title
     */
    public function __construct(
        string $title,
        string $type,
        string $code
    ) {
        $this->title = $title;
        $this->type = $type;
        $this->code = $code;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function transformDataForDisplay(mixed $data): mixed
    {
        return $data;
    }

    public function getSortOrder(): string
    {
        return 'DESC';
    }

    public function json(): string
    {
        return json_encode($this->toArray());
    }

    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'type' => $this->type,
            'code' => $this->code
        ];
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
