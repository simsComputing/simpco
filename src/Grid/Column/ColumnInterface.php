<?php

namespace Simpco\Grid\Column;

use Simpco\MithrilIntegration\Json\SerializableInterface;

interface ColumnInterface extends SerializableInterface
{
    public function getTitle(): string;

    public function transformDataForDisplay(mixed $data): mixed;

    public function getType(): string;

    public function getCode(): string;

    public function getSortOrder(): string;
}
