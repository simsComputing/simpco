<?php

namespace Simpco\Grid\Routes;

use Simpco\Grid\Data\GridDataProviderInterface;
use Simpco\Grid\Filter\FilterInterface;

class TestProvider implements GridDataProviderInterface
{
    public function getData(array $data, array $filters): array
    {
        $data = [
            ['id' => '1', 'name' => 'Table à manger', 'type' => 'Simple', 'price' => '53e', 'action' => 'https://google.com'],
            ['id' => '2', 'name' => 'Table à roulette', 'type' => 'Simple', 'price' => '53e', 'action' => 'https://google.com'],
            ['id' => '3', 'name' => 'Table à roulette', 'type' => 'Simple', 'price' => '53e', 'action' => 'https://google.com'],
            ['id' => '4', 'name' => 'Table à roulette', 'type' => 'Simple', 'price' => '53e', 'action' => 'https://google.com'],
            ['id' => '5', 'name' => 'Table à roulette', 'type' => 'Simple', 'price' => '53e', 'action' => 'https://google.com'],
            ['id' => '5', 'name' => 'Table à roulette', 'type' => 'Simple', 'price' => '53e', 'action' => 'https://google.com'],
            ['id' => '6', 'name' => 'Table à roulette', 'type' => 'Configurable', 'price' => '53e', 'action' => 'https://google.com'],
            ['id' => '7', 'name' => 'Table à roulette', 'type' => 'Bundle', 'price' => '53e', 'action' => 'https://google.com'],
        ];

        /** @var FilterInterface $filter */
        foreach ($filters as $filter) {
            $data = array_filter($data, function ($datum) use ($filter) {
                if (!$filter->getValue()) {
                    return true;
                }

                return preg_match('/' . $filter->getValue() . '/', $datum[$filter->getCode()]);
            });
        }

        return $data;
    }

    public function getDependencies(): array
    {
        return [];
    }
}
