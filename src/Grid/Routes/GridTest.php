<?php

namespace Simpco\Grid\Routes;

use Simpco\Grid\Grid;
use Simpco\MithrilIntegration\Route\MithrilRoute;

class GridTest extends MithrilRoute
{
    private Grid $grid;

    public function __construct(
        \Simpco\Framework\RequestInterface $request,
        \Simpco\Framework\ViewFactory $viewFactory,
        \Simpco\Framework\ResponseInterface $response,
        Grid $grid
    ) {
        parent::__construct($request, $viewFactory, $response);
        $this->grid = $grid;
    }

    public function getData(): array
    {
        return ['grid' => $this->grid->toArray()];
    }
}
