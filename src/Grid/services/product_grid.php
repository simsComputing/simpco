<?php

use Simpco\Admin\Models\Repository\UserRepository;
use Simpco\Grid\Column\Types\LinkColumn;
use Simpco\Grid\Column\Types\TextColumn;
use Simpco\Grid\Data\GridDoctrineDataProvider;
use Simpco\Grid\Filter\Types\TextFilter;
use Simpco\Grid\Grid;
use Simpco\Grid\Routes\GridTest;
use Simpco\Grid\Routes\TestProvider;
use Simpco\Grid\Sort\Sort;

return [
    // COLUMNS
    [TextColumn::class, ['__Username__', '__username__'], 'grid.user.username'],
    [TextColumn::class, ['__Password__', '__password__'], 'grid.user.password'],
    // FILTERS
    [TextFilter::class, ['__username__', '____', '__Username__'], 'grid.user.filter.username'],
    // SORTs
    // [Sort::class, ['__id__', '____'], 'grid.product.sort.id'],
    // PROVIDER
    [GridDoctrineDataProvider::class, ['repository' => UserRepository::class], 'grid.user.data.provider'],
    // GRID
    [
        Grid::class,
        [
            'columns' => ['grid.user.username', 'grid.user.password'],
            'providers' => ['grid.user.data.provider'],
            'filters' => ['grid.user.filter.username'],
            // 'sorts' => ['grid.product.sort.id']
        ],
        'grid.product'
    ],
    // INJECT GRID IN CONTROLLER
    [GridTest::class, ['grid' => 'grid.product'], GridTest::class]
];
