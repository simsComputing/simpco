<?php

namespace Simpco\Grid;

use Simpco\Grid\Data\GridDataProviderInterface;
use Simpco\Grid\Row\RowFactory;
use Simpco\Utils\Sort\TopologicalSort\Registry;
use Simpco\Utils\Sort\TopologicalSort\RegistryFactory;

class Grid implements GridInterface
{
    private array $columns = [];

    private array $filters;

    private Registry $registry;

    private RowFactory $rowFactory;

    /**
     * @var array
     */
    private array $rows = [];

    /**
     * @var array
     */
    private array $data = [];

    /**
     * @var array
     */
    private array $sorts = [];

    public function __construct(
        array $columns,
        array $providers,
        array $filters,
        array $sorts,
        RegistryFactory $registryFactory,
        RowFactory $rowFactory
    ) {
        $this->registry = $registryFactory->create($providers);
        $this->columns = $columns;
        $this->rowFactory = $rowFactory;
        $this->filters = $filters;
        $this->sorts = $sorts;
    }

    private function getData(): array
    {
        if (count($this->data) !== 0) {
            return $this->data;
        }

        $data = [];
        /** @var GridDataProviderInterface $provider */
        foreach ($this->registry->getList() as $provider) {
            $data = $provider->getData($data, $this->filters);
        }
        $this->data = $data;
        return $this->data;
    }

    /**
     * @return array
     */
    private function getRows(): array
    {
        if (count($this->rows) !== 0) {
            return $this->rows;
        }

        foreach ($this->getData() as $datum) {
            $this->rows[] = $this->rowFactory->create($this->columns, $datum);
        }

        return $this->rows;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'columns' => array_map(function ($column) {
                return $column->toArray();
            }, $this->columns),
            'rows' => array_map(function ($row) {
                return $row->toArray();
            }, $this->getRows()),
            'filters' => array_map(function ($filter) {
                return $filter->toArray();
            }, $this->filters),
            'sorts' => array_map(function ($sort) {
                return $sort->toArray();
            }, $this->sorts)
        ];
    }

    public function json(): string
    {
        return json_encode($this->toArray());
    }
}
