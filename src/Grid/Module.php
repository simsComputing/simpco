<?php

namespace Simpco\Grid;

use Simpco\Framework\DependencyInjection\ServiceFactory;
use Simpco\Framework\Module\EmptyModuleTrait;
use Simpco\Framework\ModuleInterface;
use Simpco\Framework\RequestInterface;
use Simpco\Grid\Data\Doctrine\Filter\LikeStrategy;
use Simpco\Grid\Data\Doctrine\Filter\StrategiesRegistry;
use Simpco\Grid\Routes\GridTest;
use Simpco\MithrilIntegration\Module as MithrilIntegrationModule;
use Simpco\Utils\Routes\ModifyDefinition;

class Module implements ModuleInterface
{
    use EmptyModuleTrait;

    public function getServices(): array
    {
        return ServiceFactory::bashFactory([
            ...require __DIR__ . '/services/product_grid.php',
            [
                StrategiesRegistry::class,
                [
                    'strategies' => [
                        LikeStrategy::class
                    ]
                ],
                StrategiesRegistry::class
            ]
        ]);
    }

    public function getRoutes(): array
    {
        return [
            RequestInterface::GET => ModifyDefinition::prefix("/grid", [
                "/test" => GridTest::class
            ]),
            RequestInterface::POST => ModifyDefinition::prefix('/grid', [
                "/test" => GridTest::class
            ])
        ];
    }

    public function getViewDir(): ?string
    {
        return __DIR__ . "/view";
    }

    public function getDependencies(): array
    {
        return [
            MithrilIntegrationModule::class
        ];
    }
}
