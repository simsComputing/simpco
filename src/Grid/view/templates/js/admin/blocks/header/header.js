import { m, redraw } from 'mithril';

const HeaderCell = (initialVnode) => {
  var sortOrder = '-';
  const changeDisplaySortOrder = () =>
    (sortOrder = sortOrder === '>' ? '<' : sortOrder === '<' ? '-' : '>');
  return {
    view: ({ attrs: { title } }) => {
      return m(
        'th',
        {
          class: 'header__cell',
          onclick: changeDisplaySortOrder,
        },
        title + ' ' + sortOrder
      );
    },
  };
};

const HeaderCompponent = ({ attrs: { columns, sorts } }) => {
  return {
    view: () => {
      const cells = columns.map(({ title }) => m(HeaderCell, { title }));
      return m('tr.header__row', cells);
    },
  };
};

export default HeaderCompponent;
