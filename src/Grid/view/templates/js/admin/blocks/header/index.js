import { simpcoRegisterBlock } from 'simpco-mithril/base';
import HeaderCompponent from './header';
import './header.scss';

simpcoRegisterBlock('admin.grid.header', HeaderCompponent);
