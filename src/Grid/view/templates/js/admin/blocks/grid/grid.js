import { m } from 'mithril';
import { simpcoDisplayBlock } from 'simpco-mithril/base';

const GridComponent = () => {
  return {
    view: ({
      attrs: {
        grid: { columns, rows, filters, sorts },
      },
    }) =>
      simpcoDisplayBlock(
        'admin.atom.form',
        {},
        m('.admin-grid', [
          simpcoDisplayBlock('admin.grid.filterList', { filters }),
          simpcoDisplayBlock('admin.atom.button', {
            type: 'submit',
            text: 'Appliquer',
          }),
          m('table', [
            simpcoDisplayBlock('admin.grid.header', { columns, sorts }),
            ...rows.map((row) => simpcoDisplayBlock('admin.grid.row', { row })),
          ]),
        ])
      ),
  };
};

export default GridComponent;
