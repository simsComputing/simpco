import { simpcoRegisterBlock } from 'simpco-mithril/base';
import GridComponent from './grid';
import './grid.scss';

simpcoRegisterBlock('admin.grid', GridComponent);
