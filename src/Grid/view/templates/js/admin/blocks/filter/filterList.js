import { m } from 'mithril';
import { simpcoDisplayBlock } from 'simpco-mithril/base';

const FilterListComponent = () => {
  return {
    view: ({ attrs: { filters } }) => {
      return m(
        '.filter-list',
        filters.map((filter) =>
          simpcoDisplayBlock('admin.grid.filter.' + filter.type, { filter })
        )
      );
    },
  };
};

export default FilterListComponent;
