import { simpcoRegisterBlock } from 'simpco-mithril/base';
import TextFilterComponent from './types/text';
import FilterListComponent from './filterList';
import './filter.scss';

simpcoRegisterBlock('admin.grid.filter.text', TextFilterComponent);
simpcoRegisterBlock('admin.grid.filterList', FilterListComponent);
