import { m } from 'mithril';
import { simpcoDisplayBlock } from 'simpco-mithril/base';

const TextFilterComponent = () => {
  return {
    view: ({ attrs: { filter } }) => {
      return m('.filter.filter--text', [
        m('.filter__label', filter.label),
        simpcoDisplayBlock('admin.atom.input.text', {
          value: filter.value,
          name: 'filter[' + filter.code + ']',
        }),
      ]);
    },
  };
};

export default TextFilterComponent;
