import { m } from 'mithril';

const LinkCellComponent = () => {
  return {
    view: ({
      attrs: {
        cell: { content },
      },
    }) => {
      return m('td.cell', m('a', { href: content.link }, content.text));
    },
  };
};

export default LinkCellComponent;
