import { simpcoRegisterBlock } from 'simpco-mithril/base';
import TextCellComponent from './types/text';
import LinkCellComponent from './types/link';

simpcoRegisterBlock('admin.grid.cell.text', TextCellComponent);
simpcoRegisterBlock('admin.grid.cell.link', LinkCellComponent);
