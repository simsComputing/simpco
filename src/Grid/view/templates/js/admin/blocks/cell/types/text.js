import { m } from 'mithril';

const TextCellComponent = () => {
  return {
    view: ({
      attrs: {
        cell: { content },
      },
    }) => {
      return m('td.cell', content);
    },
  };
};

export default TextCellComponent;
