import { simpcoRegisterBlock } from 'simpco-mithril/base';
import RowComponent from './row';
import './row.scss';

simpcoRegisterBlock('admin.grid.row', RowComponent);
