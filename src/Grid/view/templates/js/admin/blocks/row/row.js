import { m } from 'mithril';
import { simpcoDisplayBlock } from 'simpco-mithril/base';

const cells = [1, 1, 1, 1, 1];

const RowComponent = () => {
  return {
    view: ({
      attrs: {
        row: { cells },
      },
    }) =>
      m(
        'tr.row',
        cells.map((cell) =>
          simpcoDisplayBlock('admin.grid.cell.' + cell.type, { cell })
        )
      ),
  };
};

export default RowComponent;
