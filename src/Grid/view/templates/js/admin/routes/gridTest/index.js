import { simpcoRegisterBlock, simpcoRegisterRoute } from 'simpco-mithril/base';
import GridTestRoute from './gridTest';

simpcoRegisterBlock('simpco.grid.routes.gridTest', GridTestRoute);
simpcoRegisterRoute('/grid/test', 'simpco.grid.routes.gridTest');
