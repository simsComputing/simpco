import { m } from 'mithril';
import { simpcoDisplayBlock } from 'simpco-mithril/base';

const GridTestRoute = () => {
  return {
    view: ({ attrs: { grid } }) => {
      console.dir(grid);
      return simpcoDisplayBlock('admin.organism.layout', {
        content: simpcoDisplayBlock('admin.grid', { grid }),
      });
    },
  };
};

export default GridTestRoute;
