<?php

namespace Simpco\Grid\Row;

use Simpco\MithrilIntegration\Json\SerializableInterface;

interface RowInterface extends SerializableInterface
{
}
