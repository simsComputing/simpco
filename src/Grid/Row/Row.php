<?php

namespace Simpco\Grid\Row;

use Simpco\Grid\Cell\CellFactory;
use Simpco\Grid\Column\ColumnInterface;
use Simpco\Grid\Exception\GridBuildingException;

class Row implements RowInterface
{
    /**
     * @var array
     */
    private array $columns;

    /**
     * @var array
     */
    private array $data;

    /**
     * @var CellFactory
     */
    private CellFactory $cellFactory;

    /**
     * @var array
     */
    private array $cells = [];

    /**
     * @param array $columns
     * @param array $data
     * @param CellFactory $cellFactory
     */
    public function __construct(
        array $columns,
        array $data,
        CellFactory $cellFactory
    ) {
        $this->columns = $columns;
        $this->data = $data;
        $this->cellFactory = $cellFactory;
    }

    /**
     * @return string
     */
    public function json(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * @return array
     */
    private function getCells(): array
    {
        if (count($this->cells) !== 0) {
            return $this->cells;
        }

        /** @var ColumnInterface $column */
        foreach ($this->columns as $column) {
            $datum = $this->data[$column->getCode()];
            $this->cells[] = $this->cellFactory->create($column, $datum);
        }


        return $this->cells;
    }

    public function toArray(): array
    {
        $cells = $this->getCells();

        $cellsArrs = array_map(function ($cell) {
            return $cell->toArray();
        }, $cells);

        return [
            'cells' => $cellsArrs,
        ];
    }
}
