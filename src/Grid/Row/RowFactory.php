<?php

namespace Simpco\Grid\Row;

use Simpco\Grid\Cell\CellFactory;

class RowFactory
{
    private CellFactory $cellFactory;

    public function __construct(CellFactory $cellFactory)
    {
        $this->cellFactory = $cellFactory;
    }

    public function create(array $columns, array $data): Row
    {
        return new Row($columns, $data, $this->cellFactory);
    }
}
