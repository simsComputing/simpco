<?php

namespace Simpco\Grid;

use Simpco\Grid\Row\RowFactory;
use Simpco\Utils\Sort\TopologicalSort\RegistryFactory;

class GridFactory
{
    private RegistryFactory $registryFactory;

    private RowFactory $rowFactory;

    public function __construct(RegistryFactory $registryFactory, RowFactory $rowFactory)
    {
        $this->registryFactory = $registryFactory;
        $this->rowFactory = $rowFactory;
    }

    public function create(array $columns, array $providers): Grid
    {
        return new Grid($columns, $providers, $this->registryFactory, $this->rowFactory);
    }
}
