<?php

namespace Simpco\Form;

use Simpco\Framework\RequestInterface;

interface FormInterface
{
    public function getErrors(): array;

    public function post(RequestInterface $requestInterface): self;

    public function validate(RequestInterface $requestInterface): bool;

    public function getData(RequestInterface $requestInterface): array;
}
