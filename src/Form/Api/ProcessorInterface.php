<?php

namespace Simpco\Form\Api;

use Simpco\Utils\Data\DataObjectInterface;

interface ProcessorInterface
{
    public function process(array $data, DataObjectInterface $providersRegistry): mixed;
}
