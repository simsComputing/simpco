<?php

namespace Simpco\Form\Api;

use Simpco\Utils\Data\DataObjectInterface;

interface ValidatorInterface
{
    public function validate(array $data, DataObjectInterface $providersRegistry): mixed;
}
