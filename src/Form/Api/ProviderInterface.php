<?php

namespace Simpco\Form\Api;

interface ProviderInterface
{
    public function getData(): mixed;

    public function getModel(): mixed;
}
