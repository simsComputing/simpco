<?php

namespace Simpco\Form;

use Simpco\Form\Api\ProcessorInterface;
use Simpco\Form\Api\ProviderInterface;
use Simpco\Form\Api\ValidatorInterface;
use Simpco\Form\Error\ErrorInterface;
use Simpco\Framework\RequestInterface;
use Simpco\Utils\Data\DataObjectFactory;
use Simpco\Utils\Data\DataObjectInterface;

class Form implements FormInterface
{
    /**
     * @var DataObjectInterface
     */
    protected DataObjectInterface $providersRegistry;

    /**
     * @var ValidatorInterface[]
     */
    protected array $validators;

    /**
     * @var ProcessorInterface[]
     */
    protected array $processors;

    /**
     * @var ErrorInterface[]
     */
    protected array $errors = [];

    /**
     * @param array $providers
     * @param array $validators
     * @param array $processors
     * @param DataObjectFactory $dataObjectFactory
     */
    public function __construct(
        array $providers,
        array $validators,
        array $processors,
        DataObjectFactory $dataObjectFactory
    ) {
        $this->providersRegistry = $dataObjectFactory->create($providers);
        $this->validators = $validators;
        $this->processors = $processors;
    }

    /**
     * @return ErrorInterface[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param RequestInterface $requestInterface
     * @return self
     */
    public function post(RequestInterface $requestInterface): self
    {
        foreach ($this->processors as $processor) {
            $processor->process(
                $requestInterface->getBody()->getData(),
                $this->providersRegistry
            );
        }

        return $this;
    }

    /**
     * @param RequestInterface $requestInterface
     * @return boolean
     */
    public function validate(RequestInterface $requestInterface): bool
    {
        $errors = [];
        foreach ($this->validators as $validator) {
            $error = $validator->validate(
                $requestInterface->getBody()->getData(),
                $this->providersRegistry
            );
            if ($error === null) {
                continue;
            }
            $errors = [
                $error,
                ...$errors
            ];
        }
        $this->errors = $errors;
        return count($errors) === 0;
    }

    /**
     * @return DataObjectInterface
     */
    public function getProvidersRegistry(): DataObjectInterface
    {
        return $this->providersRegistry;
    }

    public function getData(RequestInterface $requestInterface): array
    {

        if (!empty($this->errors)) {
            return $requestInterface->getBody()->getData();
        }

        $data = [];
        /** @var ProviderInterface $provider */
        foreach ($this->providersRegistry->getData() as $provider) {
            $data = array_merge($data, $provider->getData());
        }
        return $data;
    }
}
