<?php

namespace Simpco\Form;
use Simpco\Framework\Module\EmptyModuleTrait;
use Simpco\Framework\ModuleInterface;

class Module implements ModuleInterface
{
    use EmptyModuleTrait;
}