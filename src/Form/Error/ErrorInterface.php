<?php

namespace Simpco\Form\Error;

interface ErrorInterface
{
    public function getMessage(): string;

    public function getCode(): string;
}
