<?php

namespace Simpco\Form\Test;
use PHPUnit\Framework\TestCase;
use Simpco\Form\Form;
use Simpco\Utils\Data\DataObjectFactory;
use Simpco\Utils\Data\DataObjectInterface;

class FormTest extends TestCase
{
    private DataObjectFactory $dataObjectFactory;

    private DataObjectInterface $dataObject;

    private array $validators;

    private array $processors;

    private array $dataProviders;

    public function setUp(): void
    {
        $this->dataObjectFactory = $this->createMock(DataObjectFactory::class);
        $this->dataObject = $this->createStub(DataObjectInterface::class);

        $this->validators = [];
        $this->processors = [];
        $this->dataProviders = [];
    }

    private function setProvidersForTest(array $providers)
    {
        $this->dataProviders = $providers;
        $this->dataObject->method('getData')->willReturnCallback(function ( $key = null, $default = null) use ($providers) {
            if ($key === null) {
                return $default;
            }
            
            return $providers[$key];
        });

        $this->dataObjectFactory
            ->expects($this->once())
            ->method('create')
            ->with($providers)
            ->willReturn($this->dataObject);
    }

    private function initForm(): Form
    {
        return new Form(
            $this->dataProviders,
            $this->validators,
            $this->processors,
            $this->dataObjectFactory
        );
    }

    public function testProvidersRegistry(): void
    {
        $this->setProvidersForTest(['test' => 'will return this value']);
        $form = $this->initForm();
        $registry = $form->getProvidersRegistry();
        $this->assertEquals('will return this value', $registry->getData('test'));
    }
}