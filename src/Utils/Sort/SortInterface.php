<?php

namespace Simpco\Utils\Sort;

interface SortInterface
{
    public function sort(): array;
}