<?php

namespace Simpco\Utils\Sort\TopologicalSort;

class RegistryFactory
{
    private Factory $factory;

    public function __construct(
        Factory $factory
    ) {
        $this->factory = $factory;
    }

    /**
     * @param array $list
     * @return Registry
     */
    public function create(array $list): Registry
    {
        return new Registry($list, $this->factory);
    }
}
