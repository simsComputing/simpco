<?php

namespace Simpco\Utils\Sort\TopologicalSort;
use Simpco\Utils\Sort\TopologicalSort;

class Factory
{
    public function create(
        array $mapping
    ) {
        $vertices = [];
        foreach ($mapping as $key => $deps) {
            $vertices[$key] = new Vertice($key);
        }

        foreach ($mapping as $key => $deps) {
            foreach ($deps as $dep) {
                $vertices[$key]->addEdge(new Edge($vertices[$dep]));
            }
        }

        return new TopologicalSort($vertices);
    }
}