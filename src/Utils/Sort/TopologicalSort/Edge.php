<?php

namespace Simpco\Utils\Sort\TopologicalSort;

class Edge
{
    private Vertice $vertice;

    public function __construct(
        Vertice $vertice
    ) {
        $this->vertice = $vertice;
    }

    public function getVertice(): Vertice
    {
        return $this->vertice;
    }
}