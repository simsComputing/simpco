<?php

namespace Simpco\Utils\Sort\TopologicalSort;

use Simpco\Utils\Sort\TopologicalSort\Factory as TopologicalSortFactory;

class Registry
{
    private array $initialList;

    private TopologicalSortFactory $topologicalSortFactory;

    private array $sortedList = [];

    public function __construct(
        $list,
        TopologicalSortFactory $topologicalSortFactory
    ) {
        $this->initialList = $list;
        $this->topologicalSortFactory = $topologicalSortFactory;
    }

    public function getList()
    {
        if (count($this->sortedList) !== 0) {
            return $this->sortedList;
        }

        $modulesWithDeps = [];
        $indexedModules = [];
        /** @var WithClassDependenciesInterface $module */
        foreach ($this->initialList as $module) {
            $modulesWithDeps[get_class($module)] = $module->getDependencies();
            $indexedModules[get_class($module)] = $module;
        }

        $sort = $this->topologicalSortFactory->create($modulesWithDeps);
        $sorted = $sort->sort();

        foreach ($sorted as $moduleClass) {
            $this->sortedList[] = $indexedModules[$moduleClass];
        }

        return $this->sortedList;
    }
}
