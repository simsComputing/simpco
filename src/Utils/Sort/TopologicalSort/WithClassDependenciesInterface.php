<?php

namespace Simpco\Utils\Sort\TopologicalSort;

interface WithClassDependenciesInterface
{
    public function getDependencies(): array;
}
