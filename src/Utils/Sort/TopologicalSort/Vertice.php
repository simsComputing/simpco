<?php

namespace Simpco\Utils\Sort\TopologicalSort;

use Simpco\Utils\Exception\CircularDependencyException;

class Vertice
{
    /**
     *
     * @var Edgge[]
     */
    private array $edges = [];

    /**
     * @var boolean
     */
    private bool $visited = false;

    /**
     * @var boolean
     */
    private bool $visiting = false;

    /**
     * @var [type]
     */
    private $finalData;

    /**
     * @param [type] $finalData
     */
    public function __construct(
        $finalData
    ) {
        $this->finalData = $finalData;
    }

    /**
     * @param Edge $edge
     * @return void
     */
    public function addEdge(Edge $edge)
    {
        $this->edges[] = $edge;
    }

    /**
     * @return array
     */
    public function getEdges(): array
    {
        return $this->edges;
    }

    /**
     * @param array $sorted
     * @return void
     */
    public function visit(array &$sorted): void
    {
        if ($this->visiting) {
            throw new CircularDependencyException('There is a circular dependency on topological sort');
        }

        if ($this->visited) {
            return;
        }

        $edges = $this->getEdges();
        $this->visiting = true;
        /** @var Edge $edge */
        foreach ($edges as $edge) {
            $edge->getVertice()->visit($sorted);
        }

        $sorted[] = $this->finalData;
        $this->visiting = false;
        $this->visited = true;
    }
}
