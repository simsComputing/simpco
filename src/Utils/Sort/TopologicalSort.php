<?php

namespace Simpco\Utils\Sort;

use Simpco\Utils\Sort\TopologicalSort\Vertice;

class TopologicalSort implements SortInterface
{
    private array $graph;

    /**
     * @param array $initialArray
     */
    public function __construct(array $graph)
    {
        $this->graph = $graph;
    }

    public function sort(): array
    {
        $result = [];
        /**
         * @var Vertice $vertice
         */
        foreach ($this->graph as $vertice) {
            $vertice->visit($result);
        }

        return $result;
    }
}