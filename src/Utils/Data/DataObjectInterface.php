<?php

namespace Simpco\Utils\Data;

interface DataObjectInterface
{
    public function getData($key, $default = null): mixed;
}