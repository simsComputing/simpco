<?php

namespace Simpco\Utils\Data;

class DataObjectFactory
{
    public function create(array $data): DataObject
    {
        return new DataObject($data);
    }
}