<?php

namespace Simpco\Utils\Data;

class DataObject implements DataObjectInterface
{
    private array $data;

    public function __construct(
        array $data
    ) {
        $this->data = $data;
    }

    public function getData($key = null, $default = null): mixed
    {
        if ($key === null) {
            return $this->data;
        }

        if (isset($this->data[$key])) {
            return $this->data[$key];
        }

        return $default;
    }
}