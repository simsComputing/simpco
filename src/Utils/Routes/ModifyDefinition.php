<?php

namespace Simpco\Utils\Routes;

class ModifyDefinition
{
    /**
     * @param string $prefix
     * @param array $routes
     * @return array
     */
    public static function prefix(string $prefix, array $routes): array
    {
        $result = [];
        foreach ($routes as $url => $obj) {
            $result[$prefix . $url] = $obj;
        }

        return $result;
    }
}