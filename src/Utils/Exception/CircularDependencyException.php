<?php

namespace Simpco\Utils\Exception;

class CircularDependencyException extends \Exception
{
}