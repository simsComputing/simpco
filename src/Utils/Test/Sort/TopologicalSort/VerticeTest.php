<?php

namespace Simpco\Utils\Test\Sort\TopologicalSort;

use PHPUnit\Framework\TestCase;
use Simpco\Utils\Sort\TopologicalSort\Edge;
use Simpco\Utils\Sort\TopologicalSort\Vertice;

/**
 * @covers \Simpco\Utils\Sort\TopologicalSort\Vertice
 * @uses \Simpco\Utils\Sort\TopologicalSort\Edge
 * @uses \Simpco\Utils\Test\Sort\TopologicalSort\VerticeTest
 */
class VerticeTest extends TestCase
{
    public static function getDataSet(): array
    {
        return [
            self::getFirstTestCase()
        ];
    }

    public static function getFirstTestCase(): array
    {
        return [[
            'V' => [],
            'A' => ['B'],
            'C' => ['A'],
            'B' => [],
            'D' => ['C']
        ]];
    }

    /**
     * @dataProvider getDataSet
     *
     * @param [type] $mapping
     * @return void
     */
    public function testSimpleUseCase($mapping)
    {
        $vertices = [];
        foreach ($mapping as $key => $deps) {
            $vertices[$key] = new Vertice($key);
        }

        foreach ($mapping as $key => $deps) {
            foreach ($deps as $dep) {
                $vertices[$key]->addEdge(new Edge($vertices[$dep]));
            }
        }

        $result = [];
        foreach ($vertices as $vertice) {
            $vertice->visit($result);
        }

        $this->assertEquals(array_search('V', $result), 0);
        $this->assertEquals(array_search('B', $result), 1);
        $this->assertEquals(array_search('A', $result), 2);
        $this->assertEquals(array_search('C', $result), 3);
        $this->assertEquals(array_search('D', $result), 4);

    }
}