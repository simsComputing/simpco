<?php

namespace Simpco\StoryBook;

use App\Routes\Index;
use Simpco\Admin\Module as AdminModule;
use Simpco\Framework\Module\EmptyModuleTrait;
use Simpco\Framework\ModuleInterface;
use Simpco\Framework\RequestInterface;
use Simpco\MithrilIntegration\Module as MithrilIntegrationModule;

class Module implements ModuleInterface
{
    use EmptyModuleTrait;

    public function getViewDir(): ?string
    {
        return __DIR__ . '/view';
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return [
            RequestInterface::GET => [
                '/storybook' => Index::class
            ]
        ];
    }

    public function getDependencies(): array
    {
        return [
            MithrilIntegrationModule::class,
            AdminModule::class
        ];
    }
}
