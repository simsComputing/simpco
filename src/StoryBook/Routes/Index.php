<?php

namespace Simpco\StoryBook\Routes;

use Simpco\MithrilIntegration\Route\MithrilRoute;

class Index extends MithrilRoute
{
    protected function getData(): array
    {
        return [];
    }
}
