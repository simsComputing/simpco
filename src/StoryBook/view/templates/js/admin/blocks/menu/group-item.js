import {m} from 'mithril';
import { simpcoRegisterBlock } from 'simpco-mithril/base';

const GroupItem = {
    view: (vnode) => m('li', {class: 'pure-menu-item menu__group__item'}, [
        m('a', {href: '#' + vnode.attrs.name, class: 'pure-menu-link menu__group__item__link'}, vnode.attrs.name)
    ])
}

simpcoRegisterBlock('admin.storybook.group.item', GroupItem);