import {m} from 'mithril';
import { simpcoDisplayBlock, simpcoRegisterBlock } from 'simpco-mithril/base';

const MenuGroup = {
    view: (vnode) => {
        const items = vnode.attrs.items ?? [];


        return m('div', {class: 'menu__group'}, [
            m('h2', {class: 'menu__group__title'}, vnode.attrs.name),
            m('div', {class: 'pure-menu'}, [
                m('ul', {class: 'pure-menu-list'}, items.map(({name}) => simpcoDisplayBlock('admin.storybook.group.item', {name})))
            ])

        ])
    }
}

simpcoRegisterBlock('admin.storybook.menu.group', MenuGroup)