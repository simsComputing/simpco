import {m} from 'mithril';
import { simpcoDisplayBlock, simpcoRegisterBlock } from 'simpco-mithril/base';

const StoryBookElement = {
    view: (vnode) => {
        const wrappedBlock = () => simpcoDisplayBlock(
            vnode.attrs.wrapper.block, 
            {...vnode.attrs.wrapper.props, block: vnode.attrs.block, props: vnode.attrs.props}
        );

        const simpleBlock = () => simpcoDisplayBlock(vnode.attrs.block, vnode.attrs.props);

        const isWrapped = typeof vnode.attrs.wrapper === 'object';

        return m('div', {class: 'elements__group__item__elements__element'}, [
            m('div', {class: 'elements__group__item__elements__element__desc'}, vnode.attrs.desc),
            isWrapped ? wrappedBlock() : simpleBlock(),
            m('pre', {}, 'simpcoDisplayBlock(' + vnode.attrs.block + ', ' + JSON.stringify(vnode.attrs.props) + ')')
        ])
    }
}

simpcoRegisterBlock('admin.storybook.elements.element', StoryBookElement);