import {m} from 'mithril';
import { simpcoDisplayBlock, simpcoRegisterBlock } from 'simpco-mithril/base';

const GroupElements = {
    view: (vnode) => {
        return m('div', {class: 'elements__group'}, [ 
            m('h2', {class: 'elements__group__title'}, vnode.attrs.name),
            vnode.attrs.items?.map(item => simpcoDisplayBlock('admin.storybook.elements.group.item', item))
        ])
    }
}

simpcoRegisterBlock('admin.storybook.elements.group', GroupElements);