import {m} from 'mithril';
import { simpcoDisplayBlock, simpcoRegisterBlock } from 'simpco-mithril/base';

const GroupItem = {
    view: (vnode) => {
        return m('div', {class: 'elements__group__item', id: vnode.attrs.name}, [
            m('h3', {}, vnode.attrs.name),
            m('div', {class: 'elements__group__item__elements'}, vnode.attrs.elements?.map(el => simpcoDisplayBlock('admin.storybook.elements.element', el)))
        ])
    }
}

simpcoRegisterBlock('admin.storybook.elements.group.item', GroupItem    )