import { m } from 'mithril';
import { simpcoDisplayBlock, simpcoRegisterBlock } from 'simpco-mithril/base';

const PureUWrapper = {
  view: (vnode) => {
    return m(
      'div',
      { class: 'pure-grid' },
      m(
        'div',
        { class: 'pure-u-' + vnode.attrs.size },
        simpcoDisplayBlock(vnode.attrs.block, vnode.attrs.props)
      )
    );
  },
};

simpcoRegisterBlock('admin.storybook.wrapper.pure-u', PureUWrapper);
