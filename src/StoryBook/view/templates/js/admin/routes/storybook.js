import {m} from 'mithril';
import { simpcoDisplayBlock, simpcoRegisterBlock, simpcoRegisterRoute } from 'simpco-mithril/base';
import storyBookMapping from './config';
import './storybook.scss';

const StoryBookComponent = {
    view: () => m('div', {class: 'storybook__container pure-g'}, [
        m('div', {class: 'pure-u-1-4'}, storyBookMapping.map((group) => simpcoDisplayBlock('admin.storybook.menu.group', group))),
        m('div', {class: 'pure-u-3-4'}, storyBookMapping.map((group) => simpcoDisplayBlock('admin.storybook.elements.group', group)))
    ])
}

simpcoRegisterBlock('storybook.index', StoryBookComponent);
simpcoRegisterRoute('/storybook', 'storybook.index');