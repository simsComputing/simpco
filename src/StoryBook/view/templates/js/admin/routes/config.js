import MenuTree from './config/menu-tree';

export default [
  {
    name: 'Atoms',
    items: [
      {
        name: 'Logo',
        elements: [
          {
            block: 'admin.atom.logo',
            props: { width: 100, height: 100 },
            desc: 'Framework logo',
          },
        ],
      },
      {
        name: 'Buttons',
        elements: [
          {
            block: 'admin.atom.button',
            props: { text: 'Button' },
            desc: 'Button',
          },
          {
            block: 'admin.atom.button',
            props: { type: 'primary', text: 'Button' },
            desc: 'Primary button',
          },
          {
            block: 'admin.atom.button',
            props: { type: 'secondary', text: 'Button' },
            desc: 'Secondary button',
          },
          {
            block: 'admin.atom.button',
            props: { type: 'accent', text: 'Button' },
            desc: 'Accent button',
          },
        ],
      },
      {
        name: 'Text Input',
        elements: [
          {
            block: 'admin.atom.input.text',
            props: { placeholder: 'Enter text...' },
            desc: 'Text Input (must absolutely be container in a form and fieldset for css to be applied)',
          },
          {
            block: 'admin.atom.input.text',
            props: { display: 'secondary', placeholder: 'Enter text...' },
            desc: 'Same rule as before. We added secondary display.',
          },
        ],
      },
      {
        name: 'Input Label',
        elements: [
          {
            block: 'admin.atom.input.label',
            props: { text: 'My input label' },
            desc: 'Label that works with all inputs. Should be contained in a fieldset (molecule)',
          },
        ],
      },
      {
        name: 'Password',
        elements: [
          {
            block: 'admin.atom.input.password',
            props: { placeholder: 'your password' },
            desc: 'Password Input (must absolutely be container in a form and fieldset for css to be applied)',
          },
          {
            block: 'admin.atom.input.password',
            props: { display: 'your password', placeholder: 'your password' },
            desc: 'Same rule as before. We added secondary display.',
          },
        ],
      },
      {
        name: 'Checkbox',
        elements: [
          {
            block: 'admin.atom.input.checkbox',
            props: {},
            desc: 'Simple checkbox nbv',
          },
        ],
      },
      {
        name: 'Radio Button',
        elements: [],
      },
      {
        name: 'Dropdown',
        elements: [],
      },
      {
        name: 'Icon',
        elements: [],
      },
      {
        name: 'Spinner',
        elements: [
          { block: 'admin.atom.spinner', props: {}, desc: 'Simple spinner' },
        ],
      },
      {
        name: 'Link',
        elements: [],
      },
      {
        name: 'Image',
        elements: [],
      },
      {
        name: 'Headings',
        elements: [],
      },
      {
        name: 'Paragraphs',
        elements: [],
      },
      {
        name: 'Navigation bar',
        elements: [],
      },
      {
        name: 'List item',
        elements: [],
      },
      {
        name: 'Loading Spinner',
        elements: [],
      },
      {
        name: 'Social media',
        elements: [],
      },
      {
        name: 'Sidebar',
        elements: [],
      },
      {
        name: 'Badge',
        elements: [],
      },
      {
        name: 'Tag',
        elements: [],
      },
    ],
  },
  {
    name: 'Molecules',
    items: [
      {
        name: 'Fieldset',
        elements: [
          {
            block: 'admin.molecule.fieldset',
            desc: 'Constructs an entire fieldset with label and input. input prop can be any of the story book inputs. Also you can use the inputProps prop, to set input props.',
            props: { label: 'Example fieldset', input: 'text' },
          },
        ],
      },
      {
        name: 'Flash Message',
        elements: [
          {
            block: 'admin.molecule.flashMessage',
            desc: 'Flash Message to show user a temporary success message',
            props: { type: 'success', text: 'What you were doing did work !' },
          },
          {
            block: 'admin.molecule.flashMessage',
            desc: 'Flash Message to show user a temporary error message',
            props: { type: 'error', text: 'What you were doing did work !' },
          },
          {
            block: 'admin.molecule.flashMessage',
            desc: 'Flash Message to show user a temporary message',
            props: {
              text: 'What you were doing did not matter that much',
            },
          },
        ],
      },
    ],
  },
  {
    name: 'Organisms',
    items: [
      {
        name: 'Navbar',
        elements: [
          {
            block: 'admin.organism.navbar',
            desc: 'Navbar, meant to be set on top of page, included in layout',
            props: {},
          },
        ],
      },
      {
        name: 'Menu',
        elements: [
          {
            block: 'admin.organism.menu',
            desc: 'Menu, included in layout',
            props: { name: 'Simpco', items: MenuTree },
            wrapper: {
              block: 'admin.storybook.wrapper.pure-u',
              props: { size: '4-24' },
            },
          },
        ],
      },
    ],
  },
];
