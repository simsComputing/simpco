<?php

namespace Simpco\Security\Test\Jwt;

use PHPUnit\Framework\TestCase;
use Simpco\Security\Hash\HS256;
use Simpco\Security\Jwt\Token;

/**
 * @covers \Simpco\Security\Jwt\Token
 */
class TokenTest extends TestCase
{
    public function testTokenStringFromData(): void
    {
        $expectedToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0IiwibmFtZSI6IlNpbW9uIiwiYWRtaW4iOmZhbHNlLCJleHBpcmF0aW9uIjoiMTI5MDAifQ==.Mzg4MjM1NTFhMGIwMDliMmRmYTRkNjcwYTcxNmEwMTE2NTQ4MjYxYzAzYzdmMjhjYWYyMWZlZGE4OWIxY2ZkMQ==';
        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT'
        ];
        $payload = [
            "sub" => '1234',
            "name" => 'Simon',
            "admin" => false,
            "expiration" => "12900"
        ];
        $secret = 'mysupersecret';
        $token = new Token($header, $payload, $secret, new HS256());
        $this->assertEquals($expectedToken, $token->__toString());
    }

    public function testTokenStringFailFromDifferentSecret(): void
    {
        $expectedToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0IiwibmFtZSI6IlNpbW9uIiwiYWRtaW4iOmZhbHNlLCJleHBpcmF0aW9uIjoiMTI5MDAifQ==.Mzg4MjM1NTFhMGIwMDliMmRmYTRkNjcwYTcxNmEwMTE2NTQ4MjYxYzAzYzdmMjhjYWYyMWZlZGE4OWIxY2ZkMQ==';
        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT'
        ];
        $payload = [
            "sub" => '1234',
            "name" => 'Simon',
            "admin" => false,
            "expiration" => "12900"
        ];
        $secret = 'changesecret';
        $token = new Token($header, $payload, $secret, new HS256());
        $this->assertNotEquals($expectedToken, $token->__toString());
    }

    public function testIsValid()
    {
        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT'
        ];
        $payload = [
            "sub" => '1234',
            "name" => 'Simon',
            "admin" => false,
            "expiration" => "12900"
        ];
        $secret = 'mysupersecret';
        $token = new Token($header, $payload, $secret, new HS256());

        $this->assertTrue($token->isValid('Mzg4MjM1NTFhMGIwMDliMmRmYTRkNjcwYTcxNmEwMTE2NTQ4MjYxYzAzYzdmMjhjYWYyMWZlZGE4OWIxY2ZkMQ=='));
    }

    public function testIsNotValid()
    {
        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT'
        ];
        $payload = [
            "sub" => '1234',
            "name" => 'Simon',
            "admin" => false,
            "expiration" => "12900"
        ];
        $secret = 'changesecret';
        $token = new Token($header, $payload, $secret, new HS256());

        $this->assertFalse($token->isValid('random=='));
    }
}