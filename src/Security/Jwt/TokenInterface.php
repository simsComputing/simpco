<?php

namespace Simpco\Security\Jwt;

interface TokenInterface
{
    public function __toString(): string;

    public function isValid(string $crypt): bool;

    public function getHeader(): array;

    public function getPayload(): array;

    public function getSignature(): string;
}