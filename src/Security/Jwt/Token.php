<?php

namespace Simpco\Security\Jwt;

use Simpco\Security\Hash\AlgorithmInterface;
use Simpco\Security\Hash\HS256;

class Token implements TokenInterface
{
    const ALLOWED_ALGOS = ['HS256'];

    private array $header;

    private array $payload;

    private string $secret;

    private array $algos = [];

    private ?string $signature = null;

    public function __construct(
        array $header,
        array $payload,
        string $secret,
        HS256 $hS256
    ) {
        $this->header = $header;
        $this->payload = $payload;
        $this->secret = $secret;
        $this->algos = [
            'HS256' => $hS256
        ];
    }

    public function __toString(): string
    {
        return implode('.', [$this->getHeaderAsString(), $this->getPayloadAsString(), $this->getSignature()]);
    }

    public function isValid(string $crypt): bool
    {
        return $crypt === $this->getSignature();
    }

    public function getHeader(): array
    {
        return $this->header;
    }

    private function getHeaderAsString(): string
    {
        return base64_encode(json_encode($this->header));
    }

    private function getPayloadAsString(): string
    {
        return base64_encode(json_encode($this->payload));
    }

    public function getPayload(): array
    {
        return $this->payload;
    }

    public function getSignature(): string
    {
        if ($this->signature !== null) {
            return $this->signature;
        }

        if (!isset($this->header['alg'])) {
            // THROW RIGHT EXCEPTION
        }

        if (!in_array($this->header['alg'], self::ALLOWED_ALGOS)) {
            // THROW RIGHT EXCEPTION
        }

        /** @var AlgorithmInterface $algo */
        $algo = $this->algos[$this->header['alg']];
        $header = $this->getHeaderAsString();
        $payload = $this->getPayloadAsString();
        $signature = $algo->hash($header . '.' . $payload, $this->secret);
        $this->signature = base64_encode($signature);
        return $this->signature;
    }

    public function getUserId(): int
    {
        return (int)$this->payload['sub'];
    }
}