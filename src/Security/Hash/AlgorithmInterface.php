<?php

namespace Simpco\Security\Hash;

interface AlgorithmInterface
{
    public function hash(string $decrypted, string $secret): string;
    public function validate(string $encrypted, string $decrypted, string $secret): bool;
}