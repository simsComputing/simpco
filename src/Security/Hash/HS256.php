<?php

namespace Simpco\Security\Hash;

class HS256 implements AlgorithmInterface
{
    public function hash(string $decrypted, string $secret): string
    {
        return hash_hmac('sha256', $decrypted, $secret);
    }

    public function validate(string $encrypted, string $decrypted, string $secret): bool
    {
        $computed = $this->hash($decrypted, $secret);
        return hash_equals($computed, $encrypted);
    }
}
