<?php

namespace Simpco\Framework\Exception;

/**
 * @codeCoverageIgnore
 */
class MissingParameterException extends \Exception
{
    public function __construct(string $paramName)
    {
        parent::__construct('Parameter ' . $paramName . ' is missing in di');
    }   
}