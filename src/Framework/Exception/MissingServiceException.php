<?php

namespace Simpco\Framework\Exception;

/**
 * @codeCoverageIgnore
 */
class MissingServiceException extends \Exception
{
    public function __construct(string $serviceName, array $instantiationStack = [])
    {
        $message = 'Service ' . $serviceName . ' is missing in di.' . PHP_EOL;
        $message .= 'Stack is : ' . PHP_EOL;
        foreach ($instantiationStack as $stackItem) {
            $message .= ' - ' . $stackItem . PHP_EOL;
        }

        parent::__construct($message);
    }
}
