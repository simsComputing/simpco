<?php

namespace Simpco\Framework\Exception;

/**
 * @codeCoverageIgnore
 */
class CircularDependencyException extends \Exception
{
    public function __construct(array $stack)
    {
        $message = 'Found circular dependency when instantiating ' . $stack[0] . '. Stack is : ' . PHP_EOL;
        foreach ($stack as $service) {
            $message .= ' - ' . $service . PHP_EOL;
        }
        parent::__construct($message);
    }   
}