<?php

namespace Simpco\Framework\Exception; 

class RedirectionException extends \Exception
{
    private string $path;

    public function __construct(string $path)
    {
        $this->path = $path;
        parent::__construct('Redirected to path ' . $path);
    }

    public function getPath(): string
    {
        return $this->path;
    }
}