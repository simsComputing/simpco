<?php

namespace Simpco\Framework\Models;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;
use Simpco\Framework\DependencyInjection\ServiceFactoryInterface;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Exception\DependencyInjectionException;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolverInterface;

class RepositoryFactory implements ServiceFactoryInterface
{
    /**
     * @var string
     */
    private string $repositoryClass;

    /**
     * @var string
     */
    private string $entityClass;

    /**
     * @var string|null
     */
    private ?string $alias;

    /**
     * @var EntityRepository|null
     */
    private ?EntityRepository $repository = null;

    /**
     * @param string $repositoryClass
     * @param string $entityClass
     * @param string|null $alias
     */
    public function __construct(
        string $repositoryClass,
        string $entityClass,
        ?string $alias = null
    ) {
        $this->repositoryClass = $repositoryClass;
        $this->entityClass = $entityClass;
        $this->alias = $alias;
    }

    /**
     * @param DependencyInjectionInterface $dependencyInjection
     * @param ServiceArgumentResolverInterface $serviceArgumentResolver
     * @param boolean $forceCreation
     * @return object
     */
    public function getInstance(
        DependencyInjectionInterface $dependencyInjection,
        ServiceArgumentResolverInterface $serviceArgumentResolver,
        bool $forceCreation = false
    ): object {
        if ($this->repository && $forceCreation !== true) {
            return $this->repository;
        }
        $entityManager = $dependencyInjection->getService(EntityManagerInterface::class);
        $this->repository = $entityManager->getRepository($this->entityClass);

        if (get_class($this->repository) !== $this->repositoryClass) {
            throw new DependencyInjectionException('You probably forgot to configure your repository on your entity for ' . $this->repositoryClass);
        }

        return $this->repository;
    }

    /**
     * @return string
     */
    public function getNameForRegistration(): string
    {
        return $this->alias ?? $this->repositoryClass;
    }

    /**
     * @param ServiceArgumentModifierInterface $serviceArgumentModifier
     * @return void
     */
    public function modifyArgument(ServiceArgumentModifierInterface $serviceArgumentModifier): void
    {
        throw new DependencyInjectionException('Cannot modify arguments in the RepositoryFactory');
    }

    /**
     * @param string $repositoryClass
     * @param string $entityClass
     * @param string|null $alias
     * @return RepositoryFactory
     */
    public static function createFactory(
        string $repositoryClass,
        string $entityClass,
        ?string $alias = null
    ): RepositoryFactory {
        return new RepositoryFactory($repositoryClass, $entityClass, $alias);
    }
}
