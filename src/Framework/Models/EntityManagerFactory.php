<?php

namespace Simpco\Framework\Models;

use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMSetup;
use Simpco\Framework\DependencyInjection;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolver;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolverInterface;
use Simpco\Framework\DependencyInjection\ServiceFactoryInterface;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Exception\DependencyInjectionException;

class EntityManagerFactory implements ServiceFactoryInterface
{
    private ?EntityManagerInterface $em = null;

    private array $modelsDirs = [];

    public function getInstance(
        DependencyInjectionInterface $dependencyInjection, 
        ServiceArgumentResolverInterface $serviceArgumentResolver,
        bool $forceCreation = false
    ): object {
        if ($this->em !== null) {
            return $this->em;
        }
        $config = ORMSetup::createAttributeMetadataConfiguration(
            $this->modelsDirs,
            true,
        );
        $param = $dependencyInjection->getParam('doctrine.config');
        $connection = DriverManager::getConnection($param, $config);
        $this->em = new EntityManager($connection, $config);
        return $this->em;
    }

    public function getNameForRegistration(): string
    {
        return EntityManagerInterface::class;
    }

    /**
     * @param ServiceArgumentModifierInterface $serviceArgumentModifier
     * @return void
     */
    public function modifyArgument(ServiceArgumentModifierInterface $serviceArgumentModifier): void
    {
        if ($serviceArgumentModifier->getArgumentName() !== 'modelsDirs') {
            throw new DependencyInjectionException('Cannot modify arguments other than modelsDirs for EntityManagerInterface');
        }

        $this->modelsDirs = $serviceArgumentModifier->modify($this->modelsDirs);
    }

    /**
     * @param array $modelsDirs
     * @return EntityManagerFactory
     */
    public static function create(): EntityManagerFactory
    {
        return new EntityManagerFactory();
    }

    public function setModelsDirs(array $modelsDirs)
    {
        $this->modelsDirs = $modelsDirs;
    }
}
