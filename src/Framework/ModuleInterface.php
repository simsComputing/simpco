<?php

namespace Simpco\Framework;

use Simpco\Utils\Sort\TopologicalSort\WithClassDependenciesInterface;

interface ModuleInterface extends WithClassDependenciesInterface
{
    public function getServices(): array;

    public function getParams(): array;

    public function getRoutes(): array;

    public function getModelsDir(): ?string;

    public function getViewDir(): ?string;

    public function getServiceModifiers(): array;
}
