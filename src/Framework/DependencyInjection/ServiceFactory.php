<?php

namespace Simpco\Framework\DependencyInjection;

use Simpco\Form\Form;
use Simpco\Framework\DependencyInjection;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Exception\DependencyInjectionException;
use Simpco\Framework\Test\DependencyInjection\Fake\WithArrayDependency;
use Simpco\Framework\View\HeadTagPoolInterface;

class ServiceFactory implements ServiceFactoryInterface, DefaultServiceFactoryInterface
{
    private string $serviceClass;

    private array $arguments;

    private ?string $alias = null;

    private ?object $singleton = null;

    /**
     * @param string $serviceClass
     * @param array $arguments
     * @param string $alias
     */
    public function __construct(
        string $serviceClass,
        array $arguments = [],
        string $alias = null
    ) {
        $this->serviceClass = $serviceClass;
        $this->arguments = $arguments;
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getNameForRegistration(): string
    {
        if ($this->alias === null) {
            return $this->serviceClass;
        }
        return $this->alias;
    }

    /**
     * @param DependencyInjection $dependencyInjection
     * @return object
     */
    public function getInstance(
        DependencyInjectionInterface $dependencyInjection,
        ServiceArgumentResolverInterface $serviceArgumentResolver,
        bool $forceCreation = false
    ): object {
        if ($this->singleton !== null && !$forceCreation) {
            return $this->singleton;
        }
        $resolvedArgs = [];
        $arguments = $serviceArgumentResolver->resolve(
            $this->serviceClass,
            $this->arguments,
            $this->getNameForRegistration()
        );
        foreach ($arguments as $argument) {
            $resolvedArgs[] = $this->getArgument($argument, $dependencyInjection);
        }
        $service = new $this->serviceClass(...$resolvedArgs);
        $this->singleton = $service;
        return $this->singleton;
    }

    private function getArgument($argument, DependencyInjection $dependencyInjection)
    {
        if (is_array($argument)) {
            $args = [];
            foreach ($argument as $key => $argItem) {
                $args[$key] = $this->getArgument($argItem, $dependencyInjection);
            }
            return $args;
        }

        if (is_object($argument)) {
            return $argument;
        }

        if (!is_string($argument)) {
            throw new DependencyInjectionException('Could not resolve argument of type ' . gettype($argument) . ' for class ' . $this->serviceClass);
        }

        if (preg_match('/^%.*%$/', $argument)) {
            return $dependencyInjection->getParam(substr($argument, 1, strlen($argument) - 2));
        }

        if (preg_match('/^__.*__$/', $argument)) {
            return substr($argument, 2, strlen($argument) - 4);
        }

        return $dependencyInjection->getService($argument);
    }

    public function modifyArgument(ServiceArgumentModifierInterface $serviceArgumentModifier): void
    {
        $argumentName = $serviceArgumentModifier->getArgumentName();
        $currentArgumentValue = isset($this->arguments[$argumentName]) ? $this->arguments[$argumentName] : null;
        $this->arguments[$argumentName] = $serviceArgumentModifier->modify($currentArgumentValue);
    }

    /**
     * @param string $serviceClass
     * @param array $arguments
     * @param string|null $alias
     * @return ServiceFactory
     */
    public static function createFactory(
        string $serviceClass,
        array $arguments = [],
        string $alias = null
    ): ServiceFactory {
        return new ServiceFactory($serviceClass, $arguments, $alias);
    }

    public static function bashFactory(array $definitions)
    {
        $result = [];
        foreach ($definitions as $definition) {
            if (!isset($definition[1])) {
                $definition[1] = [];
            }
            if (!isset($definition[2])) {
                $definition[2] = null;
            }
            $result[] = ServiceFactory::createFactory($definition[0], $definition[1], $definition[2]);
        }
        return $result;
    }
}
