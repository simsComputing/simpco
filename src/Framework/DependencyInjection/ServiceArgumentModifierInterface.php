<?php

namespace Simpco\Framework\DependencyInjection;

interface ServiceArgumentModifierInterface
{
    public function getArgumentName(): string;

    public function modify($currentArgumentValue);
}
