<?php

namespace Simpco\Framework\DependencyInjection\ServiceFactory;

use Simpco\Framework\DependencyInjection;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolver;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolverInterface;
use Simpco\Framework\DependencyInjection\ServiceFactoryInterface;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Exception\DependencyInjectionException;

class FakeFactory implements ServiceFactoryInterface
{
    private object $instance;

    private string $nameForRegistration;

    public function __construct(
        object $instance,
        string $nameForRegistration
    ) {
        $this->instance = $instance;
        $this->nameForRegistration = $nameForRegistration;
    }

    public function getInstance(
        DependencyInjectionInterface $dependencyInjection, 
        ServiceArgumentResolverInterface $serviceArgumentResolver,
        bool $forceCreation = false
    ): object {
        return $this->instance;
    }

    public function getNameForRegistration(): string
    {
        return $this->nameForRegistration;
    }

    public function modifyArgument(ServiceArgumentModifierInterface $serviceArgumentModifier): void
    {
        throw new DependencyInjectionException('Cannot modify argument of service ' . $this->getNameForRegistration());
    }
}
