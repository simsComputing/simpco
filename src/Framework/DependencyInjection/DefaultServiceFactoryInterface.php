<?php

namespace Simpco\Framework\DependencyInjection;

use Simpco\Framework\DependencyInjectionInterface;

interface DefaultServiceFactoryInterface
{
    public static function createFactory(string $serviceClass, array $arguments = [], string $alias = null): ServiceFactoryInterface;
}
