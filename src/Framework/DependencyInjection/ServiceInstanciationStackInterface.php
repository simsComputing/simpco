<?php

namespace Simpco\Framework\DependencyInjection;

use Simpco\Framework\Exception\CircularDependencyException;

interface ServiceInstanciationStackInterface
{
        /**
     * @param string $requestedService
     * @return void
     */
    public function getStackPosition(string $requestedService);

    /**
     * @throws CircularDependencyException
     * @param string $requestedService
     * @return void
     */
    public function checkForCircularDependency(string $requestedService);

    public function unregisterDependencyStack(string $resolvedService): void;

    public function getStack(): array;

    public static function create(): ServiceInstanciationStack;
}
