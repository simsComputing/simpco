<?php

namespace Simpco\Framework\DependencyInjection;

use Simpco\Framework\DependencyInjectionInterface;

interface ServiceFactoryInterface
{
    public function getInstance(
        DependencyInjectionInterface $dependencyInjection, 
        ServiceArgumentResolverInterface $serviceArgumentResolver,
        bool $forceCreation = false
    ): object;

    public function getNameForRegistration(): string;

    public function modifyArgument(ServiceArgumentModifierInterface $serviceArgumentModifier): void;
}
