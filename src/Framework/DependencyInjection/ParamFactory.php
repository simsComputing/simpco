<?php

namespace Simpco\Framework\DependencyInjection;

class ParamFactory implements ParamFactoryInterface
{
    private string $name;

    private $value = null;

    /**
     * @param string $name
     * @param string|null $value
     */
    public function __construct(string $name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getNameForRegistration(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    public static function create(string $name, $value)
    {
        return new ParamFactory($name, $value);
    }
}
