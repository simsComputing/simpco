<?php

namespace Simpco\Framework\DependencyInjection;

use ReflectionClass;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionUnionType;
use Simpco\Form\Form;

class ServiceArgumentResolver implements ServiceArgumentResolverInterface
{
    private array $cacheMapping = [];

    public function resolve(string $className, array $explicitArguments, string $instanceId)
    {
        if (isset($this->cacheMapping[$instanceId])) {
            return $this->cacheMapping[$instanceId];
        }

        $reflection = new ReflectionClass($className);
        $arguments = [];
        if ($reflection->getConstructor() === null) {
            return $arguments;
        }
        foreach ($reflection->getConstructor()->getParameters() as $parameter) {
            $position = $parameter->getPosition();
            $name = $parameter->getName();
            $typeName = $parameter->getType()->getName();
            $isArray = $this->declaresArray($parameter);
            if (isset($explicitArguments[$name])) {
                $arguments[$position] = $explicitArguments[$name];
            } elseif (isset($explicitArguments[$position])) {
                $arguments[$position] = $explicitArguments[$position];
            } elseif ($isArray) {
                $arguments[$position] = [];
            } else {
                $arguments[$position] = $typeName;
            }
        }
        ksort($arguments);
        $this->cacheMapping[$instanceId] = $arguments;
        return $arguments;
    }

    public static function create(): ServiceArgumentResolverInterface
    {
        return new ServiceArgumentResolver();
    }

    private function declaresArray(ReflectionParameter $reflectionParameter): bool
    {
        $reflectionType = $reflectionParameter->getType();

        if (!$reflectionType) {
            return false;
        }

        $types = $reflectionType instanceof ReflectionUnionType
            ? $reflectionType->getTypes()
            : [$reflectionType];

        return in_array('array', array_map(fn (ReflectionNamedType $t) => $t->getName(), $types));
    }
}
