<?php

namespace Simpco\Framework\DependencyInjection;

use Simpco\Framework\Exception\CircularDependencyException;

class ServiceInstanciationStack implements ServiceInstanciationStackInterface
{
    private array $serviceInstanciationStack = [];

    public function getStackPosition(string $requestedService)
    {
        return array_search($requestedService, $this->serviceInstanciationStack); 
    }

    public function checkForCircularDependency(string $requestedService)
    {
        $isCircular = in_array($requestedService, $this->serviceInstanciationStack);
        $this->registerDependencyStack($requestedService);
        if ($isCircular) {
            throw new CircularDependencyException($this->serviceInstanciationStack);
        }
    }

    private function registerDependencyStack(string $requestedService): void
    {
        $this->serviceInstanciationStack[] = $requestedService;
    }

    public function unregisterDependencyStack(string $resolvedService): void
    {
        $index = array_search($resolvedService, $this->serviceInstanciationStack);
        unset($this->serviceInstanciationStack[$index]);
    }

    public function getStack(): array 
    {
        return $this->serviceInstanciationStack;
    }

    public static function create(): ServiceInstanciationStack
    {
        return new ServiceInstanciationStack();
    }
}
