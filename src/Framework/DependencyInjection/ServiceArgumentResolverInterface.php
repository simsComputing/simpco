<?php

namespace Simpco\Framework\DependencyInjection;

interface ServiceArgumentResolverInterface
{
    public function resolve(string $className, array $explicitArguments, string $instanceId);

    public static function create(): ServiceArgumentResolverInterface;
}
