<?php

namespace Simpco\Framework\DependencyInjection;

interface ParamFactoryInterface
{
    public function getValue();

    public function getNameForRegistration(): string;
}
