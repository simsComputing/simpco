<?php

namespace Simpco\Framework\DependencyInjection;

use Simpco\Framework\DependencyInjection;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Exception\DependencyInjectionException;

class DependencyInjectionFactory implements ServiceFactoryInterface
{
    public function getInstance(
        DependencyInjectionInterface $dependencyInjection,
        ServiceArgumentResolverInterface $resolver,
        bool $forceCreate = false
    ): object {
        return $dependencyInjection;
    }

    public function getNameForRegistration(): string
    {
        return DependencyInjection::class;
    }

    public static function create(): DependencyInjectionFactory
    {
        return new DependencyInjectionFactory();
    }

    public function modifyArgument(ServiceArgumentModifierInterface $serviceArgumentModifier): void
    {
        throw new DependencyInjectionException('Cannot modify arguments for main dependency injection factory');
    }
}
