<?php

namespace Simpco\Framework\DependencyInjection;
use Simpco\Framework\Exception\DependencyInjectionException;

/**
 * Represents a ServiceFactoryInterface's factory.
 * Named it provider because factory logic is actually 
 * contained inside ServiceFactoryInterface::createFactory
 */
class ServiceFactoryProvider
{
    private string $serviceFactoryClass;

    public function __construct(string $serviceFactoryClass)
    {
        if (!class_exists($serviceFactoryClass)) {
            throw new DependencyInjectionException(
                'Could not find provided service factory class : ' .$serviceFactoryClass
            );
        }

        if (!in_array(DefaultServiceFactoryInterface::class, class_implements($serviceFactoryClass))) {
            throw new DependencyInjectionException('Service factory class must implement ServiceFactoryInterface');
        }

        $this->serviceFactoryClass = $serviceFactoryClass;
    }

    public function createFactory(
        string $serviceClass,
        array $arguments = [],
        string $alias = null
    ): ServiceFactoryInterface
    {
        return $this->serviceFactoryClass::createFactory(
            $serviceClass,
            $arguments,
            $alias
        );
    }

    public static function create(string $serviceFactoryClass): ServiceFactoryProvider
    {
        return new ServiceFactoryProvider($serviceFactoryClass);
    }
}