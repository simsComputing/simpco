<?php

namespace Simpco\Framework\DependencyInjection\ServiceArgumentModifier;

use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;

class ReplaceArg implements ServiceArgumentModifierInterface
{
    private string $argumentName;

    private $replacementValue;

    public function __construct(
        string $argumentName,
        $replacementValue
    ) {
        $this->argumentName = $argumentName;
        $this->replacementValue = $replacementValue;
    }

    public function getArgumentName(): string
    {
        return $this->argumentName;
    }

    public function modify($currentArgumentValue)
    {
        return $this->replacementValue;
    }

    public static function create(string $argumentName, $replacementValue): ReplaceArg
    {
        return new ReplaceArg($argumentName, $replacementValue);
    }
}