<?php

namespace Simpco\Framework\DependencyInjection\ServiceArgumentModifier;

use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;
use Simpco\Framework\Exception\DependencyInjectionException;

class ArrayPush implements ServiceArgumentModifierInterface
{
    private string $argumentName;

    private array $injectedValues;

    public function __construct(
        string $argumentName,
        array $injectedValues
    ) {
        $this->argumentName = $argumentName;
        $this->injectedValues = $injectedValues;
    }

    public function getArgumentName(): string
    {
        return $this->argumentName;
    }

    public function modify($currentArgumentValue)
    {
        if ($currentArgumentValue === null) {
            $currentArgumentValue = [];
        }

        if (!is_array($currentArgumentValue)) {
            throw new DependencyInjectionException('Expected argument' . $this->argumentName . 'to be array. Instead argument is ' . gettype($currentArgumentValue));
        }
        return [...$currentArgumentValue, ...$this->injectedValues];
    }

    public static function create(string $argumentName, array $injectedValues): ArrayPush
    {
        return new ArrayPush($argumentName, $injectedValues);
    }
}