<?php

namespace Simpco\Framework\View;

use Simpco\Framework\Exception\MissingAssetException;
use Simpco\Framework\View\HeadTag\HtmlPrinter;
use Simpco\Framework\ViewInterface;

class Includer implements IncluderInterface
{
    const MAIN_FILE = __DIR__ . '/index.phtml';

    private ViewPoolInterface $viewPool;

    public function __construct(
        ViewPoolInterface $viewPool
    ) {
        $this->viewPool = $viewPool;
    }

    public function includeMainFile(ViewInterface $view, HtmlPrinter $printer): string
    {
        $path = $this->viewPool->resolveMain($view->getMainFile());

        if ($path === null) {
            throw new MissingAssetException('Could not find main file ' . $view->getMainFile());
        }

        ob_start();
        require $path;
        $result = ob_get_clean();
        return $result;
    }

    public function includeTemplate(ViewInterface $view): string
    {
        $templateFullPath = $this->viewPool->resolveTemplate($view->getTemplate());
        if ($templateFullPath === null) {
            throw new MissingAssetException('Could not find template ' . $view->getTemplate());
        }
        ob_start();
        require $templateFullPath;
        $templateContent = ob_get_clean();
        return $templateContent;
    }

    public function includeLayout(ViewInterface $view): string
    {
        $layoutFullPath = $this->viewPool->resolveLayout($view->getLayout());
        if ($layoutFullPath === null) {
            throw new MissingAssetException('Could not find layout ' . $view->getLayout());
        }
        ob_start();
        require $layoutFullPath;
        $layoutContent = ob_get_clean();
        return $layoutContent;
    }
}
