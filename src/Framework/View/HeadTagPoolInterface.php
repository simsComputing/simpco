<?php

namespace Simpco\Framework\View;

interface HeadTagPoolInterface
{
    public function getList(): array;
}