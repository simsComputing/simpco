<?php

namespace Simpco\Framework\View;

class HeadTag implements HeadTagInterface
{
    private string $tagName;

    private array $metas;

    private bool $autoClose;

    /**
     * @param string $tagName
     * @param array $metas
     * @param bool autoClose
     */
    public function __construct(
        string $tagName,
        array $metas,
        bool $autoClose
    ) {
        $this->tagName = $tagName;
        $this->metas = $metas;
        $this->autoClose = $autoClose;
    }

    public function getTagName(): string
    {
        return $this->tagName;
    }

    public function getMetas(): array
    {
        return $this->metas;
    }

    public function isAutoClose(): bool
    {
        return $this->autoClose;
    }

    /**
     * @param string $name
     * @param array $metas
     * @return HeadTag
     */
    public static function create(string $name, array $metas, bool $autoClose = true): HeadTag
    {
        return new HeadTag($name, $metas, $autoClose);
    }
}