<?php

namespace Simpco\Framework\View\HeadTag;

use Simpco\Framework\View\HeadTagInterface;

interface PrinterInterface
{
    public function print(HeadTagInterface $headTag): string;
}