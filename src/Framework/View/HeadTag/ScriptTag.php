<?php

namespace Simpco\Framework\View\HeadTag;

use Simpco\Framework\View\HeadTag;
use Simpco\Framework\View\HeadTagInterface;

class ScriptTag {
    public static function create(string $src): HeadTagInterface
    {
        return HeadTag::create('script', ['src' => $src], false);
    }

    public static function createForLocalFile(string $path): HeadTagInterface
    {
        return HeadTag::create('script', ['src' => '/assets/js/' . $path], false);
    }
}