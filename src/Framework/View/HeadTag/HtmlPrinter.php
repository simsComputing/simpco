<?php

namespace Simpco\Framework\View\HeadTag;

use Simpco\Framework\View\HeadTagInterface;

class HtmlPrinter implements PrinterInterface
{
    public function print(HeadTagInterface $headTag): string
    {
        $tagName = $headTag->getTagName();
        $html = '<' . $headTag->getTagName() . '';

        foreach ($headTag->getMetas() as $metaName => $metaValue) {
            $html .= " $metaName=\"$metaValue\"";
        }

        if (!$headTag->isAutoClose()) {
            $html .= "></$tagName>";
        } else {
            $html .= ' />';
        }

        return $html;
    }
}