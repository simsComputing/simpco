<?php

namespace Simpco\Framework\View;

use Simpco\Framework\Router\RouteInterface;
use Simpco\Framework\View;
use Simpco\Framework\View\HeadTag\HtmlPrinter;
use Simpco\Framework\ViewInterface;

interface IncluderInterface
{
    public function includeMainFile(ViewInterface $view, HtmlPrinter $printer): string;

    public function includeTemplate(ViewInterface $view): string;

    public function includeLayout(ViewInterface $view): string;
}
