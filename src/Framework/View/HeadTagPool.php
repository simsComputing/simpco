<?php

namespace Simpco\Framework\View;

class HeadTagPool implements HeadTagPoolInterface
{
    /**
     * @var HeadTagInterface[]
     */
    private array $headTags = [];

    public function __construct(
        array $headTags
    ) {
        array_map([$this, 'registerHeadTag'], $headTags);
    }

    public function getList(): array
    {
        return $this->headTags;
    }

    private function registerHeadTag(HeadTagInterface $headTag): void
    {
        $this->headTags[] = $headTag;
    }
}