<?php

namespace Simpco\Framework\View;

interface ViewPoolInterface
{
    public function resolveMain(string $mainName): ?string;
    
    public function resolveTemplate(string $templateName): ?string;

    public function resolveLayout(string $layoutName): ?string;
}