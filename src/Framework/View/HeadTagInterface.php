<?php

namespace Simpco\Framework\View;

interface HeadTagInterface
{
    public function getTagName(): string;

    public function getMetas(): array;

    public function isAutoClose(): bool;
}