<?php

namespace Simpco\Framework\View;

use Simpco\Framework\FileSystem\Scanner;

class Pool implements ViewPoolInterface
{
    const TEMPLATE_FOLDER = '/templates/';

    const LAYOUT_FOLDER = '/layouts/';

    const MAIN_FOLDER = '/mains/';

    /**
     * @var array
     */
    private array $viewDirs;

    /**
     * @var Scanner
     */
    private Scanner $scanner;

    public function __construct(
        array $viewDirs,
        Scanner $scanner
    ) {
        $this->viewDirs = $viewDirs;
        $this->scanner = $scanner;
    }

    /**
     * @param string $mainName
     * @return string|null
     */
    public function resolveMain(string $mainName): ?string
    {
        return $this->resolveFileExists($mainName, self::MAIN_FOLDER);
    }

    /**
     * @param string $templateName
     * @return string|null
     */
    public function resolveTemplate(string $templateName): ?string
    {
        return $this->resolveFileExists($templateName, self::TEMPLATE_FOLDER);
    }

    /**
     * @param string $layoutName
     * @return string|null
     */
    public function resolveLayout(string $layoutName): ?string
    {
        return $this->resolveFileExists($layoutName, self::LAYOUT_FOLDER);
    }

    /**
     * @param string $fileName
     * @param string $subDir
     * @return string|null
     */
    private function resolveFileExists(string $fileName, string $subDir): ?string
    {
        foreach ($this->viewDirs as $dir) {
            $fullPath = $dir . $subDir . $fileName;
            if ($this->scanner->fileExists($fullPath)) {
                return $fullPath;
            }
        }
        return null;
    }
}
