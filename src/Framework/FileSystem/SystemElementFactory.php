<?php

namespace Simpco\Framework\FileSystem;
use Simpco\Framework\Exception\FileSystemException;

class SystemElementFactory
{
    /**
     * @param string $path
     * @return void
     */
    public function create(string $path)
    {
        if (is_file($path)) {
            return new File($path);
        } elseif (is_dir($path)) {
            return new Folder($path);
        } else {
            throw new FileSystemException('Could not determine what type of element ' . $path . ' is');
        }
    }
}
