<?php

namespace Simpco\Framework\FileSystem;

class Glob
{
    private SystemElementFactory $systemElementFactory;

    public function __construct(
        SystemElementFactory $systemElementFactory
    ) {
        $this->systemElementFactory = $systemElementFactory;
    }

    /**
     * @param string $pattern
     * @param integer|null $flags
     * @return array
     */
    public function execute(string $pattern, ?int $flags = 0): array
    {
        $result = glob($pattern, $flags);
        $arr = [];

        foreach ($result as $element) {
            $arr[] = $this->systemElementFactory->create($element);
        }

        return $arr;
    }
}