<?php

namespace Simpco\Framework\FileSystem;

use Simpco\Framework\Exception\FileSystemException;

class Folder
{
    private string $folderPath;

    public function __construct(string $folderPath)
    {
        if (!is_dir($folderPath)) {
            throw new FileSystemException('Folder ' . $folderPath . ' does not exist');
        }

        $this->folderPath = $folderPath;
    }

    public function getPath(): string 
    {
        return $this->folderPath;
    }
}