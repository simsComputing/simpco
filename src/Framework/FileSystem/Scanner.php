<?php

namespace Simpco\Framework\FileSystem;

class Scanner
{
    public function fileExists(string $fileName): bool
    {
        return file_exists($fileName);
    }
}