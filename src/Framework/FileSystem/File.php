<?php

namespace Simpco\Framework\FileSystem;

use Simpco\Framework\Exception\FileSystemException;

class File
{
    private string $filePath;

    public function __construct(string $filePath)
    {
        if (!file_exists($filePath)) {
            throw new FileSystemException('File ' . $filePath . ' does not exist');
        }

        if (!is_file($filePath)) {
            throw new FileSystemException('Trying to open a folder as a file : ' . $filePath);
        }

        $this->filePath = $filePath;
    }

    public function getPath(): string 
    {
        return $this->filePath;
    }
}