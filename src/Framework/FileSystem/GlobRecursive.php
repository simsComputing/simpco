<?php

namespace Simpco\Framework\FileSystem;

class GlobRecursive
{
    private Glob $glob;

    public function __construct(
        Glob $glob
    ) {
        $this->glob = $glob;
    }

    /**
     * @param string $pattern
     * @param integer|null $flags
     * @return array
     */
    public function execute(string $pattern, ?int $flags = 0): array
    {
        $arr = $this->glob->execute($pattern, $flags);
        $result = [];
        foreach ($arr as $element) {
            $result[] = $element;
            if ($element instanceof Folder) {
                $result = array_merge($result, $this->execute($element->getPath() . '/*'));
            }
        }

        return $result;
    }
}