<?php 

namespace Simpco\Framework;

use Simpco\Framework\Request\Body;

interface RequestInterface
{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const PATCH = 'PATCH';
    const OPTIONS = 'OPTIONS';
    const DELETE = 'DELETE';

    const ALL_POSSIBLE_METHODS = [
        self::GET,
        self::POST,
        self::PUT,
        self::PATCH,
        self::OPTIONS,
        self::DELETE
    ];

    public function getPath(): string;

    public function getHeader(string $name): ?string;

    public function getMethod(): string;

    public function getBody(): ?Body;
}
