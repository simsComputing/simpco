<?php

namespace Simpco\Framework\Module;

/**
 * @codeCoverageIgnore
 */
trait NoModelsTrait
{
    public function getModelsDir(): ?string
    {
        return null;
    }
}