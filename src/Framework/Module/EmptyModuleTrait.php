<?php

namespace Simpco\Framework\Module;

/**
 * @codeCoverageIgnore
 */
trait EmptyModuleTrait
{
    public function getServices(): array
    {
        return [];
    }

    public function getParams(): array
    {
        return [];
    }

    public function getRoutes(): array
    {
        return [];
    }

    public function getModelsDir(): ?string
    {
        return null;
    }

    public function getViewDir(): ?string
    {
        return null;
    }

    public function getServiceModifiers(): array
    {
        return [];
    }

    public function getDependencies(): array
    {
        return [];
    }
}