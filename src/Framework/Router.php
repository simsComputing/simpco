<?php

namespace Simpco\Framework;

use Simpco\Framework\Exception\RoutingException;
use Simpco\Framework\Router\ModuleRoutesRegistryInterface;
use Simpco\Framework\Router\RouteModifierInterface;
use Simpco\Framework\Router\UrlWildcardInterface;

class Router implements RouterInterface
{
    private array $pathMapping = [];

    private array $routeModifiers;

    const SERVICE_KEY = '__SERVICE_FOR_PATH__';

    const WILDCARD_KEY = '__wildcard__';

    private UrlWildcardInterface $urlWildcard;

    private ModuleRoutesRegistryInterface $moduleRoutesRegistry;

    /**
     * @param RouteModifierInterface[] $modifiers
     */
    public function __construct(
        ModuleRoutesRegistryInterface $moduleRoutesRegistry,
        array $routeModifiers,
        UrlWildcardInterface $urlWildcard
    ) {
        $this->routeModifiers = $routeModifiers;
        $this->urlWildcard = $urlWildcard;
        $this->moduleRoutesRegistry = $moduleRoutesRegistry;
        foreach (RequestInterface::ALL_POSSIBLE_METHODS as $method) {
            $this->pathMapping[$method] = [];
            foreach ($this->moduleRoutesRegistry->getList($method) as $path => $route) {
                $explodedPath = $this->explodePath($path);
                $this->registerExplodedPathInMapping($method, $explodedPath, $route);
            }
        }

    }

    /**
     * @param string $path
     * @return array
     */
    private function explodePath(string $path): array
    {
        return explode('/', $path);
    }

    private function registerExplodedPathInMapping(string $method, array $explodedPath, string $route)
    {
        $currentMapping = &$this->pathMapping[$method];
        $explodedPath = array_filter($explodedPath, [$this, 'filterEmptyParts']);
        foreach ($explodedPath as $pathPart) {
            $pathPart = $this->formatPathPart($pathPart);
            if (!isset($currentMapping[$pathPart])) {
                $currentMapping[$pathPart] = [];
            }
            $currentMapping = &$currentMapping[$pathPart];
        }
        $currentMapping[self::SERVICE_KEY] = $route;
    }

    private function formatPathPart(string $pathPart): string
    {
        if (preg_match('/^\{.+\}$/', $pathPart)) {
            return self::WILDCARD_KEY;
        }

        return $pathPart;
    }

    public function getRoute(RequestInterface $request): string
    {
        $this->urlWildcard->reset();
        $requestedPath = $request->getPath();
        /** @var RouteModifierInterface $modifier  */
        foreach ($this->routeModifiers as $modifier) {
            $requestedPath = $modifier->modify($requestedPath) ?? $requestedPath;
        }
        $explodedRequestedPath = $this->explodePath($requestedPath);
        $explodedRequestedPath = array_filter($explodedRequestedPath, [$this, 'filterEmptyParts']);
        $currentMapping = $this->pathMapping[$request->getMethod()];
        foreach($explodedRequestedPath as $requestedPathPart) {
            if (!isset($currentMapping[$requestedPathPart]) && !isset($currentMapping[self::WILDCARD_KEY])) {
                throw new RoutingException('Could not find route for requested path : ' . $requestedPath);
            } elseif (isset($currentMapping[$requestedPathPart])) {
                $currentMapping = $currentMapping[$requestedPathPart];
            } else {
                $currentMapping = $currentMapping[self::WILDCARD_KEY];
                $this->urlWildcard->push($requestedPathPart);
            }
        }

        if (!isset($currentMapping[self::SERVICE_KEY])) {
            throw new RoutingException('Could not find final service for requested path');
        }

        return $currentMapping[self::SERVICE_KEY];
    }

    private function filterEmptyParts($part) {
        return (bool)$part;
    }
}