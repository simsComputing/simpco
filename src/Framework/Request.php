<?php 

namespace Simpco\Framework;

use PHPUnit\Framework\Attributes\CodeCoverageIgnore;
use Simpco\Framework\Exception\BadRequestException;
use Simpco\Framework\Request\Body;

class Request implements RequestInterface
{
    private string $path;

    private array $headers;

    private string $method;

    private ?Body $body;

    #[CodeCoverageIgnore]
    public function __construct(
        string $path,
        array $headers,
        string $method,
        ?Body $body = null
    ) {
        $this->path = $path;
        $this->headers = $headers;
        $this->body = $body;

        if (!in_array($method, RequestInterface::ALL_POSSIBLE_METHODS)) {
            throw new BadRequestException('Method is not allowed by simpco : ' . $method);
        }

        $this->method = $method;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getHeader(string $name): ?string
    {
        if (!isset($this->headers[$name])) {
            return null;
        }

        return $this->headers[$name];
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getBody(): ?Body
    {
        return $this->body;
    }
}