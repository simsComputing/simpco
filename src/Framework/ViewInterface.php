<?php

namespace Simpco\Framework;

interface ViewInterface
{
    /**
     * @return HeadTagInterface[]
     */
    public function getHeadTags(): array;

    public function getLayout(): string;

    public function getTemplate(): string;

    public function getMainFile(): string;

    public function getData($key);
}
