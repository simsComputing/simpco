<?php

namespace Simpco\Framework;

use Simpco\Framework\Application\Runner\HttpRunner;
use Simpco\Framework\Application\Sender\HttpSender;
use Simpco\Framework\Application\Starter\DIStarter;
use Simpco\Framework\Application\Starter\ModelsStarter;
use Simpco\Framework\Application\Starter\RoutesStarter;
use Simpco\Framework\Application\Starter\ViewsStarter;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolver;
use Simpco\Framework\DependencyInjection\ServiceFactory;
use Simpco\Framework\DependencyInjection\ServiceFactoryProvider;
use Simpco\Framework\DependencyInjection\ServiceInstanciationStack;
use Simpco\Framework\Exception\BootstrapException;
use Simpco\Framework\Router\ModuleRoutesRegistry;

class Bootstrap
{
    public static function http(string $modulesFilePath): Application
    {
        if (!file_exists($modulesFilePath)) {
            throw new BootstrapException('Could not find modules file on path : ' . $modulesFilePath);
        }

        $modules = require_once $modulesFilePath;
        $di = self::di();
        $routesRegistry = new ModuleRoutesRegistry();

        $starters = [
            new DIStarter(),
            new ModelsStarter(),
            new ViewsStarter(),
            new RoutesStarter($routesRegistry)
        ];

        $runner = new HttpRunner();

        $senders = [
            new HttpSender()
        ];

        return Application::create(
            $di,
            $modules,
            $starters,
            $runner,
            $senders
        );
    }

    public static function di(): DependencyInjectionInterface
    {
        $serviceInstanciationStack = ServiceInstanciationStack::create();
        $serviceArgumentResolver = ServiceArgumentResolver::create();
        $serviceFactoryProvider = ServiceFactoryProvider::create(ServiceFactory::class);

        $di = DependencyInjection::create(
            $serviceInstanciationStack,
            $serviceArgumentResolver,
            $serviceFactoryProvider
        );

        return $di;
    }
}
