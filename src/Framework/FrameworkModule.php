<?php

namespace Simpco\Framework;

use Simpco\Framework\Module\NoModelsTrait;
use Simpco\Framework\Router\JavascriptRoute;

class FrameworkModule implements ModuleInterface
{
    use NoModelsTrait;

    public function getServices(): array
    {
        return require_once __DIR__ . '/FrameworkModule/services.php';
    }

    public function getParams(): array
    {
        return [];
    }

    public function getRoutes(): array
    {
        return require_once __DIR__ . '/FrameworkModule/routes.php';
    }

    public function getViewDir(): ?string
    {
        return __DIR__ . '/Module/view';
    }

    public function getServiceModifiers(): array
    {
        return [];
    }

    public function getDependencies(): array
    {
        return [];
    }
}
