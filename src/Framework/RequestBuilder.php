<?php 

namespace Simpco\Framework;

use Simpco\Framework\Exception\HttpException;
use Simpco\Framework\Http\HeaderFormatter;
use Simpco\Framework\Request\Body;

class RequestBuilder
{
    public function build(): Request
    {
        return new Request(
            $this->retrievePath(),
            $this->retrieveHeaders(),
            $this->retrieveMethod(),
            $this->retrieveBody()
        );       
    }

    /**
     * @return string
     */
    private function retrievePath(): string
    {
        $path = $_SERVER['REQUEST_URI'];
        $path = explode('?', $path)[0];
        $path = explode('#', $path)[0];
        return $path;
    }

    /**
     * @return array
     */
    private function retrieveHeaders(): array
    {
        $formatter = new HeaderFormatter();
        $serverValues = $_SERVER;
        $headers = [];
        foreach ($serverValues as $name => $value) {
            if (is_string($value) && preg_match('/^HTTP_\w+/', $name)) {
                $headers[$formatter->format($name)] = $value;
            }
        }
        return $headers;
    }

    private function retrieveMethod(): string
    {
        $method = $_SERVER['REQUEST_METHOD'];

        if (!$method) {
            throw new HttpException('Could not determine request method');
        }

        return $method;
    }

    private function retrieveBody(): ?Body
    {
        if (in_array($this->retrieveMethod(), [RequestInterface::POST])) {
            return new Body(json_decode(file_get_contents('php://input'), true) ?? []);
        }

        return null;
    }


    public static function create(): RequestBuilder
    {
        return new RequestBuilder();
    }
}