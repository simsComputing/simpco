<?php

namespace Simpco\Framework;

class Session implements SessionInterface
{
    private bool $sessionStarted = false;

    public function get(string $key, mixed $default = null): mixed
    {
        $this->startSession();

        if (!isset($_SESSION[$key])) {
            return $default;
        }

        return $_SESSION[$key];
    }

    public function set(string $key, mixed $value): self
    {
        $this->startSession();
        $_SESSION[$key] = $value;
        return $this;
    }

    private function startSession(): void
    {
        if ($this->sessionStarted === true) {
            return;
        }

        if (session_status() === PHP_SESSION_ACTIVE) {
            $this->sessionStarted = true;
            return;
        }

        session_start();
        $this->sessionStarted = true;
    }
}