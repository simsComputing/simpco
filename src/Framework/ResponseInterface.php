<?php 

namespace Simpco\Framework;

use Simpco\Framework\Http\HeaderInterface;

interface ResponseInterface
{
    /**
     * @return HeaderInterface[]
     */
    public function getHeaders(): array;

    public function addHeader(HeaderInterface $header): self;

    public function removeHeader(string $headerName): self;

    /**
     * @return string
     */
    public function getBody(): string;

    public function setBody(string $body): self;
}