<?php

namespace Simpco\Framework;

use Simpco\Framework\View\HeadTagPoolInterface;

class ViewFactory
{
    private HeadTagPoolInterface $headTagPool;

    public function __construct(
        HeadTagPoolInterface $headTagPool
    )  {
        $this->headTagPool = $headTagPool;
    }

    protected function getBaseHeadTags()
    {
        return $this->headTagPool->getList();
    }
    
    public function create(string $layout, string $template, array $data, array $headTags = [], string $main = 'main.phtml'): ViewInterface
    {
        $baseHeadTags = $this->getBaseHeadTags();
        foreach ($headTags as $headTag) {
            $baseHeadTags[] = $headTag;
        }

        return View::create($layout, $template, $data, $main, $baseHeadTags);
    }
}