<?php

namespace Simpco\Framework;

interface RouterInterface
{
    public function getRoute(RequestInterface $request): string;
}