<?php

namespace Simpco\Framework;

use Simpco\Framework\View\HeadTagInterface;

class View implements ViewInterface
{
    private array $headTags;

    /**
     * @var string
     */
    private string $layout;

    private string $template;

    private string $main;

    private array $data;

    /**
     * @param HeadTagInterface[] $headTags
     */
    public function __construct(
        string $layout,
        string $template,
        array $data,
        array $headTags,
        string $main
    ) {
        $this->headTags = $headTags;
        $this->layout = $layout;
        $this->template = $template;
        $this->main = $main;
        $this->data = $data;
    }

    /**
     * @param string|int $key
     * @return void
     */
    public function getData($key)
    {
        if (!isset($this->data[$key])) {
            return null;
        }

        return $this->data[$key];
    }

    /**
     * @return HeadTagInterface[]
     */
    public function getHeadTags(): array
    {
        return $this->headTags;
    }

    public function getLayout(): string
    {
        return $this->layout;
    }

    public function getTemplate(): string
    {
        return $this->template;
    }

    public function getMainFile(): string
    {
        return $this->main;
    }

    /**
     * @param string $layout
     * @param array $headTags
     * @return ViewInterface
     */
    public static function create(
        string $layout,
        string $template,
        array $data,
        string $main = 'main.phtml',
        array $headTags = []
    ): ViewInterface {
        return new View($layout, $template, $data, $headTags, $main);
    }
}
