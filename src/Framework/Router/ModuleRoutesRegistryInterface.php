<?php

namespace Simpco\Framework\Router;

interface ModuleRoutesRegistryInterface
{
    public function registerAll(array $routes);

    public function getList(string $method);
}