<?php

namespace Simpco\Framework\Router;

interface RouteModifierInterface 
{
    public function modify(string $path): ?string;
}
