<?php

namespace Simpco\Framework\Router;

use Simpco\Framework\RequestInterface;

class ModuleRoutesRegistry implements ModuleRoutesRegistryInterface
{
    private array $mapping = [];

    public function __construct()
    {
        foreach (RequestInterface::ALL_POSSIBLE_METHODS as $method) {
            $this->mapping[$method] = [];
        }        
    }

    /**
     * @param array $routes
     * @return void
     */
    public function registerAll(array $routes): void
    {
        foreach (RequestInterface::ALL_POSSIBLE_METHODS as $method) {
            if (!isset($routes[$method])) {
                continue;
            }
            $this->mapping[$method] = array_merge($this->mapping[$method], $routes[$method]);
        }
    }

    /**
     * @param string $method
     * @return array
     */
    public function getList(string $method): array
    {
        return $this->mapping[$method];
    }
}