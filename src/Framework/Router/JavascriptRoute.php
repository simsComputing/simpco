<?php

namespace Simpco\Framework\Router;

use Simpco\Framework\Http\Header;
use Simpco\Framework\ResponseInterface;
use Simpco\Framework\Router\RouteInterface;
use Simpco\Framework\ViewFactory;
use Simpco\Framework\ViewInterface;

class JavascriptRoute implements RouteInterface
{
    /**
     * @var ViewFactory
     */
    private ViewFactory $viewFactory;

    private UrlWildcardInterface $urlWildcard;

    private ResponseInterface $response;

    public function __construct(
        ViewFactory $viewFactory,
        UrlWildcardInterface $urlWildcard,
        ResponseInterface $response
    ) {
        $this->response = $response;
        $this->viewFactory = $viewFactory;
        $this->urlWildcard = $urlWildcard;
    }

    public function execute(): ViewInterface
    {
        $this->response->addHeader(Header::create('Content-Type', 'application/javascript'));
        $jsPathParts = [];
        while ($part = $this->urlWildcard->get(count($jsPathParts))) {
            $jsPathParts[] = $part;
        }

        $jsFileName = implode('/', $jsPathParts);
        return $this->viewFactory->create('empty.phtml', '/js/' . $jsFileName, [], [], 'empty.phtml');
    }
}
