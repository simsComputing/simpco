<?php

namespace Simpco\Framework\Router;

use Simpco\Framework\ViewInterface;

interface RouteInterface 
{
    public function execute(): ViewInterface;
}
