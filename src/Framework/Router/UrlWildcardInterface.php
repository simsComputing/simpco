<?php

namespace Simpco\Framework\Router;

interface UrlWildcardInterface
{
    public function push(string $value): void;

    public function get(int $index): ?string;

    public function reset(): void;
}