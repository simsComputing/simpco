<?php

namespace Simpco\Framework\Router;

class UrlWildcard implements UrlWildcardInterface
{
    private array $values = [];

    public function push(string $value): void
    {
        $this->values[] = $value;
    }

    public function get(int $index): ?string
    {
        if (!isset($this->values[$index])) {
            return null;
        }

        return $this->values[$index];
    }

    public function reset(): void
    {
        $this->values = [];
    }
}