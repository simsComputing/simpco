<?php

namespace Simpco\Framework\Http;

interface HeaderInterface
{
    const CONTENT_TYPE = 'Content-Type';

    const CONTENT_TYPE_JSON = 'application/json';

    const CONTENT_TYPE_HTML = 'text/html';

    public function getName();

    public function getValue();

    public static function create(string $name, string $value);
}
