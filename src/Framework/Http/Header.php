<?php 

namespace Simpco\Framework\Http;

class Header implements HeaderInterface
{
    private string $name;

    private string $value;

    public function __construct(string $name, string $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public static function create(string $name, string $value): HeaderInterface
    {
        return new Header($name, $value);
    }
}