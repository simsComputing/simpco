<?php 

namespace Simpco\Framework\Http;

class HeaderFormatter
{
    public function format(string $requestHeader): string
    {
        $requestHeader = strtolower($requestHeader);
        $requestHeader = explode('_', $requestHeader);
        $requestHeader = array_filter($requestHeader, function ($value) {
            return $value !== 'http';
        });
        $requestHeader = array_map('ucfirst', $requestHeader);
        return implode('-', $requestHeader);
    }
}