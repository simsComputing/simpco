<?php

use Simpco\Framework\DependencyInjection\DependencyInjectionFactory;
use Simpco\Framework\DependencyInjection\ServiceFactory;
use Simpco\Framework\Models\EntityManagerFactory;
use Simpco\Framework\Response;
use Simpco\Framework\ResponseInterface;
use Simpco\Framework\Router;
use Simpco\Framework\Router\UrlWildcard;
use Simpco\Framework\Router\UrlWildcardInterface;
use Simpco\Framework\RouterInterface;
use Simpco\Framework\Session;
use Simpco\Framework\SessionInterface;
use Simpco\Framework\View\HeadTagPool;
use Simpco\Framework\View\HeadTagPoolInterface;
use Simpco\Framework\View\Includer;
use Simpco\Framework\View\IncluderInterface;
use Simpco\Framework\View\Pool;
use Simpco\Framework\View\ViewPoolInterface;

$classicFactories = ServiceFactory::bashFactory(
    [
        [Router::class, ['routes' => '%router.routes%'], RouterInterface::class],
        [Includer::class, [], IncluderInterface::class],
        [Response::class, [], ResponseInterface::class],
        [Pool::class, ['viewDirs' => '%viewPool.dirs%'], ViewPoolInterface::class],
        [UrlWildcard::class, [], UrlWildcardInterface::class],
        [HeadTagPool::class, [], HeadTagPoolInterface::class],
        [Session::class, [], SessionInterface::class]
    ]
);

return [
    ...$classicFactories,
    EntityManagerFactory::create(),
    DependencyInjectionFactory::create()
];
