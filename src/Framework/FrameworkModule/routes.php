<?php

use Simpco\Framework\RequestInterface;
use Simpco\Framework\Router;
use Simpco\Framework\Router\JavascriptRoute;

function make_js_route_with_depth(int $depth)
{
    $parts = ['assets', 'js'];
    for ($i = 0; $i < $depth; $i++) {
        $parts[] = Router::WILDCARD_KEY;
    }

    return implode('/', $parts);
}

$javascriptRoutes = [];

for ($i = 0; $i < 10; $i++) {
    $javascriptRoutes[make_js_route_with_depth($i + 1)] = JavascriptRoute::class;
}

return [RequestInterface::GET => $javascriptRoutes];