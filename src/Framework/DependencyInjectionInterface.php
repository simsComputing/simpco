<?php

namespace Simpco\Framework;

use Simpco\Framework\DependencyInjection\ParamFactoryInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolver;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolverInterface;
use Simpco\Framework\DependencyInjection\ServiceFactoryInterface;
use Simpco\Framework\DependencyInjection\ServiceFactoryProvider;
use Simpco\Framework\DependencyInjection\ServiceInstanciationStackInterface;

interface DependencyInjectionInterface
{
        /**
     * @param ServiceFactoryInterface $serviceFactory
     * @return void
     */
    public function registerService(ServiceFactoryInterface $serviceFactory): self;

    /**
     * @param ParamFactoryInterface $paramFactory
     * @return void
     */
    public function registerParam(ParamFactoryInterface $paramFactory): self;

    /**
     * @param string $name
     * @return object
     */
    public function getService(string $name): object;

    /**
     * @param string $paramName
     * @return string|array
     */
    public function getParam(string $paramName);

    public function getCurrentStackSize(): int;

    public static function create(
        ServiceInstanciationStackInterface $serviceInstanciationStack,
        ServiceArgumentResolverInterface $serviceArgumentResolver,
        ServiceFactoryProvider $serviceFactoryProvider
    ): DependencyInjectionInterface;

    public function modifyArgument(
        string $service,
        ServiceArgumentModifierInterface $serviceArgumentModifier
    ): void;
}