<?php

namespace Simpco\Framework;

use Simpco\Framework\Application\RunnerInterface;

interface ApplicationInterface
{
    public function init(): self;

    public function run(): self;

    public function send(): self;

    public static function create(
        DependencyInjectionInterface $di,
        array $modules,
        array $starters,
        RunnerInterface $runner,
        array $senders
    ): ApplicationInterface;
}
