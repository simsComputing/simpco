<?php

namespace Simpco\Framework\Application;

use Simpco\Framework\DependencyInjectionInterface;

interface RunnerInterface
{
    public function run(DependencyInjectionInterface $dependencyInjection): void;
}
