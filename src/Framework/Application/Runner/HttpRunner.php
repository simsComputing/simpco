<?php

namespace Simpco\Framework\Application\Runner;
use Simpco\Framework\Application\RunnerInterface;
use Simpco\Framework\DependencyInjection\ServiceFactory\FakeFactory;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Exception\RedirectionException;
use Simpco\Framework\Http\Header;
use Simpco\Framework\RequestBuilder;
use Simpco\Framework\RequestInterface;
use Simpco\Framework\ResponseInterface;
use Simpco\Framework\RouterInterface;
use Simpco\Framework\View\HeadTag\HtmlPrinter;
use Simpco\Framework\View\IncluderInterface;

class HttpRunner implements RunnerInterface
{
    public function run(DependencyInjectionInterface $dependencyInjection): void
    {
        $request = RequestBuilder::create()->build();
        $dependencyInjection->registerService(new FakeFactory($request, RequestInterface::class));
        $response = $dependencyInjection->getService(ResponseInterface::class);
        try {
            $route = $dependencyInjection->getService(RouterInterface::class)->getRoute($request);
            $view = $dependencyInjection->getService($route)->execute();
            $printer = $dependencyInjection->getService(HtmlPrinter::class);
            /** @var IncluderInterface */
            $includer = $dependencyInjection->getService(IncluderInterface::class);
            $body = $includer->includeMainFile($view, $printer);
            $response->setBody($body);
        } catch (RedirectionException $redirection) {
            $response->addHeader(Header::create('Location', $redirection->getPath()));
        }
    }
}
