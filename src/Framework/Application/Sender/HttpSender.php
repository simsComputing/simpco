<?php

namespace Simpco\Framework\Application\Sender;
use Simpco\Framework\Application\SenderInterface;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\ResponseInterface;

class HttpSender implements SenderInterface
{
    public function send(DependencyInjectionInterface $dependencyInjection): void
    {
        $response = $dependencyInjection->getService(ResponseInterface::class);
        
        /**
         * @var HeaderInterface $header
         */
        foreach ($response->getHeaders() as $header) {
            header($header->getName() . ': ' . $header->getValue());
        }
        echo $response->getBody();
    }
}
