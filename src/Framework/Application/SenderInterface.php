<?php

namespace Simpco\Framework\Application;

use Simpco\Framework\DependencyInjectionInterface;

interface SenderInterface
{
    public function send(DependencyInjectionInterface $dependencyInjection): void;
}
