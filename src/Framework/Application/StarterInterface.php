<?php

namespace Simpco\Framework\Application;

use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Module\Registry;
use Simpco\Framework\ModuleInterface;

interface StarterInterface
{
    public function beforeStart(        
        DependencyInjectionInterface $dependencyInjection,
        Registry $registry
    ): void;

    public function start(        
        ModuleInterface $module,
        DependencyInjectionInterface $dependencyInjection
    ): void;

    public function afterStart(DependencyInjectionInterface $dependencyInjection): void;
}
