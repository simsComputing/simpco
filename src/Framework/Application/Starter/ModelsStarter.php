<?php

namespace Simpco\Framework\Application\Starter;

use Doctrine\ORM\EntityManagerInterface;
use Simpco\Framework\Application\StarterInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifier\ReplaceArg;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Module\Registry;
use Simpco\Framework\ModuleInterface;

class ModelsStarter implements StarterInterface
{
    private array $modelsDirs = [];

    public function beforeStart(        
        DependencyInjectionInterface $dependencyInjection,
        Registry $registry
    ): void {
        return;
    }

    public function start(        
        ModuleInterface $module,
        DependencyInjectionInterface $dependencyInjection
    ): void {
        if ($module->getModelsDir()) {
            $this->modelsDirs[] = $module->getModelsDir();
        }
    }

    public function afterStart(DependencyInjectionInterface $dependencyInjection): void {
        $dependencyInjection->modifyArgument(
            EntityManagerInterface::class,
            ReplaceArg::create('modelsDirs', $this->modelsDirs)
        );
    }
}