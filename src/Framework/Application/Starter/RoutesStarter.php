<?php

namespace Simpco\Framework\Application\Starter;

use Simpco\Framework\Application\StarterInterface;
use Simpco\Framework\DependencyInjection\ServiceFactory\FakeFactory;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Module\Registry;
use Simpco\Framework\ModuleInterface;
use Simpco\Framework\Router\ModuleRoutesRegistry;
use Simpco\Framework\Router\ModuleRoutesRegistryInterface;

class RoutesStarter implements StarterInterface
{
    private ModuleRoutesRegistry $moduleRoutesRegistry;

    /**
     * @param ModuleRoutesRegistry $moduleRoutesRegistry
     */
    public function __construct(
        ModuleRoutesRegistry $moduleRoutesRegistry
    ) {
        $this->moduleRoutesRegistry = $moduleRoutesRegistry;
    }

    public function beforeStart(        
        DependencyInjectionInterface $dependencyInjection,
        Registry $registry
    ): void {
        return;
    }

    public function start(        
        ModuleInterface $module,
        DependencyInjectionInterface $dependencyInjection
    ): void {
        $this->moduleRoutesRegistry->registerAll($module->getRoutes());
    }

    public function afterStart(DependencyInjectionInterface $dependencyInjection): void
    {
        $dependencyInjection->registerService(new FakeFactory($this->moduleRoutesRegistry, ModuleRoutesRegistryInterface::class));
    }
}