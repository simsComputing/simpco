<?php

namespace Simpco\Framework\Application\Starter;
use Simpco\Framework\Application\StarterInterface;
use Simpco\Framework\DependencyInjection\ServiceFactory\FakeFactory;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Module\Registry;
use Simpco\Framework\ModuleInterface;

class DIStarter implements StarterInterface
{
    private array $modifiers = [];

    public function beforeStart(        
        DependencyInjectionInterface $dependencyInjection,
        Registry $registry
    ): void {
        $dependencyInjection->registerService(
            new FakeFactory($registry, Registry::class)
        );
    }

    public function start(        
        ModuleInterface $module,
        DependencyInjectionInterface $dependencyInjection
    ): void {
        array_map([$dependencyInjection, 'registerService'], $module->getServices());
        array_map([$dependencyInjection, 'registerParam'], $module->getParams());
        $this->modifiers[] = $module->getServiceModifiers();
    }

    public function afterStart(DependencyInjectionInterface $dependencyInjection): void
    {
        foreach ($this->modifiers as $moduleModifiers) {
            foreach ($moduleModifiers as $service => $serviceModifiers) {
                foreach ($serviceModifiers as $modifier) {
                    $dependencyInjection->modifyArgument($service, $modifier);
                }
            }
        }
    }
}
