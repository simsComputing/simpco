<?php 

namespace Simpco\Framework\Application\Starter;

use Simpco\Framework\Application\StarterInterface;
use Simpco\Framework\DependencyInjection\ParamFactory;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Module\Registry;
use Simpco\Framework\ModuleInterface;

class ViewsStarter implements StarterInterface
{
    private array $viewDirs = [];

    public function beforeStart(        
        DependencyInjectionInterface $dependencyInjection,
        Registry $registry
    ): void {
        return;
    }

    public function start(        
        ModuleInterface $module,
        DependencyInjectionInterface $dependencyInjection
    ): void {
        if (null !== $module->getViewDir()) {
            $this->viewDirs[get_class($module)] = $module->getViewDir();
        }
    }

    public function afterStart(DependencyInjectionInterface $dependencyInjection): void
    {
        $dependencyInjection->registerParam(ParamFactory::create('viewPool.dirs', $this->viewDirs));
    }
}