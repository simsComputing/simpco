<?php 

namespace Simpco\Framework;

use Simpco\Framework\Http\HeaderInterface;

class Response implements ResponseInterface
{
    private array $headers = [];

    private string $body = '';

    public function addHeader(HeaderInterface $header): self
    {
        $this->headers[$header->getName()] = $header;
        return $this;
    }

    public function removeHeader(string $headerName): self
    {
        unset($this->headers[$headerName]);
        return $this;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;
        return $this;
    }

    public static function create()
    {
        return new Response();
    }
}
