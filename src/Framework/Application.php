<?php

namespace Simpco\Framework;

use Simpco\Framework\Application\RunnerInterface;
use Simpco\Framework\Application\SenderInterface;
use Simpco\Framework\Application\StarterInterface;
use Simpco\Framework\DependencyInjection\ServiceFactory\FakeFactory;
use Simpco\Framework\Module\Registry;
use Simpco\Utils\Sort\TopologicalSort\Factory as TopologicalSortFactory;

class Application implements ApplicationInterface
{
    private DependencyInjectionInterface $di;

    private array $modules;

    /**
     * @var StarterInterface[]
     */
    private array $starters;

    private RunnerInterface $runner;

    /**
     * @var SenderInterface[]
     */
    private array $senders;

    /**
     * @param string $moduleDir
     */
    public function __construct(
        DependencyInjectionInterface $di,
        array $modules,
        array $starters,
        RunnerInterface $runner,
        array $senders
    ) {
        $this->di = $di;
        $this->modules = $modules;
        $this->starters = $starters;
        $this->runner = $runner;
        $this->senders = $senders;
    }

    /**
     * @return self
     */
    public function init(): self
    {
        $registry = new Registry($this->modules, new TopologicalSortFactory());
        $this->di->registerService(new FakeFactory($registry, Registry::class));

        foreach ($this->starters as $starter) {
            $starter->beforeStart($this->di, $registry);
        }

        /** @var ModuleInterface $module */
        foreach ($registry->getList() as $module) {
            foreach ($this->starters as $starter) {
                $starter->start($module, $this->di);
            }
        }

        foreach ($this->starters as $starter) {
            $starter->afterStart($this->di);
        }

        return $this;
    }

    public function run(): self
    {
        $this->runner->run($this->di);
        return $this;
    }

    public function send(): self
    {
        foreach ($this->senders as $sender) {
            $sender->send($this->di);
        }
        return $this;
    }

    public static function create(
        DependencyInjectionInterface $di,
        array $modules,
        array $starters,
        RunnerInterface $runner,
        array $senders
    ): ApplicationInterface {
        return new Application(
            $di,
            $modules,
            $starters,
            $runner,
            $senders
        );
    }

    public function getDi(): DependencyInjectionInterface
    {
        return $this->di;
    }
}
