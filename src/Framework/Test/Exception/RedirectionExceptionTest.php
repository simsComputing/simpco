<?php

namespace Simpco\Framework\Test\Exception;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\Exception\RedirectionException;

/**
 * @covers \Simpco\Framework\Exception\RedirectionException
 */
class RedirectionExceptionTest extends TestCase
{
    public function testSimpleUseCase()
    {
        $exception = new RedirectionException('/test/path');

        $this->assertEquals('/test/path', $exception->getPath());
        $this->assertEquals('Redirected to path /test/path', $exception->getMessage());
    }
}