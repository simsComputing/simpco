<?php

namespace Simpco\Framework\Test;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\Session;

/**
 * @covers \Simpco\Framework\Session
 */
class SessionTest extends TestCase
{
    private function setSessionVars(): void
    {
        session_start();
        $_SESSION['test'] = 'value';
        $_SESSION['test2'] = 'value2';
    }

    public function testSessionGetter(): void
    {
        $this->setSessionVars();
        $session = new Session();

        $value = $session->get('test');
        $value2 = $session->get('test2');
        $default = $session->get('doesnotexist');

        $this->assertEquals('value', $value);
        $this->assertEquals('value2', $value2);
        $this->assertNull($default);

        session_destroy();
    }

    public function testSessionSetter()
    {
        $session = new Session();
        $session->set('test', 'value2');

        $this->assertEquals('value2', $session->get('test'));
    }
}