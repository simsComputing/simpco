<?php 

namespace Simpco\Framework\Test;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\RequestBuilder;
use Simpco\Framework\RequestInterface;

/**
 * @covers \Simpco\Framework\RequestBuilder
 */
class RequestBuilderTest extends TestCase
{
    /**
     * @dataProvider getTestRequestBuilderDataSet
     * @param string $expectedPath
     * @return void
     */
    public function testRequestBuilder(string $expectedPath)
    {
        $this->setServerVar($expectedPath);
        $builder = new RequestBuilder();
        $request = $builder->build();
        $this->assertInstanceOf(RequestInterface::class, $request);
    }

    public static function getTestRequestBuilderDataSet(): array
    {
        return [
            ['/catalog/123/edit']
        ];
    }

    private function setServerVar(string $path)
    {
        $_SERVER['REQUEST_URI'] = $path;
        $_SERVER['REQUEST_METHOD'] = 'GET';
    }

    public function testCreate()
    {
        $builder = RequestBuilder::create();
        $this->assertInstanceOf(RequestBuilder::class, $builder);
    }
}
 