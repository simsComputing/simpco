<?php

namespace Simpco\Framework\Test\Module;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\Module\Registry;
use Simpco\Framework\ModuleInterface;
use Simpco\Utils\Sort\TopologicalSort;
use Simpco\Utils\Sort\TopologicalSort\Factory as TopologicalSortFactory;

/**
 * @covers \Simpco\Framework\Module\Registry
 */
class RegistryTest extends TestCase
{
    private array $list;

    private TopologicalSortFactory $topologicalSortFactory;

    private TopologicalSort $topologicalSort;

    private Registry $registry;

    public function setUp(): void
    {
        $module1 = $this->createStub(ModuleInterface::class);
        $module2 = $this->createStub(ModuleInterface::class);
        $module3 = $this->createStub(ModuleInterface::class);
        $module4 = $this->createStub(ModuleInterface::class);
        $this->list = [$module1, $module2, $module3, $module4];
        $this->topologicalSortFactory = $this->createStub(TopologicalSortFactory::class);
        $this->topologicalSort = $this->createStub(TopologicalSort::class);
        $this->topologicalSortFactory->method('create')->willReturn($this->topologicalSort);

        $this->topologicalSort->method('sort')->willReturn(
            [get_class($module4), get_class($module3), get_class($module1), get_class($module2)]
        );

        $this->registry = new Registry($this->list, $this->topologicalSortFactory);
    }

    public function testSimpleUseCase()
    {
        $result = $this->registry->getList();
        $this->assertSameSize($this->list, $result);
        // For registry cache using
        $this->assertSame($result, $this->registry->getList());
    }
}