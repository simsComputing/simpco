<?php

namespace Simpco\Framework\Test;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\Http\HeaderInterface;
use Simpco\Framework\Request;
use Simpco\Framework\RequestInterface;

/**
 * @covers \Simpco\Framework\Request
 */
class RequestTest extends TestCase
{
    /**
     * @dataProvider getTestRequestDataSet
     * @param string $path
     * @return void
     */
    public function testRequest(string $path, array $headers)
    {
        $request = new Request($path, $headers, RequestInterface::GET);
        $this->assertEquals($path, $request->getPath());
        foreach ($headers as $name => $value) {
            $this->assertEquals($value, $request->getHeader($name));
        }
    }

    /**
     * @return array
     */
    public static function getTestRequestDataSet(): array
    {
        return [
            ['/try/it/out', ['Content-Type' => 'application/json']],
            ['/', ['Content-Type' => 'text/html']]
        ];
    }
}