<?php

namespace Simpco\Framework\Test\Models;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolverInterface;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Exception\DependencyInjectionException;
use Simpco\Framework\Models\EntityManagerFactory;

/**
 * @covers \Simpco\Framework\Models\EntityManagerFactory
 */
class EntityManagerFactoryTest extends TestCase
{
    private DependencyInjectionInterface $dependencyInjection;

    private ServiceArgumentResolverInterface $serviceArgumentResolver;

    private ServiceArgumentModifierInterface $serviceArgumentModifier;

    private EntityManagerFactory $entityManagerFactory;

    public function setUp(): void
    {
        $this->dependencyInjection = $this->createStub(DependencyInjectionInterface::class);
        $this->dependencyInjection->method('getParam')->willReturn([
            'driver' => 'pdo_mysql',
            'user' => 'root',
            'password' => 'example',
            'dbname' => 'simpco',
            'host' => 'db',
            'port' => '3306'
        ]);
        $this->serviceArgumentResolver = $this->createStub(ServiceArgumentResolverInterface::class);
        $this->serviceArgumentModifier = $this->createStub(ServiceArgumentModifierInterface::class);
        $this->entityManagerFactory = new EntityManagerFactory();
        $this->entityManagerFactory->setModelsDirs([]);
    }

    public function testSimpleUseCase(): void
    {
        $factory = $this->entityManagerFactory;
        $em = $factory->getInstance($this->dependencyInjection, $this->serviceArgumentResolver);
        $this->assertInstanceOf(EntityManager::class, $em);
        $this->assertSame($em, $factory->getInstance($this->dependencyInjection, $this->serviceArgumentResolver));
        $this->assertEquals(EntityManagerInterface::class, $factory->getNameForRegistration());
    }

    public function testFailOnModifyWrongArgument(): void
    {
        $this->serviceArgumentModifier->method('getArgumentName')->willReturn('something');
        $this->expectException(DependencyInjectionException::class);
        $this->entityManagerFactory->modifyArgument($this->serviceArgumentModifier);
    }

    public function testSuccessOnModifyModelsDirs(): void
    {
        $this->serviceArgumentModifier->method('getArgumentName')->willReturn('modelsDirs');
        $this->serviceArgumentModifier->method('modify')->willReturn([]);
        $this->entityManagerFactory->modifyArgument($this->serviceArgumentModifier);
        $em = $this->entityManagerFactory->getInstance($this->dependencyInjection, $this->serviceArgumentResolver);
        $this->assertInstanceOf(EntityManager::class, $em);
    }

    public function testFactory(): void
    {
        $this->assertInstanceOf(EntityManagerFactory::class, EntityManagerFactory::create());
    }
}