<?php 

namespace Simpco\Framework\Test;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\Application;
use Simpco\Framework\Application\RunnerInterface;
use Simpco\Framework\Application\SenderInterface;
use Simpco\Framework\Application\StarterInterface;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\ModuleInterface;

/**
 * @covers \Simpco\Framework\Application
 * @uses \Simpco\Framework\DependencyInjection\ServiceFactory\FakeFactory
 * @uses \Simpco\Framework\Module\Registry
 * @uses \Simpco\Utils\Sort\TopologicalSort
 * @uses \Simpco\Utils\Sort\TopologicalSort\Factory
 * @uses \Simpco\Utils\Sort\TopologicalSort\Vertice
 */
class ApplicationTest extends TestCase
{
    /**
     * @var DependencyInjectionInterface
     */
    private DependencyInjectionInterface $dependencyInjection;

    /**
     * @var array
     */
    private array $modules = [];

    /**
     * @var array
     */
    private array $starters = [];

    /**
     * @var RunnerInterface
     */
    private RunnerInterface $runner;

    /**
     * @var array
     */
    private array $senders = [];

    /**
     * @return void
     */
    public function setUp(): void
    {
        $this->dependencyInjection = $this->createStub(DependencyInjectionInterface::class);

        $this->modules[] = $this->createStub(ModuleInterface::class);
        $this->modules[] = $this->createStub(ModuleInterface::class);
        $this->modules[] = $this->createStub(ModuleInterface::class);

        $this->starters[] = $this->createStub(StarterInterface::class);
        $this->starters[] = $this->createStub(StarterInterface::class);
        $this->starters[] = $this->createStub(StarterInterface::class);

        $this->runner = $this->createStub(RunnerInterface::class);

        $this->senders[] = $this->createStub(SenderInterface::class);
        $this->senders[] = $this->createStub(SenderInterface::class);
        $this->senders[] = $this->createStub(SenderInterface::class);
    }

    /**
     * @return void
     */
    public function testMainUseCase(): void
    {
        $app = new Application(
            $this->dependencyInjection,
            $this->modules,
            $this->starters,
            $this->runner,
            $this->senders
        );

        $app
            ->init()
            ->run()
            ->send();

        $this->assertInstanceOf(DependencyInjectionInterface::class, $app->getDi());

    }

    public function testFactory(): void
    {
        $app = Application::create(
            $this->dependencyInjection,
            $this->modules,
            $this->starters,
            $this->runner,
            $this->senders
        );

        $this->assertInstanceOf(Application::class, $app);
    }
}
