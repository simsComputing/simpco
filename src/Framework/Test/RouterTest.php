<?php

namespace Simpco\Framework\Test;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\RequestInterface;
use Simpco\Framework\Router;
use Simpco\Framework\Router\ModuleRoutesRegistry;
use Simpco\Framework\Router\ModuleRoutesRegistryInterface;
use Simpco\Framework\Router\RouteModifierInterface;
use Simpco\Framework\Router\UrlWildcardInterface;
use Simpco\Framework\Test\Fake\Routes\Catalog;
use Simpco\Framework\Test\Fake\Routes\CatalogId;
use Simpco\Framework\Test\Fake\Routes\CatalogIdEdit;
use Simpco\Framework\Test\Fake\Routes\Entity1Entity22;
use Simpco\Framework\Test\Fake\Routes\EntityIdEntity2Id2;
use Simpco\Framework\Test\Fake\Routes\Index;
use Simpco\Framework\Test\Fake\Routes\TestPath;

/**
 * @covers \Simpco\Framework\Router
 * @todo Test if we insert two wildcards with same name what happens
 * @todo Test how route gets widcard parameters.
 */
class RouterTest extends TestCase
{
    const ROUTES_DEFINITIONS = [
        RequestInterface::GET => [
            '' => Index::class,
            'catalog' => Catalog::class,
            'catalog/{id}' => CatalogId::class,
            'catalog/{id}/edit' => CatalogIdEdit::class,
            '/entity/{id}/entity2/{id2}' => EntityIdEntity2Id2::class,
            '/entity/1/entity2/2' => Entity1Entity22::class,
            '/test/path' => TestPath::class
        ]
    ];

    private UrlWildcardInterface $urlWildcard;

    private ModuleRoutesRegistryInterface $moduleRoutesRegistry;

    private array $modifiers = [];

    private function createRouter(): Router
    {
        return new Router($this->moduleRoutesRegistry, $this->modifiers, $this->urlWildcard);
    }

    private function createRoutesRegistryMap(): array
    {
        $map = [];
        foreach (RequestInterface::ALL_POSSIBLE_METHODS as $method) {
            $definitions = [];
            if (isset(self::ROUTES_DEFINITIONS[$method])) {
                $definitions = self::ROUTES_DEFINITIONS[$method];
            }
            $map[] = [$method, $definitions];
        }

        return $map;
    }
    public function setUp(): void
    {
        $this->moduleRoutesRegistry = $this->createStub(ModuleRoutesRegistryInterface::class);
        $this->moduleRoutesRegistry->method('getList')->willReturnMap($this->createRoutesRegistryMap());
        $this->urlWildcard = $this->createStub(UrlWildcardInterface::class);
        $this->modifiers = [];
    }

    /**
     * @dataProvider getRequestDataSet
     * @param RequestInterface $request
     * @param string $expected
     * @return void
     */
    public function testRouter($requestPath, $expected)
    {
        $router = $this->createRouter();
        $request = $this->createRequestStub($requestPath);
        $route = $router->getRoute($request);
        $this->assertEquals(self::ROUTES_DEFINITIONS['GET'][$expected], $route);
    }

    public static function getRequestDataSet(): array
    {
        return [
            ['/catalog/123/edit', 'catalog/{id}/edit'],
            ['catalog/123/edit', 'catalog/{id}/edit'],
            ['/catalog/123', 'catalog/{id}'],
            ['test/path', '/test/path'],
            ['entity/39898/entity2/aezofij','/entity/{id}/entity2/{id2}'],
            ['entity/1/entity2/2','/entity/1/entity2/2'],
            ['/', '']
        ];
    }

    private function createRequestStub(string $requestPath)
    {
        $request = $this->createStub(RequestInterface::class);
        $request->method('getPath')->willReturn($requestPath);
        $request->method('getMethod')->willReturn('GET');
        return $request;
    }

    public function testRouterWithModifier(): void
    {
        $modifier = $this->createMock(RouteModifierInterface::class);
        $modifier->expects($this->once())->method('modify')->willReturn('/test/path');
        $this->modifiers[] = $modifier;
        $router = $this->createRouter();
        $finalRoute = $router->getRoute($this->createRequestStub('/try/any/path'));
        $this->assertSame(self::ROUTES_DEFINITIONS['GET']['/test/path'], $finalRoute);
    }

    public function testRouterWithModifierReturnNull(): void
    {
        $modifier = $this->createMock(RouteModifierInterface::class);
        $modifier->expects($this->once())->method('modify')->willReturn(null);
        $this->modifiers[] = $modifier;
        $router = $this->createRouter();

        $finalRoute = $router->getRoute($this->createRequestStub('catalog'));

        $this->assertSame(self::ROUTES_DEFINITIONS['GET']['catalog'], $finalRoute);
    }
}