<?php

namespace Simpco\Framework\Test\FileSystem;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\Exception\FileSystemException;
use Simpco\Framework\FileSystem\File;

/**
 * @covers \Simpco\Framework\FileSystem\File
 */
class FileTest extends TestCase
{
    public function testFileThatExists()
    {
        $file = new File(__DIR__ . '/Fake/glob/file1.txt');
        $this->assertEquals(__DIR__ . '/Fake/glob/file1.txt', $file->getPath());
    }

    public function testFileThatDoesNotExist()
    {
        $this->expectException(FileSystemException::class);
        new File(__DIR__ . '/Fake/existsnot.txt');
    }

    public function testFailIfFolder()
    {
        $this->expectException(FileSystemException::class);
        new File(__DIR__ . '/Fake/glob/folder1');
    }
}