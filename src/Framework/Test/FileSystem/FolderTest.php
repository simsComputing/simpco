<?php

namespace Simpco\Framework\Test\FileSystem;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\Exception\FileSystemException;
use Simpco\Framework\FileSystem\File;
use Simpco\Framework\FileSystem\Folder;

/**
 * @covers \Simpco\Framework\FileSystem\Folder
 */
class FolderTest extends TestCase
{
    public function testFolderThatExists()
    {
        $file = new Folder(__DIR__ . '/Fake/glob/folder1');
        $this->assertEquals(__DIR__ . '/Fake/glob/folder1', $file->getPath());
    }

    public function testFolderThatDoesNotExist()
    {
        $this->expectException(FileSystemException::class);
        new Folder(__DIR__ . '/Fake/existsnot');
    }

    public function testFailIfFile()
    {
        $this->expectException(FileSystemException::class);
        new Folder(__DIR__ . '/Fake/glob/folder1/file1.txt');
    }
}