<?php

namespace Simpco\Framework\Test\FileSystem;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\FileSystem\File;
use Simpco\Framework\FileSystem\Folder;
use Simpco\Framework\FileSystem\Glob;
use Simpco\Framework\FileSystem\SystemElementFactory;

/**
 * @covers \Simpco\Framework\FileSystem\Glob
 */
class GlobTest extends TestCase
{
    private Glob $glob;

    private SystemElementFactory $systemElementFactory;

    const ELEMENTS_MAPPING = [
        __DIR__ . '/Fake/glob/file1.txt' => File::class,
        __DIR__ . '/Fake/glob/file2.txt' => File::class,
        __DIR__ . '/Fake/glob/folder1' => Folder::class,
        __DIR__ . '/Fake/glob/folder1/file3.txt' => File::class,
        __DIR__ . '/Fake/glob/folder1/file4.txt' => File::class,
    ];

    public function setUp(): void
    {
        $this->systemElementFactory = $this->createStub(SystemElementFactory::class);
        $returnMap = [];
        foreach (self::ELEMENTS_MAPPING as $elementPath => $elementType) {
            $returnMap[] = [$elementPath, $this->createElementStub($elementType, $elementPath)];
        }
        $this->systemElementFactory->method('create')->willReturnMap($returnMap);

        $this->glob = new Glob($this->systemElementFactory);
    }

    public static function getDataSet(): array
    {
        return [
            [
                __DIR__ . '/Fake/glob/*', 
                3, 
                [
                    __DIR__ . '/Fake/glob/file1.txt',
                    __DIR__ . '/Fake/glob/file2.txt',
                    __DIR__ . '/Fake/glob/folder1',
                ]
            ],
            [
                __DIR__ . '/Fake/glob/folder1/*', 
                2, 
                [
                    __DIR__ . '/Fake/glob/folder1/file2.txt',
                    __DIR__ . '/Fake/glob/folder1/file3.txt',
                ]
            ]
        ];
    }

    /**
     * @dataProvider getDataSet
     * @param string $mask
     * @param integer $expectedNbOfElements
     * @param array $expectedFiles
     * @return void
     */
    public function testGlob(string $mask, int $expectedNbOfElements, array $expectedFiles)
    {
        $result = $this->glob->execute($mask);
        $this->assertEquals($expectedNbOfElements, count($result));
        foreach ($result as $element) {
            return $this->assertContains($element->getPath(), $expectedFiles);
        }
    }

    private function createElementStub(string $class, string $path)
    {
        $stub = $this->createStub($class);
        $stub->method('getPath')->willReturn($path);
        return $stub;
    }
}