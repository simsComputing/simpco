<?php

namespace Simpco\Framework\Test\FileSystem;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\Exception\FileSystemException;
use Simpco\Framework\FileSystem\File;
use Simpco\Framework\FileSystem\Folder;
use Simpco\Framework\FileSystem\SystemElementFactory;

/**
 * @covers \Simpco\Framework\FileSystem\SystemElementFactory
 * @uses \Simpco\Framework\FileSystem\File
 * @uses \Simpco\Framework\FileSystem\Folder
 */
class SystemElementFactoryTest extends TestCase
{
    public function testWithFile()
    {
        $factory = new SystemElementFactory();
        $file = $factory->create(__DIR__ . '/Fake/glob/file1.txt');

        $this->assertInstanceOf(File::class, $file);
    }

    public function testWithFolder()
    {
        $factory = new SystemElementFactory();
        $folder = $factory->create(__DIR__ . '/Fake/glob/folder1');

        $this->assertInstanceOf(Folder::class, $folder);   
    }

    public function testNonExistent()
    {
        $this->expectException(FileSystemException::class);
        $factory = new SystemElementFactory();
        $folder = $factory->create(__DIR__ . '/Fake/glob/donotexist');

    }
}