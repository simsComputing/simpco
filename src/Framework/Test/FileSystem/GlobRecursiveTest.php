<?php

namespace Simpco\Framework\Test\FileSystem;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\FileSystem\File;
use Simpco\Framework\FileSystem\Folder;
use Simpco\Framework\FileSystem\Glob;
use Simpco\Framework\FileSystem\GlobRecursive;
use Simpco\Framework\FileSystem\SystemElementFactory;

/**
 * @covers \Simpco\Framework\FileSystem\GlobRecursive
 */
class GlobRecursiveTest extends TestCase
{
    private Glob $glob;

    private GlobRecursive $globRecursive;


    const ELEMENTS_MAPPING = [
        __DIR__ . '/Fake/glob/file1.txt' => File::class,
        __DIR__ . '/Fake/glob/file2.txt' => File::class,
        __DIR__ . '/Fake/glob/folder1' => Folder::class,
        __DIR__ . '/Fake/glob/folder1/file3.txt' => File::class,
        __DIR__ . '/Fake/glob/folder1/file4.txt' => File::class,
    ];

    const GLOB_MAP = [
        __DIR__ . '/Fake/glob/*' => [
            __DIR__ . '/Fake/glob/file1.txt',
            __DIR__ . '/Fake/glob/file2.txt',
            __DIR__ . '/Fake/glob/folder1'
        ],
        __DIR__ . '/Fake/glob/folder1/*' => [
            __DIR__ . '/Fake/glob/folder1/file3.txt',
            __DIR__ . '/Fake/glob/folder1/file4.txt',
        ]
    ];

    public function setUp(): void
    {
        $this->glob = $this->createStub(Glob::class);
        $returnMap = [];
        foreach (self::GLOB_MAP as $globPattern => $elements) {
            $returnMap[] = [$globPattern, 0, array_map([$this, 'createElementStub'], $elements)];
        }
        $this->glob->method('execute')->willReturnMap($returnMap);

        $this->globRecursive = new GlobRecursive($this->glob);
    }

    /**
     * @param string $mask
     * @param integer $expectedNbOfElements
     * @param array $expectedFiles
     * @return void
     */
    public function testGlobRecursive()
    {
        $result = $this->globRecursive->execute(__DIR__ . '/Fake/glob/*');
        $this->assertEquals(5, count($result));
        foreach ($result as $element) {
            return $this->assertContains($element->getPath(), array_keys(self::ELEMENTS_MAPPING));
        }
    }

    private function createElementStub(string $path)
    {
        $stub = $this->createStub(self::ELEMENTS_MAPPING[$path]);
        $stub->method('getPath')->willReturn($path);
        return $stub;
    }
}