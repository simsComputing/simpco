<?php

namespace Simpco\Framework\Test;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\DependencyInjection;
use Simpco\Framework\DependencyInjection\ParamFactoryInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolver;
use Simpco\Framework\DependencyInjection\ServiceFactoryInterface;
use Simpco\Framework\DependencyInjection\ServiceFactoryProvider;
use Simpco\Framework\DependencyInjection\ServiceInstanciationStack;
use Simpco\Framework\Exception\CircularDependencyException;
use Simpco\Framework\Exception\MissingParameterException;
use Simpco\Framework\Exception\MissingServiceException;
use Simpco\Framework\Test\DependencyInjection\Fake\FirstDependency;

/**
 * @covers \Simpco\Framework\DependencyInjection
 */
class DependencyInjectionTest extends TestCase
{
    private DependencyInjection $dependencyInjection;

    private ServiceArgumentResolver $serviceArgumentResolver;

    private ServiceInstanciationStack $serviceInstanciationStack;

    private ServiceFactoryProvider $serviceFactoryProvider;

    private ServiceFactoryInterface $firstDependencyFactory;


    public function setUp(): void
    {
        $this->serviceInstanciationStack = $this->createMock(ServiceInstanciationStack::class);
        $this->serviceArgumentResolver = $this->createStub(ServiceArgumentResolver::class);
        $this->serviceFactoryProvider = $this->createStub(ServiceFactoryProvider::class);

        $this->dependencyInjection = new DependencyInjection(
            $this->serviceInstanciationStack,
            $this->serviceArgumentResolver,
            $this->serviceFactoryProvider
        );

        $this->firstDependencyFactory = $this->setupServiceFactoryForClass(new FirstDependency());

        $this->serviceFactoryProvider->method('createFactory')->willReturnMap([
            [FirstDependency::class, [], null, $this->firstDependencyFactory]
        ]);
    }

    private function setupServiceFactoryForClass(object $object)
    {
        $stub = $this->createStub(ServiceFactoryInterface::class);
        $stub->method('getInstance')->willReturn($object);
        $stub->method('getNameForRegistration')->willReturn(FirstDependency::class);
        return $stub;
    }

    /**
     * @return void
     */
    public function testSimpleDependencyInjectionService()
    {
        $serviceFactoryStub = $this->createStub(ServiceFactoryInterface::class);
        $serviceFactoryStub->method('getNameForRegistration')->willReturn('service1');
        $serviceFactoryStub->method('getInstance')->willReturn(new FirstDependency());
        $di = $this->dependencyInjection;
        $di->registerService($serviceFactoryStub);
        $service = $di->getService('service1');
        $this->assertInstanceOf(FirstDependency::class, $service);
    }

    /**
     * @return void
     */
    public function testSimpleDependencyInjectionParam()
    {
        $paramFactoryStub = $this->createStub(ParamFactoryInterface::class);
        $paramFactoryStub
            ->method('getNameForRegistration')->willReturn('param.test');
        $paramFactoryStub
            ->method('getValue')->willReturn('This is a test param');

        $di = $this->dependencyInjection;
        $di->registerParam($paramFactoryStub);
        $this->assertIsString($di->getParam('param.test'));
        $this->assertEquals('This is a test param', $di->getParam('param.test'));
    }

    /**
     * @return void
     */
    public function testUnregisterServiceInstantiationStack()
    {
        $stack = [];
        $testCase = $this;
        $this->serviceInstanciationStack
            ->expects($this->once())
            ->method('checkForCircularDependency')
            ->willReturnCallback(function (string $service) use (&$stack) {
                $stack[] = $service;
            });
        $this->serviceInstanciationStack
            ->expects($this->once())
            ->method('unregisterDependencyStack')
            ->willReturnCallback(function (string $service) use (&$stack, $testCase) {
                if (!in_array($service, $stack)) {
                    $testCase->fail('Service Instanciation Stack not called properly');
                }
            });

        $this->dependencyInjection->getService(FirstDependency::class);
    }

    /**
     * @return void
     */
    public function testWithRealCircularDependency()
    {
        $this->serviceInstanciationStack
            ->expects($this->once())
            ->method('checkForCircularDependency')
            ->willReturnCallback(function(string $service) {
                throw new CircularDependencyException([$service, $service]);
        });

        $this->expectException(CircularDependencyException::class);
        $this->dependencyInjection->getService(FirstDependency::class);
    }

    /**
     * @return void
     */
    public function testMissingService()
    {
        $this->expectException(MissingServiceException::class);
        $this->dependencyInjection->getService('This\Class\Does\Not\Exist');
    }

    /**
     * @return void
     */
    public function testInstantiationForUnregisteredService()
    {
        $service = $this->dependencyInjection->getService(FirstDependency::class);
        $this->assertInstanceOf(FirstDependency::class, $service);
    }

    /**
     * @return void
     */
    public function testMissingParameter()
    {
        $this->expectException(MissingParameterException::class);
        $this->dependencyInjection->getParam('this.does.not.exist');
    }
}
