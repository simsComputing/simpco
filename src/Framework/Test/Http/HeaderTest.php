<?php

namespace Simpco\Framework\Test\Http;

use Simpco\Framework\Http\Header;
use Simpco\Framework\Test\AbstractValueBagTestCase;

/**
 * @covers \Simpco\Framework\Http\Header
 */
class HeaderTest extends AbstractValueBagTestCase
{
    protected function getClassName(): string
    {
        return Header::class;
    }

    public static function getDataSet(): array
    {
        return [
            [['getName', 'Content-Type'], ['getValue', 'application/json']]
        ];
    }
}
