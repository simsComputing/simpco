<?php

namespace Simpco\Framework\Test\Http;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\Http\HeaderFormatter;

/**
 * @covers \Simpco\Framework\Http\HeaderFormatter
 */
class HeaderFormatterTest extends TestCase
{
    public function testContentType()
    {
        $formatter = new HeaderFormatter();

        $this->assertEquals('Content-Type', $formatter->format('HTTP_CONTENT_TYPE'));
    }
}