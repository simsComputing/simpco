<?php

namespace Simpco\Framework\Test;
use PHPUnit\Framework\TestCase;

abstract class AbstractValueBagTestCase extends TestCase
{
    abstract protected function getClassName(): string;

    /**
     * @dataProvider getDataSet
     *
     * @param [type] ...$args
     * @return void
     */
    public function testValues(...$args)
    {
        $class = $this->getClassName();
        $instanceArgs = array_map([$this, 'getArgValue'], $args);
        $instance = new $class(...$instanceArgs);

        foreach ($args as $methodAndValue)
        {
            $methodName = $methodAndValue[0];
            $this->assertEquals($methodAndValue[1], $instance->$methodName());
        }
    }

    /**
     * @dataProvider getDataSet
     *
     * @param [type] ...$args
     * @return void
     */
    public function testFactory(...$args)
    {
        $class = $this->getClassName();
        $instanceArgs = array_map([$this, 'getArgValue'], $args);

        $instance = $class::create(...$instanceArgs);
        $this->assertInstanceOf($class, $instance);
    }

    private function getArgValue(array $methodAndValue): string
    {
        return $methodAndValue[1];
    }

    abstract public static function getDataSet(): array;
}