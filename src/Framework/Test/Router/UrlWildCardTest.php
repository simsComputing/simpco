<?php

namespace Simpco\Framework\Test\Router;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\Router\UrlWildcard;

/**
 * @covers \Simpco\Framework\Router\UrlWildcard
 */
class UrlWildcardTest extends TestCase
{
    public function testRetrieveWildCards()
    {
        $urlWildcard = new UrlWildcard();

        $urlWildcard->push('test');
        $urlWildcard->push('test2');
        $urlWildcard->push('test3');

        $this->assertEquals('test', $urlWildcard->get(0));
        $this->assertEquals('test2', $urlWildcard->get(1));
        $this->assertEquals('test3', $urlWildcard->get(2));
        $this->assertEquals(null, $urlWildcard->get(3));

        $urlWildcard->reset();

        $this->assertNull($urlWildcard->get(0));
    }
}