<?php

namespace Simpco\Framework\Test\Router;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\Http\HeaderInterface;
use Simpco\Framework\ResponseInterface;
use Simpco\Framework\Router\JavascriptRoute;
use Simpco\Framework\Router\UrlWildcard;
use Simpco\Framework\Router\UrlWildcardInterface;
use Simpco\Framework\ViewFactory;
use Simpco\Framework\ViewInterface;

/**
 * @covers \Simpco\Framework\Router\JavascriptRoute
 * @uses \Simpco\Framework\Http\Header
 */
class JavascriptRouteTest extends TestCase
{
        /**
     * @var ViewFactory
     */
    private ViewFactory $viewFactory;

    private UrlWildcardInterface $urlWildcard;

    private ResponseInterface $response;

    private ViewInterface $view;

    public function setUp(): void
    {
        $this->viewFactory = $this->createMock(ViewFactory::class);
        $this->urlWildcard = $this->createStub(UrlWildcard::class);
        $this->response = $this->createMock(ResponseInterface::class);
        $this->view = $this->createStub(ViewInterface::class);
    }

    public function testSimpleUseCase(): void
    {
        $header = null;
        $response = $this->response;
        $this->response->method('addHeader')->willReturnCallback(function(HeaderInterface $headerArg) use (&$header, $response) {
            $header = $headerArg;
            return $response;

        });

        $this->viewFactory
            ->expects($this->once())
            ->method('create')
            ->with('empty.phtml', '/js/test/path', [], [], 'empty.phtml')
            ->willReturn($this->view);

        $this->urlWildcard->method('get')->willReturnMap([
            [0, 'test'],
            [1, 'path'],
            [2, null]
        ]);

        $route = new JavascriptRoute(
            $this->viewFactory,
            $this->urlWildcard,
            $this->response
        );

        $route->execute();
    }
}