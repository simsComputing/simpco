<?php

namespace Simpco\Framework\Test\Router;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\RequestInterface;
use Simpco\Framework\Router\ModuleRoutesRegistry;

/**
 * @covers \Simpco\Framework\Router\ModuleRoutesRegistry
 */
class ModuleRoutesRegistryTest extends TestCase
{
    const MODULES_MAPPING = [
        'module1' => [
            RequestInterface::GET => [
                'module1/test',
                'module1/test2'
            ],
            RequestInterface::POST => [
                'module1/test',
                'module1/test2'
            ],
            RequestInterface::PUT => [
                'module1/test',
                'module1/test2'
            ]
        ],
        'module2' => [
            RequestInterface::GET => [
                'module2/test',
                'module2/test2'
            ],
            RequestInterface::POST => [
                'module2/test',
                'module2/test2'
            ],
            RequestInterface::PUT => [
                'module2/test',
            ]
        ]  
    ];

    const RESULT_MAPPING = [
        RequestInterface::GET => [
            'module1/test',
            'module1/test2',
            'module2/test',
            'module2/test2'
        ],
        RequestInterface::POST => [
            'module1/test',
            'module1/test2',
            'module2/test',
            'module2/test2'
        ],
        RequestInterface::PUT => [
            'module1/test',
            'module1/test2',
            'module2/test'
        ]
    ];

    public function testListIsRegisteredProperly(): void
    {
        $registry = new ModuleRoutesRegistry();
        foreach (self::MODULES_MAPPING as $routes) {
            $registry->registerAll($routes);
        }
        
        foreach (self::RESULT_MAPPING as $method => $routes) {
            $this->assertEquals($routes, $registry->getList($method));
        }
    }
}