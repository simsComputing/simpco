<?php

namespace Simpco\Framework\Test;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\Http\HeaderInterface;
use Simpco\Framework\Response;
use Simpco\Framework\ResponseInterface;

/**
 * @covers \Simpco\Framework\Response
 */
class ResponseTest extends TestCase
{
    public function testSimpleResponse(): void
    {
        $response = new Response();

        $response->setBody('Test body');
        $this->assertEquals('Test body', $response->getBody());
        
        $header = $this->createStub(HeaderInterface::class);
        $header->method('getName')->willReturn('Content-Type');

        $response->addHeader($header);

        $this->assertArrayHasKey('Content-Type', $response->getHeaders());

        $response->removeHeader('Content-Type');

        $this->assertArrayNotHasKey('Content-Type', $response->getHeaders());
    }

    public function testFactory(): void
    {
        $response = Response::create();
        $this->assertInstanceOf(Response::class, $response);
        $this->assertInstanceOf(ResponseInterface::class, $response);
    }
}