<?php

namespace Simpco\Framework\Test\Fake\Routes;

use Simpco\Framework\Router\RouteInterface;
use Simpco\Framework\ViewInterface;

class AbstractFakeRoute implements RouteInterface
{
    public function execute(): ViewInterface
    {
        throw new \Exception('Fake route cannot be executed');
    }
}