<?php

namespace Simpco\Framework\Test\View;

use PHPUnit\Framework\MockObject\Stub;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\FileSystem\Scanner;
use Simpco\Framework\View\Pool;

/**
 * @covers \Simpco\Framework\View\Pool
 */
class PoolTest extends TestCase
{
    /**
     * @var Scanner|Stub
     */
    private Scanner $scanner;

    private array $viewDirs = [
        '/module1/views',
        '/module2/views',
        '/module3/views',
    ];

    public function setUp(): void
    {
        $this->scanner = $this->createStub(Scanner::class);
    }

    /**
     * @return void
     */
    public function testResolveWithPriorityForTemplates()
    {
        $this->testResolveWithPriorityForFolder('resolveTemplate', Pool::TEMPLATE_FOLDER);
    }

    /**
     * @return void
     */
    public function testResolveWithPriorityForLayouts()
    {
        $this->testResolveWithPriorityForFolder('resolveLayout', Pool::LAYOUT_FOLDER);
    }

    /**
     * @return void
     */
    public function testResolveWithPriorityForMains()
    {
        $this->testResolveWithPriorityForFolder('resolveMain', Pool::MAIN_FOLDER);
    }

    /**
     * @return void
     */
    public function testResolveNullWhenThereIsNoneForTemplates()
    {
        $this->testResolveWNullWhenThereIsNoneForFolder('resolveTemplate', Pool::TEMPLATE_FOLDER);
    }

    /**
     * @return void
     */
    public function testResolveNullWhenThereIsNoneForLayouts()
    {
        $this->testResolveWNullWhenThereIsNoneForFolder('resolveLayout', Pool::LAYOUT_FOLDER);
    }

    /**
     * @return void
     */
    public function testResolveNullWhenThereIsNoneForMains()
    {
        $this->testResolveWNullWhenThereIsNoneForFolder('resolveMain', Pool::MAIN_FOLDER);
    }

    /**
     * @param string $resolveMethod
     * @param string $folderName
     * @return void
     */
    private function testResolveWithPriorityForFolder(string $resolveMethod, string $folderName)
    {
        $this->scanner->method('fileExists')->willReturnMap([
            ['/module1/views' . $folderName . 'file1', true],
            ['/module2/views' . $folderName . 'file1', true],
            ['/module3/views' . $folderName . 'file1', false],
        ]);
        $pool = new Pool($this->viewDirs, $this->scanner);
        $this->assertEquals('/module1/views' . $folderName . 'file1', $pool->$resolveMethod('file1'));
    }

    /**
     * @param string $resolveMethod
     * @param string $folderName
     * @return void
     */
    private function testResolveWNullWhenThereIsNoneForFolder(string $resolveMethod, string $folderName)
    {
        $this->scanner->method('fileExists')->willReturnMap([
            ['/module1/views' . $folderName . 'file1', false],
            ['/module2/views' . $folderName . 'file1', false],
            ['/module3/views' . $folderName . 'file1', false],
        ]);
        $pool = new Pool($this->viewDirs, $this->scanner);
        $this->assertEquals(null, $pool->$resolveMethod('file1'));
    }
}
