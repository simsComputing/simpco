<?php

namespace Simpco\Framework\Test\View;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\View\HeadTagInterface;
use Simpco\Framework\View\HeadTagPool;

/**
 * @covers \Simpco\Framework\View\HeadTagPool
 */
class HeadTagPoolTest extends TestCase
{
    public function testSimpleUseCase(): void
    {
        $headTags = [];
        for ($i = 0; $i < 10; $i++) {
            $headTags[] = $this->createStub(HeadTagInterface::class);
        }

        $pool = new HeadTagPool($headTags);

        $this->assertEquals(count($headTags), count($pool->getList()));
    }
}