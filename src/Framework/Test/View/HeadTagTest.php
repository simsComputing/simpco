<?php 

namespace Simpco\Framework\Test\View;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\View\HeadTag;

/**
 * @covers \Simpco\Framework\View\HeadTag
 */
class HeadTagTest extends TestCase
{
    public function testSimpleHeadTag()
    {
        $headTag = new HeadTag('meta', ['charset' => 'UTF-8'], false);

        $this->assertEquals('meta', $headTag->getTagName());
        $this->assertEquals(['charset' => 'UTF-8'], $headTag->getMetas());
        $this->assertEquals(false, $headTag->isAutoClose());
    }

    public function testHeadTagFactory()
    {
        $headTag = HeadTag::create('meta', ['charset' => 'UTF-8']);

        $this->assertEquals('meta', $headTag->getTagName());
        $this->assertEquals(['charset' => 'UTF-8'], $headTag->getMetas());
        $this->assertEquals(true, $headTag->isAutoClose());
    }
}