<?php

namespace Simpco\Framework\Test\View;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\Exception\MissingAssetException;
use Simpco\Framework\View\HeadTag\HtmlPrinter;
use Simpco\Framework\View\Includer;
use Simpco\Framework\View\ViewPoolInterface;
use Simpco\Framework\ViewInterface;

/**
 * @covers \Simpco\Framework\View\Includer
 */
class IncluderTest extends TestCase
{
    private ViewPoolInterface $viewPool;

    private ViewInterface $view;

    private HtmlPrinter $htmlPrinter;

    public function setUp(): void
    {
        $this->viewPool = $this->createMock(ViewPoolInterface::class);
        $this->view = $this->createMock(ViewInterface::class);
        $this->htmlPrinter = $this->createStub(HtmlPrinter::class);
    }

    public function testIncludeMainFile(): void
    {
        $this->viewPool
            ->expects($this->once())
            ->method('resolveMain')
            ->with('main.phtml')
            ->willReturn(__DIR__ . '/Fake/main.phtml');

        $this->view
            ->expects($this->once())
            ->method('getMainFile')
            ->willReturn('main.phtml');


        $includer = $this->createIncluder();

        $fileContent = $includer->includeMainFile($this->view, $this->htmlPrinter);

        $this->assertEquals('<main>main</main>', $fileContent);
    }

    public function testIncludeMissingMainFile(): void
    {
        $this->viewPool
            ->expects($this->once())
            ->method('resolveMain')
            ->with('missing.phtml')
            ->willReturn(null);

        $this->view
            ->expects($this->exactly(2))
            ->method('getMainFile')
            ->willReturn('missing.phtml');

        $includer = $this->createIncluder();
        $this->expectException(MissingAssetException::class);
        $includer->includeMainFile($this->view, $this->htmlPrinter);
    }

    public function testIncludeLayout(): void
    {
        $this->viewPool
            ->expects($this->once())
            ->method('resolveLayout')
            ->with('layout.phtml')
            ->willReturn(__DIR__ . '/Fake/layout.phtml');

        $this->view
            ->expects($this->once())
            ->method('getLayout')
            ->willReturn('layout.phtml');

        $includer = $this->createIncluder();
        $fileContent = $includer->includeLayout($this->view);
        $this->assertEquals('<main>layout</main>', $fileContent);
    }

    public function testIncludMissingeLayout(): void
    {
        $this->viewPool
            ->expects($this->once())
            ->method('resolveLayout')
            ->with('missing.phtml')
            ->willReturn(null);

        $this->view
            ->expects($this->exactly(2))
            ->method('getLayout')
            ->willReturn('missing.phtml');

        $includer = $this->createIncluder();

        $this->expectException(MissingAssetException::class);
        $includer->includeLayout($this->view);
    }

    public function testIncludeTemplate(): void
    {
        $this->viewPool
            ->expects($this->once())
            ->method('resolveTemplate')
            ->with('template.phtml')
            ->willReturn(__DIR__ . '/Fake/template.phtml');

        $this->view
            ->expects($this->once())
            ->method('getTemplate')
            ->willReturn('template.phtml');

        $includer = $this->createIncluder();
        $fileContent = $includer->includeTemplate($this->view);
        $this->assertEquals('<main>template</main>', $fileContent);
    }

    public function testIncludeMissingTemplate(): void
    {
        $this->viewPool
            ->expects($this->once())
            ->method('resolveTemplate')
            ->with('missing.phtml')
            ->willReturn(null);

        $this->view
            ->expects($this->exactly(2))
            ->method('getTemplate')
            ->willReturn('missing.phtml');

        $includer = $this->createIncluder();
        $this->expectException(MissingAssetException::class);
        $includer->includeTemplate($this->view);
    }

    private function createIncluder(): Includer
    {
        $includer = new Includer($this->viewPool);
        return $includer;
    }
}