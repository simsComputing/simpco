<?php

namespace Simpco\Framework\Test\View\HeadTag;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\View\HeadTag\ScriptTag;
use Simpco\Framework\View\HeadTagInterface;

/**
 * @covers \Simpco\Framework\View\HeadTag\ScriptTag
 * @uses  \Simpco\Framework\View\HeadTag
 */
class ScriptTagTest extends TestCase
{
    public function testFactory()
    {
        $scriptTag = new ScriptTag();
        $tag = $scriptTag::create('https://test.js');
        $this->assertInstanceOf(HeadTagInterface::class, $tag);
        $this->assertEquals('script', $tag->getTagName());
    }

    public function testFactoryForLocal()
    {
        $scriptTag = new ScriptTag();
        $tag = $scriptTag::createForLocalFile('/bonjour.js');
        $this->assertInstanceOf(HeadTagInterface::class, $tag);
        $this->assertEquals('script', $tag->getTagName());
    }
}
