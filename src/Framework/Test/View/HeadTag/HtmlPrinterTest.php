<?php 

namespace Simpco\Framework\Test\View\HeadTag;

use Simpco\Framework\View\HeadTag\HtmlPrinter;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\View\HeadTagInterface;

/**
 * @covers \Simpco\Framework\View\HeadTag\HtmlPrinter
 */
class HtmlPrinterTest extends TestCase
{
    /**
     * @dataProvider getTestPrinterDataSet
     * @param [type] $tagName
     * @param [type] $metas
     * @param [type] $autoClose
     * @param [type] $expected
     * @return void
     */
    public function testPrinter($tagName, $metas, $autoClose, $expected)
    {
        $stub = $this->createStub(HeadTagInterface::class);
        $stub->method('getTagName')->willReturn($tagName);
        $stub->method('getMetas')->willReturn($metas);
        $stub->method('isAutoClose')->willReturn($autoClose);

        $printer = new HtmlPrinter();

        $printedValue = $printer->print($stub);

        $this->assertEquals($printedValue, $expected);
    }

    public static function getTestPrinterDataSet(): array
    {
        return [
            [
                'script',
                ['src' => './index.js'],
                false,
                '<script src="./index.js"></script>'
            ],
            [
                'meta',
                ['charset' => 'UTF-8'],
                true,
                '<meta charset="UTF-8" />'
            ]
        ];
    }
}