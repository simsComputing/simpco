<?php

namespace Simpco\Framework\Test\DependencyInjection\ServiceFactory;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolverInterface;
use Simpco\Framework\DependencyInjection\ServiceFactory\FakeFactory;
use Simpco\Framework\DependencyInjectionInterface;
use Simpco\Framework\Exception\DependencyInjectionException;

/**
 * @covers \Simpco\Framework\DependencyInjection\ServiceFactory\FakeFactory
 */ 
class FakeFactoryTest extends TestCase
{
    /**
     * @dataProvider getDataSetForReturnValues
     * @param [type] $instance
     * @param [type] $name
     * @return void
     */
    public function testReturnValues($instance, $name)
    {
        $factory = new FakeFactory($instance, $name);
        
        $di = $this->createMock(DependencyInjectionInterface::class);
        $di->expects($this->never())->method('getService');

        $resolver = $this->createMock(ServiceArgumentResolverInterface::class);
        $resolver->expects($this->never())->method('resolve');

        $this->assertEquals($name, $factory->getNameForRegistration());
        $this->assertSame($instance, $factory->getInstance($di, $resolver));
    }

    public static function getDataSetForReturnValues(): array
    {
        return [
            [(object)['is' => 'ok'], 'test'],
            [(object)['is' => 'oktoo'], 'test1'],
            [(object)['is' => 'oktootoo'], 'test2'],
        ];
    }

    public function testExceptionOnArgumentModification(): void
    {
        $factory = new FakeFactory((object)[], 'test');
        $this->expectException(DependencyInjectionException::class);

        $modifier = $this->createStub(ServiceArgumentModifierInterface::class);
        $factory->modifyArgument($modifier);
    }
}