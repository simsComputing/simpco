<?php 

namespace Simpco\Framework\Test\DependencyInjection;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\DependencyInjection\ServiceFactoryProvider;
use Simpco\Framework\Exception\DependencyInjectionException;
use Simpco\Framework\Test\DependencyInjection\Fake\FailedServiceFactoryForTests;
use Simpco\Framework\Test\DependencyInjection\Fake\ServiceFactoryForTests;

/**
 * @covers \Simpco\Framework\DependencyInjection\ServiceFactoryProvider
 */
class ServiceFactoryProviderTest extends TestCase
{
    public function testFailIfClassDoesntExist(): void
    {
        $this->expectException(DependencyInjectionException::class);
        new ServiceFactoryProvider('Fake\Class\Who\Does\Not\Exist');
    }

    public function testFailIfClassDoesNotHaveInterface(): void
    {
        $this->expectException(DependencyInjectionException::class);
        new ServiceFactoryProvider(FailedServiceFactoryForTests::class);
    }

    public function testProvideFactory(): void
    {
        $provider = new ServiceFactoryProvider(ServiceFactoryForTests::class);
        $factory = $provider->createFactory('nomatter');
        $this->assertInstanceOf(ServiceFactoryForTests::class, $factory);
    }

    public function testStaticFactory(): void
    {
        $provider = ServiceFactoryProvider::create(ServiceFactoryForTests::class);
        $this->assertInstanceOf(ServiceFactoryProvider::class, $provider);
    }
}
