<?php 

namespace Simpco\Framework\Test\DependencyInjection\Fake;

class MainService
{
    const PARAM_NAME = '%param.test%';

    private FirstDependency $firstDependency;

    private SecondDependency $secondDependency;

    private string $param;

    public function __construct(
        FirstDependency $firstDependency,
        SecondDependency $secondDependency,
        string $param
    ) {
        $this->firstDependency = $firstDependency;
        $this->secondDependency = $secondDependency;
        $this->param = $param;
    }

    /**
     * Get the value of secondDependency
     */ 
    public function getSecondDependency()
    {
        return $this->secondDependency;
    }

    /**
     * Get the value of firstDependency
     */ 
    public function getFirstDependency()
    {
        return $this->firstDependency;
    }

    public function getParam()
    {
        return $this->param;
    }
}