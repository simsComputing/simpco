<?php 

namespace Simpco\Framework\Test\DependencyInjection\Fake;

use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolverInterface;
use Simpco\Framework\DependencyInjectionInterface;

class FailedServiceFactoryForTests
{
    public function getInstance(
        DependencyInjectionInterface $dependencyInjection, 
        ServiceArgumentResolverInterface $serviceArgumentResolver,
        bool $forceCreation = false
    ): object {
        return (object)[];
    }

    public function getNameForRegistration(): string
    {
        return 'test';
    }

    public function modifyArgument(ServiceArgumentModifierInterface $serviceArgumentModifier): void
    {
        return;
    }
}