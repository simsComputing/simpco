<?php

namespace Simpco\Framework\Test\DependencyInjection\Fake;

class WithObjectDependency
{
    private object $ofFirstDependency;

    public function __construct(
        object $ofFirstDependency
    ) {
        $this->ofFirstDependency = $ofFirstDependency;
    }

    public function getOfFirstDependency(): object
    {
        return $this->ofFirstDependency;
    }
}