<?php 

namespace Simpco\Framework\Test\DependencyInjection\Fake;

use Simpco\Framework\DependencyInjection\DefaultServiceFactoryInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolverInterface;
use Simpco\Framework\DependencyInjection\ServiceFactoryInterface;
use Simpco\Framework\DependencyInjectionInterface;

class ServiceFactoryForTests implements ServiceFactoryInterface, DefaultServiceFactoryInterface
{
    public function getInstance(
        DependencyInjectionInterface $dependencyInjection, 
        ServiceArgumentResolverInterface $serviceArgumentResolver,
        bool $forceCreation = false
    ): object {
        return (object)[];
    }

    public function getNameForRegistration(): string
    {
        return 'test';
    }

    public function modifyArgument(ServiceArgumentModifierInterface $serviceArgumentModifier): void
    {
        return;
    }

    public static function createFactory(string $serviceClass, array $arguments = [], string $alias = null): self
    {
        return new ServiceFactoryForTests();
    }
}