<?php

namespace Simpco\Framework\Test\DependencyInjection\Fake;

class WithArrayDependency
{
    private array $ofFirstDependency;

    public function __construct(
        array $ofFirstDependency
    ) {
        $this->ofFirstDependency = $ofFirstDependency;
    }

    public function getOfFirstDependency(): array
    {
        return $this->ofFirstDependency;
    }
}