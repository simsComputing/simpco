<?php

namespace Simpco\Framework\Test\DependencyInjection;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\DependencyInjection;
use Simpco\Framework\DependencyInjection\DependencyInjectionFactory;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolver;
use Simpco\Framework\Exception\DependencyInjectionException;

/**
 * @covers \Simpco\Framework\DependencyInjection\DependencyInjectionFactory
 */
class DependencyInjectionFactoryTest extends TestCase
{
    private ServiceArgumentResolver $serviceArgumentResolver;

    private DependencyInjection $dependencyInjection;

    public function setUp(): void
    {
        $this->serviceArgumentResolver = $this->createStub(ServiceArgumentResolver::class);
        $this->dependencyInjection = $this->createStub(DependencyInjection::class);
    }

    /**
     * @return void
     */
    public function testGetInstance()
    {
        $factory = new DependencyInjectionFactory();

        $di = $factory->getInstance($this->dependencyInjection, $this->serviceArgumentResolver);

        $this->assertSame($this->dependencyInjection, $di);
    }

    /**
     * @return void
     */
    public function testGetNameForRegistration()
    {
        $factory = new DependencyInjectionFactory();
        $this->assertEquals(DependencyInjection::class, $factory->getNameForRegistration());
    }

    /**
     * @return void
     */
    public function testCreate()
    {
        $diFactory = DependencyInjectionFactory::create();
        $this->assertInstanceOf(DependencyInjectionFactory::class, $diFactory);
    }

    /**
     * @return void
     */
    public function testModifyArgument()
    {
        $modifier = $this->createStub(ServiceArgumentModifierInterface::class);
        $factory = new DependencyInjectionFactory();
        $this->expectException(DependencyInjectionException::class);
        $factory->modifyArgument($modifier);
    }
}