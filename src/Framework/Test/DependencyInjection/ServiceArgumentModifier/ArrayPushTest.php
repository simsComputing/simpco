<?php 

namespace Simpco\Framework\Test\DependencyInjection\ServiceArgumentModifier;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\DependencyInjection;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifier\ArrayPush;
use Simpco\Framework\Exception\DependencyInjectionException;
use Simpco\Framework\Test\DependencyInjection\Fake\FirstDependency;

/**
 * @covers \Simpco\Framework\DependencyInjection\ServiceArgumentModifier\ArrayPush
 */
class ArrayPushTest extends TestCase
{
    public function testArrayPushEmptyArray()
    {
        $modifier = new ArrayPush('testArgument', [FirstDependency::class]);

        $this->assertEquals('testArgument', $modifier->getArgumentName());
        $this->assertEquals([FirstDependency::class], $modifier->modify([]));
    }

    public function testArrayPushCurrentValueIsNull()
    {
        $modifier = new ArrayPush('testArgument', [FirstDependency::class]);

        $this->assertEquals('testArgument', $modifier->getArgumentName());
        $this->assertEquals([FirstDependency::class], $modifier->modify(null));
    }

    public function testArrayPushFailsIfArgumentIsNotArray()
    {
        $modifier = new ArrayPush('testArgument', [FirstDependency::class]);
        $this->expectException(DependencyInjectionException::class);
        $modifier->modify('testArg');
    }

    public function testFactory()
    {
        $this->assertInstanceOf(ArrayPush::class, ArrayPush::create('argumentName', [FirstDependency::class]));
    }
}
