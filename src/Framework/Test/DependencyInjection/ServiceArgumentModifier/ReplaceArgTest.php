<?php 

namespace Simpco\Framework\Test\DependencyInjection\ServiceArgumentModifier;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifier\ReplaceArg;

/**
 * @covers \Simpco\Framework\DependencyInjection\ServiceArgumentModifier\ReplaceArg
 */
class ReplaceArgTest extends TestCase
{
    public function testReplaceArg()
    {
        $modifier = new ReplaceArg('testArgument', 'argValue');

        $this->assertEquals('testArgument', $modifier->getArgumentName());
        $this->assertEquals('argValue', $modifier->modify([]));
    }

    public function testFactory()
    {
        $modifier = ReplaceArg::create('testArgument', 'argValue');
        $this->assertInstanceOf(ReplaceArg::class, $modifier);
    }
}