<?php 

namespace Simpco\Framework\Test\DependencyInjection;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\Exception\CircularDependencyException;
use Simpco\Framework\DependencyInjection\ServiceInstanciationStack;

/**
 * @covers \Simpco\Framework\DependencyInjection\ServiceInstanciationStack
 */
class ServiceInstanciationStackTest extends TestCase
{
    const STACK_NO_CIRCULAR_DEPENDENCY = [
        'service1',
        'service2',
        'service3',
        'service4',
        'service5',
        'service6',
        'service7',
        'service8',
    ];

    const STACK_CIRCULAR_DEPENDENCY = [
        'service1',
        'service2',
        'service3',
        'service4',
        'service5',
        'service6',
        'service7',
        'service2',
    ];

    const STACK_WITH_UNREGISTER_NO_CIRCULAR = [
        'service1',
        'service2',
        'service3',
        'service4',
        'service5',
        'unregister' => 'service2',
        'service6',
        'service7',
        'service2',
    ];

    public function testFactory()
    {
        $stack = ServiceInstanciationStack::create();
        $this->assertInstanceOf(ServiceInstanciationStack::class, $stack);
    }

    public function testNoCircularDependency()
    {
        $stack = new ServiceInstanciationStack();
        $this->expectNotToPerformAssertions();
        foreach (self::STACK_NO_CIRCULAR_DEPENDENCY as $dep) {
            $stack->checkForCircularDependency($dep);
        }
    }

    public function testCircularDependency()
    {
        $stack = new ServiceInstanciationStack();
        $this->expectException(CircularDependencyException::class);
        foreach (self::STACK_CIRCULAR_DEPENDENCY as $dep) {
            $stack->checkForCircularDependency($dep);
        }
    }

    public function testNoCircularDependencyWithUnregister()
    {
        $stack = new ServiceInstanciationStack();
        $this->expectNotToPerformAssertions();
        foreach (self::STACK_NO_CIRCULAR_DEPENDENCY as $index => $dep) {
            if ($index === 'unregister') {
                $stack->unregisterDependencyStack($dep);                
            } else {
                $stack->checkForCircularDependency($dep);
            }
        }
    }

    public function testStackPosition()
    {
        $stack = new ServiceInstanciationStack();
        foreach (self::STACK_NO_CIRCULAR_DEPENDENCY as $dep) {
            $stack->checkForCircularDependency($dep);
        }

        $stackPosition = $stack->getStackPosition(self::STACK_NO_CIRCULAR_DEPENDENCY[0]);
        $stackPosition2 = $stack->getStackPosition(self::STACK_NO_CIRCULAR_DEPENDENCY[4]);

        $this->assertEquals(0, $stackPosition);
        $this->assertEquals(4, $stackPosition2);
    }

    public function testServiceRegistration()
    {
        $stack = new ServiceInstanciationStack();
        $stack->checkForCircularDependency('service1');
        
        $this->assertContains('service1', $stack->getStack());

        $stack->unregisterDependencyStack('service1');

        $this->assertNotContains('service1', $stack->getStack());

    }
}