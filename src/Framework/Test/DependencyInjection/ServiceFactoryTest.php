<?php

namespace Simpco\Framework\Test\DependencyInjection;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Simpco\Framework\DependencyInjection;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolver;
use Simpco\Framework\DependencyInjection\ServiceFactory;
use Simpco\Framework\Exception\DependencyInjectionException;
use Simpco\Framework\Test\DependencyInjection\Fake\FirstDependency;
use Simpco\Framework\Test\DependencyInjection\Fake\MainService;
use Simpco\Framework\Test\DependencyInjection\Fake\SecondDependency;
use Simpco\Framework\Test\DependencyInjection\Fake\WithArrayDependency;
use Simpco\Framework\Test\DependencyInjection\Fake\WithObjectDependency;

/**
 * @covers \Simpco\Framework\DependencyInjection\ServiceFactory
 */
class ServiceFactoryTest extends TestCase
{
    /**
     * @var DependencyInjection|MockObject
     */
    private DependencyInjection $dependencyInjection;

    private ServiceArgumentResolver $serviceArgumentResolver;

    public function setUp(): void
    {
        $this->dependencyInjection = $this->createStub(DependencyInjection::class);
        $this->dependencyInjection
            ->method('getService')
            ->willReturnMap([
                [FirstDependency::class, new FirstDependency()],
                [SecondDependency::class, new SecondDependency()]
            ]);
        $this->dependencyInjection
            ->method('getParam')
            ->willReturnMap([
                ['param.test', 'Test'],
                ['param.test.modified', 'Test Modified']
            ]);

        $this->serviceArgumentResolver = $this->createStub(ServiceArgumentResolver::class);
        $this->serviceArgumentResolver
            ->method('resolve')
            ->willReturn([FirstDependency::class, SecondDependency::class, MainService::PARAM_NAME]);
    }

    /**
     * @return void
     */
    public function testSimpleFactory()
    {
        $factory = new ServiceFactory(
            MainService::class,
            [FirstDependency::class, SecondDependency::class, MainService::PARAM_NAME]
        );
        $this->assertEquals(MainService::class, $factory->getNameForRegistration());
        $this->assertInstanceOf(MainService::class, $factory->getInstance($this->dependencyInjection, $this->serviceArgumentResolver));
    }

    public function testSimpleFactoryWithAlias()
    {
        $factory = new ServiceFactory(
            MainService::class,
            [FirstDependency::class, SecondDependency::class, MainService::PARAM_NAME],
            'alias'
        );
        $this->assertEquals('alias', $factory->getNameForRegistration());
    }

    public function testServiceFactoryFactory()
    {
        $factory = ServiceFactory::createFactory(
            MainService::class,
            [FirstDependency::class, SecondDependency::class, MainService::PARAM_NAME]
        );

        $this->assertInstanceOf(ServiceFactory::class, $factory);
        $this->assertInstanceOf(MainService::class, $factory->getInstance($this->dependencyInjection, $this->serviceArgumentResolver));
    }

    public function testSingleton()
    {
        $factory = new ServiceFactory(
            MainService::class,
            [FirstDependency::class, SecondDependency::class, MainService::PARAM_NAME]
        );

        $instance1 = $factory->getInstance($this->dependencyInjection, $this->serviceArgumentResolver);
        $instance2 = $factory->getInstance($this->dependencyInjection, $this->serviceArgumentResolver);

        $this->assertSame($instance1, $instance2);
    }

    public function testForceCreation()
    {
        $factory = new ServiceFactory(
            MainService::class,
            [FirstDependency::class, SecondDependency::class, MainService::PARAM_NAME]
        );

        $instance1 = $factory->getInstance($this->dependencyInjection, $this->serviceArgumentResolver);
        $instance2 = $factory->getInstance($this->dependencyInjection, $this->serviceArgumentResolver, true);

        $this->assertNotSame($instance1, $instance2);
    }

    public function testBashFactory()
    {
        $services = ServiceFactory::bashFactory(
            [
                [MainService::class, [FirstDependency::class, SecondDependency::class]],
                [FirstDependency::class],
                [SecondDependency::class],
                [MainService::class, [FirstDependency::class, SecondDependency::class], 'alias']
            ]
        );
        foreach ($services as $service) {
            $this->assertInstanceOf(ServiceFactory::class, $service);
        }
    }

    /**
     * @return void
     */
    public function testResolveImplicitArguments()
    {
        $factory = new ServiceFactory(MainService::class, ['param' => '%param.test%']);
        $service = $factory->getInstance($this->dependencyInjection, $this->serviceArgumentResolver);
        $this->assertInstanceOf(MainService::class, $service);
    }

    public function testModifyArgument()
    {
        $factory = new ServiceFactory(
            MainService::class,
            [FirstDependency::class, SecondDependency::class, MainService::PARAM_NAME]
        );

        $modifier = $this->createStub(ServiceArgumentModifierInterface::class);
        $modifier->method('getArgumentName')->willReturn('param');
        $modifier->method('modify')->willReturn('%param.test.modified%');

        $factory->modifyArgument($modifier);

        $argumentResolver = $this->createMock(ServiceArgumentResolver::class);
        $argumentResolver
            ->expects($this->once())
            ->method('resolve')
            ->with(
                MainService::class,
                [FirstDependency::class, SecondDependency::class, MainService::PARAM_NAME, 'param' => '%param.test.modified%']
            )
            ->willReturn([FirstDependency::class, SecondDependency::class, '%param.test.modified%']);

        $instance = $factory->getInstance($this->dependencyInjection, $argumentResolver, false);

        $this->assertEquals('Test Modified', $instance->getParam());
    }

    /**
     * @return void
     */
    public function testFactoryWithArrayDependency()
    {
        $factory = new ServiceFactory(
            WithArrayDependency::class,
            [[FirstDependency::class, FirstDependency::class, FirstDependency::class]]
        );
        $serviceArgumentResolver = $this->createStub(ServiceArgumentResolver::class);
        $serviceArgumentResolver->method('resolve')->willReturn([[FirstDependency::class, FirstDependency::class, FirstDependency::class]]);
        $instance = $factory->getInstance($this->dependencyInjection, $serviceArgumentResolver);

        foreach ($instance->getOfFirstDependency() as $firstDependency) {
            $this->assertInstanceOf(FirstDependency::class, $firstDependency);
        }
    }

    public function testFactoryWithExplicitObjectDependency()
    {
        $factory = new ServiceFactory(
            WithObjectDependency::class,
            [(object)['is' => 'ok']]
        );

        $serviceArgumentResolver = $this->createStub(ServiceArgumentResolver::class);
        $serviceArgumentResolver->method('resolve')->willReturn([(object)['is' => 'ok']]);
        $instance = $factory->getInstance($this->dependencyInjection, $serviceArgumentResolver);

        $this->assertInstanceOf(WithObjectDependency::class, $instance);
        $this->assertEquals($instance->getOfFirstDependency()->is, 'ok');
    }

    public function testArgumentResolutionFailure()
    {
        $factory = new ServiceFactory(
            WithObjectDependency::class
        );

        $serviceArgumentResolver = $this->createStub(ServiceArgumentResolver::class);
        $serviceArgumentResolver->method('resolve')->willReturn([true]);
        $this->expectException(DependencyInjectionException::class);
        $factory->getInstance($this->dependencyInjection, $serviceArgumentResolver);
    }

    public function testPreservationOfArrayKeys()
    {
        $factory = new ServiceFactory(WithArrayDependency::class);
        $serviceArgumentResolver = $this->createStub(ServiceArgumentResolver::class);
        $serviceArgumentResolver->method('resolve')->willReturn([['key' => '%param.test%']]);

        /** @var WithArrayDependency $instance */
        $instance = $factory->getInstance($this->dependencyInjection, $serviceArgumentResolver);

        $this->assertEquals(
            ['key'],
            array_keys($instance->getOfFirstDependency()),
            'Service was created with array that did not preserve serviceArgumentResolver keys'
        );
    }
}
