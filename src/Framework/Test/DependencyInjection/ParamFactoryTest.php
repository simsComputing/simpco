<?php

namespace Simpco\Framework\Test\DependencyInjection;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\DependencyInjection\ParamFactory;

/**
 * @covers \Simpco\Framework\DependencyInjection\ParamFactory
 */
class ParamFactoryTest extends TestCase
{
    public function testParamFactory()
    {
        $param = 'test.param';
        $value = 'this is a test';
        $factory = new ParamFactory($param, $value);
        $this->assertEquals($value, $factory->getValue());
        $this->assertEquals($param, $factory->getNameForRegistration());
    }

    public function testParamFactoryFactory()
    {
        $param = 'test.param';
        $value = 'this is a test';
        $factory = ParamFactory::create($param, $value);
        $this->assertEquals($value, $factory->getValue());
        $this->assertEquals($param, $factory->getNameForRegistration());
    }
}