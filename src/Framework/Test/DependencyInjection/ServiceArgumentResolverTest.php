<?php

namespace Simpco\Framework\Test\DependencyInjection;

use PHPUnit\Framework\TestCase;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolver;
use Simpco\Framework\Test\DependencyInjection\Fake\FirstDependency;
use Simpco\Framework\Test\DependencyInjection\Fake\MainService;
use Simpco\Framework\Test\DependencyInjection\Fake\SecondDependency;
use Simpco\Framework\Test\DependencyInjection\Fake\WithArrayDependency;

/**
 * @covers \Simpco\Framework\DependencyInjection\ServiceArgumentResolver
 */
class ServiceArgumentResolverTest extends TestCase
{
    public function testResolver()
    {
        $resolver = new ServiceArgumentResolver();
        $this->assertEquals(
            [FirstDependency::class, SecondDependency::class, '%param.test%'],
            $resolver->resolve(MainService::class, [2 => '%param.test%'], 'id1')
        );
        $this->assertEquals([], $resolver->resolve(FirstDependency::class, [], 'id2'));

        // ASSERT THAT CACHE IS DOING ITS WORK. firstDependency argument will not be reevaluated.
        $this->assertEquals(
            [FirstDependency::class, SecondDependency::class, '%param.test%'],
            $resolver->resolve(MainService::class, [2 => '%param.test%', 'firstDependency' => 'surcharge'], 'id1')
        );
    }

    public function testResolverWithExplicitArgumentForService()
    {
        $resolver = new ServiceArgumentResolver();
        $this->assertEquals(
            ['surcharge', SecondDependency::class, '%param.test%'],
            $resolver->resolve(MainService::class, [2 => '%param.test%', 'firstDependency' => 'surcharge'], 'id1')
        );
    }

    public function testResolverWithBothPositionAndNamedArguments()
    {
        $resolver = new ServiceArgumentResolver();
        $this->assertEquals(
            [FirstDependency::class, SecondDependency::class, '%param.test.modified%'],
            $resolver->resolve(MainService::class, [2 => '%param.test%', 'param' => '%param.test.modified%'], 'id1')
        );
    }

    public function testResolverWithArrayDependency()
    {
        $resolver = new ServiceArgumentResolver();
        $this->assertEquals(
            [[]],
            $resolver->resolve(WithArrayDependency::class, [], 'id1')
        );
    }

    public function testResolverWithArrayDependencyAndExplicitArguments()
    {
        $resolver = new ServiceArgumentResolver();
        $this->assertEquals(
            [[FirstDependency::class, FirstDependency::class, FirstDependency::class]],
            $resolver->resolve(
                WithArrayDependency::class,
                [[FirstDependency::class, FirstDependency::class, FirstDependency::class]],
                'id1'
            )
        );
    }

    public function testResolverPreservesKeysOfArrayParameters()
    {
        $resolver = new ServiceArgumentResolver();
        $parameters = $resolver->resolve(WithArrayDependency::class, [['key' => FirstDependency::class]], 'id1');
        $this->assertEquals(
            ['key'],
            array_keys($parameters[0])
        );
    }

    public function testResolveTwiceSameClassWithDifferentArguments()
    {
        $resolver = new ServiceArgumentResolver();
        $parameters1 = $resolver->resolve(WithArrayDependency::class, [['key' => 'value1']], 'id1');
        $parameters2 = $resolver->resolve(WithArrayDependency::class, [['key' => 'value2']], 'id2');

        $this->assertEquals(
            'value1',
            $parameters1[0]['key']
        );

        $this->assertEquals(
            'value2',
            $parameters2[0]['key']
        );
    }
}
