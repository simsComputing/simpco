<?php

namespace Simpco\Framework;

use Simpco\Framework\DependencyInjection\ParamFactoryInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifierInterface;
use Simpco\Framework\DependencyInjection\ServiceArgumentResolverInterface;
use Simpco\Framework\DependencyInjection\ServiceFactoryInterface;
use Simpco\Framework\DependencyInjection\ServiceFactoryProvider;
use Simpco\Framework\DependencyInjection\ServiceInstanciationStackInterface;
use Simpco\Framework\Exception\MissingParameterException;
use Simpco\Framework\Exception\MissingServiceException;

class DependencyInjection implements DependencyInjectionInterface
{
    /**
     * @var ServiceFactoryInterface[]
     */
    private array $serviceRegistration = [];

    /**
     * @var ParamFactoryInterface[]
     */
    private array $paramRegistration = [];

    /**
     * @var ServiceInstanciationStackInterface
     */
    private ServiceInstanciationStackInterface $serviceInstanciationStack;

    /**
     * @var ServiceArgumentResolverInterface
     */
    private ServiceArgumentResolverInterface $serviceArgumentResolver;

    private ServiceFactoryProvider $serviceFactoryProvider;

    public function __construct(
        ServiceInstanciationStackInterface $serviceInstanciationStack,
        ServiceArgumentResolverInterface $serviceArgumentResolver,
        ServiceFactoryProvider $serviceFactoryProvider
    ) {
        $this->serviceInstanciationStack = $serviceInstanciationStack;
        $this->serviceArgumentResolver = $serviceArgumentResolver;
        $this->serviceFactoryProvider = $serviceFactoryProvider;
    }

    /**
     * @param ServiceFactoryInterface $serviceFactory
     * @return void
     */
    public function registerService(ServiceFactoryInterface $serviceFactory): self
    {
        $this->serviceRegistration[$serviceFactory->getNameForRegistration()] = $serviceFactory;
        return $this;
    }

    /**
     * @param ParamFactoryInterface $paramFactory
     * @return void
     */
    public function registerParam(ParamFactoryInterface $paramFactory): self
    {
        $this->paramRegistration[$paramFactory->getNameForRegistration()] = $paramFactory;
        return $this;
    }

    /**
     * @param string $name
     * @return object
     */
    public function getService(string $name): object
    {
        $this->validateService($name);
        $this->serviceInstanciationStack->checkForCircularDependency($name);
        $service = $this->serviceRegistration[$name]->getInstance($this, $this->serviceArgumentResolver);
        $this->serviceInstanciationStack->unregisterDependencyStack($name);

        return $service;
    }

    /**
     * @param string $paramName
     * @return string|array
     */
    public function getParam(string $paramName)
    {
        if (!isset($this->paramRegistration[$paramName])) {
            throw new MissingParameterException($paramName);
        }

        return $this->paramRegistration[$paramName]->getValue();
    }

    public function getCurrentStackSize(): int
    {
        return count($this->serviceInstanciationStack->getStack());
    }

    public static function create(
        ServiceInstanciationStackInterface $serviceInstanciationStack,
        ServiceArgumentResolverInterface $serviceArgumentResolver,
        ServiceFactoryProvider $serviceFactoryProvider
    ): DependencyInjection {
        return new DependencyInjection(
            $serviceInstanciationStack,
            $serviceArgumentResolver,
            $serviceFactoryProvider
        );
    }

    public function modifyArgument(
        string $service,
        ServiceArgumentModifierInterface $serviceArgumentModifier
    ): void {
        $this->validateService($service);
        $this->serviceRegistration[$service]->modifyArgument($serviceArgumentModifier);
    }

    private function validateService(string $service)
    {
        if (isset($this->serviceRegistration[$service])) {
            return;
        }

        if (class_exists($service)) {
            $this->registerService($this->serviceFactoryProvider->createFactory($service));
            return;
        }

        throw new MissingServiceException($service, $this->serviceInstanciationStack->getStack());
    }
}
