<?php

namespace Simpco\MithrilIntegration\Javascript;

use Simpco\Console\CommandLine\BuilderFactoryInterface as CommandLineBuilderFactory;
use Simpco\Console\CommandLine\Exec;
use Simpco\Framework\FileSystem\Scanner;
use Simpco\Framework\Module\Registry as ModulesRegistry;
use Simpco\MithrilIntegration\Exception\MithrilModuleMissingException;
use Simpco\MithrilIntegration\Module;

class Compiler implements CompilerInterface
{
    private string $entryFile;

    private string $outputFile;

    private string $appRoot;

    private CommandLineBuilderFactory $commandLineBuilderFactory;

    private ModulesRegistry $modulesRegistry;

    private Exec $exec;

    private Scanner $scanner;

    /**
     * @param string $entryFile
     * @param string $outputFile
     * @param CommandLineBuilderFactory $commandLineBuilderFactory
     * @param Exec $exec
     * @param ModulesRegistry $modulesRegistry
     * @param Scanner $scanner
     * @param string $appRoot
     */
    public function __construct(
        string $entryFile,
        string $outputFile,
        CommandLineBuilderFactory $commandLineBuilderFactory,
        Exec $exec,
        ModulesRegistry $modulesRegistry,
        Scanner $scanner,
        string $appRoot
    ) {
        $this->entryFile = $entryFile;
        $this->outputFile = $outputFile;
        $this->appRoot = $appRoot;
        $this->commandLineBuilderFactory = $commandLineBuilderFactory;
        $this->modulesRegistry = $modulesRegistry;
        $this->exec = $exec;
        $this->scanner = $scanner;
    }

    public function compile(string $mode = 'development', $watch = false): void
    {
        $commandLineBuilder = $this->commandLineBuilderFactory->create();

        $commandLineBuilder
            ->setCommandName('cd')
            ->addArgument($this->appRoot)
            ->next()
            ->setCommandName('./node_modules/.bin/webpack');

        $mithrilModuleViewDir = null;
        foreach ($this->modulesRegistry->getList() as $module) {
            if ($module instanceof Module) {
                $mithrilModuleViewDir = $module->getViewDir();
                continue;
            }
            $viewDir = $module->getViewDir();
            $entryFile = $viewDir . '/templates/js/' . $this->entryFile;
            if (!$this->scanner->fileExists($entryFile)) {
                continue;
            }
            $commandLineBuilder->addOption('entry', $entryFile);
        }

        if ($mithrilModuleViewDir === null) {
            throw new MithrilModuleMissingException('You must add MithrilIntegration Module to your app');
        }
        $initialisationFile = $mithrilModuleViewDir . '/templates/js/mithrilInitialisation.js';
        $commandLineBuilder->addOption('entry', $initialisationFile);
        $webpackConfig = $mithrilModuleViewDir . '/templates/js/webpack.config.js';
        $commandLineBuilder
            ->addOption('c', $webpackConfig, false)
            ->addOption('output-path', $mithrilModuleViewDir . '/templates/js/toinclude')
            ->addOption('output-filename', $this->outputFile);

        if ($watch) {
            $commandLineBuilder->addOption('watch');
        }

        $commandLineBuilder->addOption('mode', $mode);

        $this->exec->exec($commandLineBuilder->build());
    }
}