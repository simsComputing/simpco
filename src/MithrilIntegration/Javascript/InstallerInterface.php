<?php

namespace Simpco\MithrilIntegration\Javascript;

interface InstallerInterface
{
    public function install(): void;
}
