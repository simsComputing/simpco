<?php

namespace Simpco\MithrilIntegration\Javascript;

interface CompilerInterface
{
    public function compile(string $mode = 'developer', $watch = false): void;
}