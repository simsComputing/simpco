<?php

namespace Simpco\MithrilIntegration\Javascript;

use Simpco\Console\CommandLine\BuilderFactoryInterface as CommandLineBuilderFactoryInterface;
use Simpco\Console\CommandLine\Exec;
use Simpco\Framework\Module\Registry as ModulesRegistry;

class Installer implements InstallerInterface
{
    private ModulesRegistry $modulesRegistry;

    private CommandLineBuilderFactoryInterface $commandLineBuilderFactory;

    private Exec $exec;

    private string $appRoot;

    public function __construct(
        ModulesRegistry $modulesRegistry,
        CommandLineBuilderFactoryInterface $commandLineBuilderFactory,
        Exec $exec,
        string $appRoot
    ) {
        $this->modulesRegistry = $modulesRegistry;
        $this->commandLineBuilderFactory = $commandLineBuilderFactory;
        $this->appRoot = $appRoot;
        $this->exec = $exec;
    }

    public function install(): void
    {
            foreach ($this->modulesRegistry->getList() as $module) {
                $viewDir = $module->getViewDir();
                $packagejson = $viewDir . '/templates/js';
                if (!file_exists($packagejson . '/package.json')) {
                    continue;
                }
                
                $command = $this->commandLineBuilderFactory
                    ->create()
                    ->setCommandName('cd')
                    ->addArgument($this->appRoot)
                    ->next()
                    ->setCommandName('npm')
                    ->addArgument('install')
                    ->addOption('save')
                    ->addArgument($packagejson)
                    ->build();

                $this->exec->exec($command);
            }
    }
}
