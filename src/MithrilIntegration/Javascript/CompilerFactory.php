<?php

namespace Simpco\MithrilIntegration\Javascript;

use Simpco\Console\CommandLine\BuilderFactoryInterface as CommandLineBuilderFactory;
use Simpco\Console\CommandLine\Exec;
use Simpco\Framework\FileSystem\Scanner;
use Simpco\Framework\Module\Registry as ModulesRegistry;

class CompilerFactory implements CompilerFactoryInterface
{
    private string $appRoot;

    private CommandLineBuilderFactory $commandLineBuilderFactory;

    private ModulesRegistry $modulesRegistry;

    private Exec $exec;

    private Scanner $scanner;

    /**
     * @param string $entryFile
     * @param string $outputFile
     * @param CommandLineBuilderFactory $commandLineBuilderFactory
     * @param Exec $exec
     * @param ModulesRegistry $modulesRegistry
     * @param Scanner $scanner
     * @param string $appRoot
     */
    public function __construct(
        CommandLineBuilderFactory $commandLineBuilderFactory,
        Exec $exec,
        ModulesRegistry $modulesRegistry,
        Scanner $scanner,
        string $appRoot
    ) {
        $this->appRoot = $appRoot;
        $this->commandLineBuilderFactory = $commandLineBuilderFactory;
        $this->modulesRegistry = $modulesRegistry;
        $this->exec = $exec;
        $this->scanner = $scanner;
    }

    public function create(string $entryFile, string $outputFile): CompilerInterface
    {
        return new Compiler(
            $entryFile,
            $outputFile,
            $this->commandLineBuilderFactory,
            $this->exec,
            $this->modulesRegistry,
            $this->scanner,
            $this->appRoot
        );
    }
}