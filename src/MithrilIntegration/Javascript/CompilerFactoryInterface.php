<?php

namespace Simpco\MithrilIntegration\Javascript;

interface CompilerFactoryInterface
{
    /**
     * @param string $entryFile
     * @param string $outputFile
     * @return CompilerInterface
     */
    public function create(string $entryFile, string $outputFile): CompilerInterface;
}
