const display = (b) => b();

const defaultValue = (attribute, defaultValue) =>
  typeof attribute === 'undefined' ? defaultValue : attribute;

const getSimpcoObj = () => {
  return new Promise((resolve) => {
    var intervalId = setInterval(() => {
      if (typeof window.simpco !== 'undefined') {
        resolve(window.simpco);
        clearInterval(intervalId);
      }
    }, 100);
  });
};

const fpLog = (value) => {
  console.dir(value);
  return value;
};

export { display, defaultValue, getSimpcoObj, fpLog };
