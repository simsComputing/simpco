import { request, m, route, redraw } from 'mithril';

const getCurrentUrlForRequest = () => window.location.href.split('#!')[1] ?? '';

const routeWrapper = (getComponentCallback, getLoadingComponent) => {
  const routeInit = {
    data: null,
    fetch: () => {
      if (window.simpcoInitData !== true) {
        routeInit.data = window.simpcoInitData;
        window.simpcoInitData = true;
      } else {
        request({
          method: 'GET',
          url: getCurrentUrlForRequest(),
          headers: {
            'Content-Type': 'application/json',
          },
        }).then((data) => (routeInit.data = data));
      }
    },
  };
  const finalComponent = () => ({
    oninit: () => {
      window.simpco = {
        fetch: routeInit.fetch.bind(routeInit),
        post: (body = {}) =>
          request({
            method: 'POST',
            url: getCurrentUrlForRequest(),
            headers: {
              'Content-Type': 'application/json',
            },
            body,
          }).then((data) => {
            routeInit.data = data;
            return data;
          }),
      };
      routeInit.fetch();
    },
    view: function () {
      return routeInit.data === null
        ? m(getLoadingComponent(), {})
        : m(getComponentCallback(), routeInit.data);
    },
  });

  return finalComponent;
};

export default routeWrapper;
