var m = require('mithril');
import { tidy } from "./mithrilJest";
import { simpcoRegisterBlock } from "simpco-mithril/base";

const mithrilComponentDependencyDeclaration = (name) => {
    const component = {
        view: (vnode) => m('div', {}, [
            m('.dependency', 'Dependency : ' + name),
            vnode.attrs ? m('ul', [
                Object.entries(vnode.attrs).map((pair) => m('li', pair[0] + ' : ' + pair[1]))
            ]) : null
        ])
    };
    simpcoRegisterBlock(name, component);
}

const makeComponentTest = (name, component, attrs = {}) => describe(name, () => {
    it("Should display properly", () => {
      const cmp = m(component, attrs);
      const html = tidy(cmp);
      expect(html).toMatchSnapshot();
    });
  });


const makeMMock = (callback) => (component, props) => callback(component, props);

const makeRouteMock = () => ({
    set: jest.fn(() => {})
});

export {
    mithrilComponentDependencyDeclaration,
    makeComponentTest,
    makeMMock,
    makeRouteMock
};