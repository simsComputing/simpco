import routeWrapper from './routerWrapper';
import { m } from 'mithril';
import { route } from 'mithril';

const simpcoBlocksRegistry = {};
const simpcoBlocksHooksRegistry = {};
const simpcoRoutesRegistry = {};

var defaultRoute = '';

const simpcoRegisterBlock = (name, block) => {
  simpcoBlocksRegistry[name] = block;
};

const simpcoDisplayBlock = (name, props = {}, children = []) =>
  m(simpcoGetBlock(name), props, children);

const emptyBlock = (name) => ({
  view: () =>
    m('div', { class: 'simpco-empty-block' }, 'Could not find block ' + name),
});
const simpcoGetBlock = (name) => simpcoBlocksRegistry[name] ?? emptyBlock(name);

const modifyBlockAttrs = (block, attrsToAdd) => {
  block.attrs = {
    ...block.attrs,
    ...attrsToAdd,
  };
  return block;
};

const simpcoGetArrayOfBlocks = (name, attrs = {}) =>
  simpcoBlocksRegistry[name]?.map((data) =>
    modifyBlockAttrs(data.block, attrs)
  ) ?? [];

const simpcoPushArrayOfBlocks = (
  arrayName,
  blockName,
  block,
  sortOrder = 100
) => {
  if (typeof simpcoBlocksRegistry[arrayName] === 'undefined') {
    simpcoBlocksRegistry[arrayName] = [];
  }
  simpcoBlocksRegistry[arrayName] = simpcoBlocksRegistry[arrayName].filter(
    (blockData) => blockData.blockName !== blockName
  );
  simpcoBlocksRegistry[arrayName].push({ block, sortOrder, blockName });
  simpcoBlocksRegistry[arrayName] = simpcoBlocksRegistry[arrayName].sort(
    (a, b) => a.sortOrder - b.sortOrder
  );
};

const simpcoRegisterHookForBlock = (name, hook, block, sortOrder = 0) => {
  if (typeof window.simpcoBlocksHooksRegistry[name] === 'undefined') {
    simpcoBlocksHooksRegistry[name] = {};
  }
  if (typeof window.simpcoBlocksHooksRegistry[name][hook] === 'undefined') {
    simpcoBlocksHooksRegistry[name][hook] = [];
  }
  simpcoBlocksHooksRegistry[name][hook].push({ block, sortOrder });

  simpcoBlocksHooksRegistry[name][hook].sort(
    (hook1, hook2) => -hook1.sortOrder + hook2.sortOrder
  );
};

const simpcoRegisterRoute = (url, blockName) => {
  const wrappedComponent = routeWrapper(
    () => simpcoGetBlock(blockName),
    () => simpcoGetBlock('mithril.loading')
  );
  simpcoRoutesRegistry[url] = wrappedComponent;
};

const simpcoRegisterDefaultRoute = (newDefaultRoute) => {
  defaultRoute = newDefaultRoute;
};

const simpcoGetRoutes = () => {
  return simpcoRoutesRegistry;
};

const simpcoInit = () => {
  const routes = simpcoGetRoutes();
  const routesWithBLocks = {};
  Object.entries(routes).forEach((entry) => {
    const [url, block] = entry;
    routesWithBLocks[url] = block;
  });
  route.prefix = '';
  document.addEventListener('SimpcoInitLoaded', function () {
    route(document.body, defaultRoute, routesWithBLocks);
  });
};

export {
  simpcoRegisterBlock,
  simpcoGetBlock,
  simpcoDisplayBlock,
  simpcoRegisterHookForBlock,
  simpcoGetRoutes,
  simpcoRegisterRoute,
  simpcoGetArrayOfBlocks,
  simpcoPushArrayOfBlocks,
  simpcoRegisterDefaultRoute,
  simpcoInit,
};
