<?php

namespace Simpco\MithrilIntegration\Console;

use Simpco\Console\AbstractCommand;
use Simpco\MithrilIntegration\Javascript\InstallerInterface;

class InstallJavascriptCommand extends AbstractCommand
{
    private InstallerInterface $installerInterface;

    /**
     * @param InstallerInterface $installerInterface
     */
    public function __construct(
        InstallerInterface $installerInterface
    ) {
        $this->installerInterface = $installerInterface;
    }

    protected function configure(): void
    {
        $this->setName('install:javascript');
    }

    public function execute(\Simpco\Console\Input\InputInterface $input, \Simpco\Console\Output\OutputInterface $output): void
    {
        $this->installerInterface->install();
    }
}
