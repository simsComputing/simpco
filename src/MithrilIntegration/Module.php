<?php

namespace Simpco\MithrilIntegration;

use Simpco\Console\CommandPoolInterface;
use Simpco\Console\Module as ConsoleModule;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifier\ArrayPush;
use Simpco\Framework\DependencyInjection\ServiceFactory;
use Simpco\Framework\FrameworkModule;
use Simpco\Framework\ModuleInterface;
use Simpco\Framework\View\HeadTag\ScriptTag;
use Simpco\Framework\View\HeadTagPoolInterface;
use Simpco\MithrilIntegration\Console\CompileJavascriptCommand;
use Simpco\MithrilIntegration\Console\InstallJavascriptCommand;
use Simpco\MithrilIntegration\Javascript\Compiler;
use Simpco\MithrilIntegration\Javascript\CompilerFactory;
use Simpco\MithrilIntegration\Javascript\CompilerFactoryInterface;
use Simpco\MithrilIntegration\Javascript\Installer;
use Simpco\MithrilIntegration\Javascript\InstallerInterface;

class Module implements ModuleInterface
{
    public function getServices(): array
    {
        return ServiceFactory::bashFactory(
            [
                [InstallJavascriptCommand::class, ['viewDirs' => '%viewPool.dirs%', 'appRoot' => '%app.root%']],
                [Installer::class, ['appRoot' => '%app.root%'], InstallerInterface::class],
                [CompilerFactory::class, ['appRoot' => '%app.root%'], CompilerFactoryInterface::class]
            ]
        );
    }

    public function getParams(): array
    {
        return [];
    }

    public function getRoutes(): array
    {
        return [];
    }

    public function getModelsDir(): ?string
    {
        return null;
    }

    public function getViewDir(): ?string
    {
        return __DIR__ . '/view';
    }

    public function getServiceModifiers(): array
    {
        return [
            CommandPoolInterface::class => [
                ArrayPush::create('commands', [
                    InstallJavascriptCommand::class  
                ])
            ]
        ];
    }

    public function getDependencies(): array
    {
        return [
            FrameworkModule::class,
            ConsoleModule::class
        ];
    }
}
