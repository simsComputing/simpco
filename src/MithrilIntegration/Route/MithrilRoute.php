<?php

namespace Simpco\MithrilIntegration\Route;

use Simpco\Framework\Http\Header;
use Simpco\Framework\Http\HeaderInterface;
use Simpco\Framework\RequestInterface;
use Simpco\Framework\ResponseInterface;
use Simpco\Framework\Router\RouteInterface;
use Simpco\Framework\ViewFactory;
use Simpco\Framework\ViewInterface;
use Simpco\MithrilIntegration\Exception\BadMithrilRequestException;

abstract class MithrilRoute implements RouteInterface
{
    /**
     * @var RequestInterface
     */
    protected RequestInterface $request;

    /**
     * @var ViewFactory
     */
    protected ViewFactory $viewFactory;

    /**
     * @var ResponseInterface
     */
    protected ResponseInterface $response;

    /**
     * @param RequestInterface $request
     * @param ViewFactory $viewFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        RequestInterface $request,
        ViewFactory $viewFactory,
        ResponseInterface $response
    ) {
        $this->request = $request;
        $this->viewFactory = $viewFactory;
        $this->response = $response;
    }

    final public function execute(): ViewInterface
    {
        switch ($this->request->getHeader(HeaderInterface::CONTENT_TYPE)) {
            case HeaderInterface::CONTENT_TYPE_HTML:
            case null:
                return $this->htmlResposne();
            case HeaderInterface::CONTENT_TYPE_JSON:
                return $this->jsonResponse();
            default:
                throw new BadMithrilRequestException('Could not determine how to respond to this request with mithril integration');
        }
    }

    private function jsonResponse(): ViewInterface
    {
        $this->response->addHeader(Header::create(HeaderInterface::CONTENT_TYPE, HeaderInterface::CONTENT_TYPE_JSON));
        return $this->viewFactory->create(
            'empty.phtml',
            'json.phtml',
            ['json' => $this->getData()],
            [],
            'empty.phtml'
        );
    }

    private function htmlResposne(): ViewInterface
    {
        return $this->viewFactory->create(
            'mithril_layout.phtml',
            'mithril_template.phtml',
            ['data' => json_encode($this->getData())],
            [],
            'mithril_main.phtml'
        );
    }

    /**
     * @return array
     */
    abstract protected function getData(): array;
}
