<?php

namespace Simpco\MithrilIntegration\Test\Javascript;
use PHPUnit\Framework\TestCase;
use Simpco\Console\CommandLine\Builder as CommandLineBuilder;
use Simpco\Console\CommandLine\BuilderFactory as CommandLineBuilderFactory;
use Simpco\Console\CommandLine\Exec;
use Simpco\Framework\FileSystem\Scanner;
use Simpco\Framework\Module\Registry as ModulesRegistry;
use Simpco\Framework\ModuleInterface;
use Simpco\MithrilIntegration\Javascript\Compiler;
use Simpco\MithrilIntegration\Module;

/**
 * @covers \Simpco\MithrilIntegration\Javascript\Compiler
 */
class CompilerTest extends TestCase
{
    private CommandLineBuilderFactory $commandLineBuilderfactory;

    private CommandLineBuilder $commandLineBuilder;

    private Exec $exec;

    private ModulesRegistry $modulesRegistry;

    private Scanner $scanner;

    private string $appRoot = '/home/app/root';

    public function setUp(): void
    {
        $this->commandLineBuilderfactory = $this->createStub(CommandLineBuilderFactory::class);
        $this->exec = $this->createMock(Exec::class);
        $this->modulesRegistry = $this->createStub(ModulesRegistry::class);
        $this->commandLineBuilder = $this->createMock(CommandLineBuilder::class);
        $this->commandLineBuilder->method('addOption')->willReturn($this->commandLineBuilder);
        $this->commandLineBuilderfactory->method('create')->willReturn($this->commandLineBuilder);
        $this->scanner = $this->createStub(Scanner::class);
    }

    public static function getDataSet(): array
    {
        return [
            [
                'index.js', 
                'output.js', 
                [
                    ModuleInterface::class => '/home/root/module/view',
                    Module::class => '/home/root/mithril/view'
                ],
                'developer',
                true,
                'cd /home/app/root ; ./node_modules/.bin/webpack --entry /home/root/module/view/templates/js/output.js -c /home/root/mithril/view/templates/js/webpack.config.js --output-path /home/root/mithril/view/templates/js/output --output-filename output;js --watch --mode developer'
            ]
        ];
    }

    /**
     * @dataProvider getDataSet
     * @param [type] $entryFile
     * @param [type] $outputFile
     * @param [type] $modules
     * @param [type] $expectedMode
     * @param [type] $watch
     * @param [type] $expectedCli
     * @return void
     */
    public function testCompilation(
        $entryFile, 
        $outputFile, 
        $modules, 
        $expectedMode, 
        $watch, 
        $expectedCli
    ): void {
        $modulesMocks = [];
        $scannerMethodMap = [];
        foreach ($modules as $class => $path) {
            $modulesMocks[] = $this->createModuleMock($class, $path);
            $scannerMethodMap[] = [$path . '/templates/js/' . $entryFile, true];
        }

        $modulesMocks[] = $this->createModuleMock(ModuleInterface::class, '/nothing/here');
        $scannerMethodMap[] = ['/nothing/here/templates/js/' . $entryFile, false];

        $this->scanner->method('fileExists')->willReturnMap($scannerMethodMap);
        $this->modulesRegistry->method('getList')->willReturn($modulesMocks);
        $this->commandLineBuilder->expects($this->once())->method('build')->willReturn($expectedCli);

        $nbOfOptions = count($modules);
        $nbOfOptions += $watch === true ? 1 : 0;
        $nbOfOptions += 4;

        $this->commandLineBuilder->expects($this->exactly($nbOfOptions))->method('addOption');

        $this->exec->expects($this->once())->method('exec')->with($expectedCli);

        $compiler = $this->createCompiler($entryFile, $outputFile);

        $compiler->compile($expectedMode, $watch);
    }

    private function createCompiler(string $entryFile, string $outputFile): Compiler
    {
        return new Compiler(
            $entryFile,
            $outputFile,
            $this->commandLineBuilderfactory,
            $this->exec,
            $this->modulesRegistry,
            $this->scanner,
            $this->appRoot
        );
    }

    private function createModuleMock(string $class, string $modulePath)
    {
        $mock = $this->createMock($class);
        $mock->expects($this->once())->method('getViewDir')->willReturn($modulePath);
        return $mock;
    }
}