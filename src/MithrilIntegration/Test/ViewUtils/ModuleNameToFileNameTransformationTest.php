<?php

namespace Simpco\MithrilIntegration\Test\ViewUtils;

use PHPUnit\Framework\TestCase;
use Simpco\MithrilIntegration\Module;
use Simpco\MithrilIntegration\ViewUtils\ModuleNameToFileNameTransformation;

/**
 * @covers \Simpco\MithrilIntegration\ViewUtils\ModuleNameToFileNameTransformation
 * @uses \Simpco\MithrilIntegration\Test\ViewUtils\ModuleNameToFileNameTransformationTest
 */
class ModuleNameToFileNameTransformationTest extends TestCase
{
    /**
     * @dataProvider getDataSet
     * @return void
     */
    public function testSimple($startString, $startFrom, $expectedFile, $expectedModule)
    {
        $transfo = new ModuleNameToFileNameTransformation($startString, $startFrom);

        $this->assertEquals($expectedFile, $transfo->getFile());
        $this->assertEquals($expectedModule, $transfo->getModule());
    }

    public static function getDataSet(): array
    {
        return [
            [Module::class, ModuleNameToFileNameTransformation::START_FROM_MODULE, 'Simpco_MithrilIntegration_Module', Module::class],
            ['Simpco_MithrilIntegration_Module', ModuleNameToFileNameTransformation::START_FROM_FILE, 'Simpco_MithrilIntegration_Module', Module::class],
        ];
    }
}