<?php

namespace Simpco\MithrilIntegration\ViewUtils;

use InvalidArgumentException;

class ModuleNameToFileNameTransformation
{
    const START_FROM_MODULE = 'module';

    const START_FROM_FILE = 'file';

    private string $module;
    private string $file;

    public function __construct(
        string $startString,
        string $startFrom
    ) {
        switch($startFrom) {
            case self::START_FROM_FILE:
                $this->file = $startString;
                $this->transformFileIntoModule();
                break;
            case self::START_FROM_MODULE:
                $this->module = $startString;
                $this->transformModuleIntoFile();
                break;
            default:
                throw new InvalidArgumentException('startFrom argument is invalid.');
        }
    }

    private function transformFileIntoModule(): void
    {
        $this->module = str_replace('_', '\\', $this->file);
    }

    private function transformModuleIntoFile(): void
    {
        $this->file = str_replace('\\', '_', $this->module);
    }

    public function getModule(): string
    {
        return $this->module;
    }

    public function getFile(): string
    {
        return $this->file;
    }
}