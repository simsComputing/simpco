<?php

namespace Simpco\MithrilIntegration\ViewUtils;

use Simpco\Framework\ViewFactory;
use Simpco\Framework\ViewInterface;

class MithrilViewFactory
{
    private ViewFactory $viewFactory;

    public function __construct(ViewFactory $viewFactory)
    {
        $this->viewFactory = $viewFactory;
    }

    public function create(string $template, string $layout, array $headTags): ViewInterface
    {
        return $this->viewFactory->create($layout, $template, $headTags, 'mithril_main.phtml');
    }
}