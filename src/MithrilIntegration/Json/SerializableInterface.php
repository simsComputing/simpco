<?php

namespace Simpco\MithrilIntegration\Json;

interface SerializableInterface
{
    public function json(): string;

    public function toArray(): array;
}
