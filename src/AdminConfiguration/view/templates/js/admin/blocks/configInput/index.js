import { simpcoRegisterBlock } from 'simpco-mithril/base';
import ConfigInputComponent from './configInput';

simpcoRegisterBlock('admin.configuration.input', ConfigInputComponent);
