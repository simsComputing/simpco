import { simpcoDisplayBlock } from 'simpco-mithril/base';

const ConfigInputComponent = () => {
  return {
    view: (vnode) => {
      vnode.attrs.inputProps.value =
        vnode.attrs.formValues[vnode.attrs.inputProps.name];
      return simpcoDisplayBlock('admin.molecule.fieldset', { ...vnode.attrs });
    },
  };
};

export default ConfigInputComponent;
