import { simpcoRegisterBlock } from 'simpco-mithril/base';
import ConfigAccordionComponent from './configAccordion';

simpcoRegisterBlock('admin.configuration.accordion', ConfigAccordionComponent);
