import {
  simpcoDisplayBlock,
  simpcoGetArrayOfBlocks,
} from 'simpco-mithril/base';

const ConfigAccordionComponent = () => {
  return {
    view: ({ attrs: { title, code, formValues } }) =>
      simpcoDisplayBlock('admin.organism.accordion', {
        title,
        content: simpcoGetArrayOfBlocks('admin.configuration.tab.' + code, {
          formValues,
        }),
      }),
  };
};

export default ConfigAccordionComponent;
