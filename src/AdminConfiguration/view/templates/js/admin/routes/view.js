import { m } from 'mithril';
import {
  simpcoDisplayBlock,
  simpcoGetArrayOfBlocks,
  simpcoRegisterBlock,
  simpcoRegisterRoute,
} from 'simpco-mithril/base';
import './view.scss';
import {
  addConfigurationInput,
  initConfig,
  makeConfigurationTab,
} from '../manager';

makeConfigurationTab('general', 'General', 10);
addConfigurationInput('general', 'website/name', 'text', 'Website Name');

const View = () => {
  return {
    view: (vnode) => {
      initConfig();
      return simpcoDisplayBlock('admin.organism.layout', {
        content: [
          m('div', { class: 'pure-g' }, [
            vnode.attrs.simpco_form_result_message
              ? simpcoDisplayBlock('admin.molecule.flashMessage', {
                  type: 'success',
                  text: vnode.attrs.simpco_form_result_message,
                })
              : null,
            m(
              'div',
              { class: 'pure-u-5-5 configuration-section' },
              simpcoDisplayBlock('admin.atom.form', {}, [
                ...simpcoGetArrayOfBlocks('admin.configuration.tabs', {
                  formValues: vnode.attrs,
                }),
                m('button', { type: 'submit' }, 'Save'),
              ])
            ),
          ]),
        ],
      });
    },
  };
};

simpcoRegisterBlock('admin.configuration.view', View);
simpcoRegisterRoute('/admin/configuration', 'admin.configuration.view');
