import {
  simpcoDisplayBlock,
  simpcoGetArrayOfBlocks,
  simpcoPushArrayOfBlocks,
} from 'simpco-mithril/base';

const tabFuncStack = [];
const inputsFuncStack = [];

const makeConfigurationTab = (code, title, sortOrder = 10) => {
  const actuallyMakeConfigurationTab = () => {
    const block = simpcoDisplayBlock('admin.configuration.accordion', {
      title,
      code,
    });
    simpcoPushArrayOfBlocks('admin.configuration.tabs', code, block, sortOrder);
  };
  tabFuncStack.push(actuallyMakeConfigurationTab);
};

const addConfigurationInput = (
  tabCode,
  inputCode,
  input,
  label,
  sortOrder = 10
) => {
  const actuallyAddConfigurationInput = () => {
    const block = simpcoDisplayBlock('admin.configuration.input', {
      label,
      input,
      inputProps: { name: inputCode },
    });
    simpcoPushArrayOfBlocks(
      'admin.configuration.tab.' + tabCode,
      inputCode,
      block,
      sortOrder
    );
  };

  inputsFuncStack.push(actuallyAddConfigurationInput);
};

const execFunc = (func) => func();

const initConfig = () => {
  tabFuncStack.forEach(execFunc);
  inputsFuncStack.forEach(execFunc);
};

export { makeConfigurationTab, addConfigurationInput, initConfig };
