import { registerMenuConfig } from "simpco-admin/menuConfig";
import { makeAdminConfigurationRoute } from "./utils";

registerMenuConfig('admin.configuration',  {text: 'Configuration', href: '#'}, 1000, null);
registerMenuConfig('admin.configuration.general', {text: 'Général', href:  makeAdminConfigurationRoute('/')}, 10, 'admin.configuration');
