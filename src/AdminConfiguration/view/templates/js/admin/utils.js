const makeAdminConfigurationRoute = (path) => '/admin/configuration' + path;

export {
    makeAdminConfigurationRoute
}