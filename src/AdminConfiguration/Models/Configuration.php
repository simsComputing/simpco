<?php

namespace Simpco\AdminConfiguration\Models;

use Doctrine\ORM\Mapping as ORM;
use Simpco\AdminConfiguration\Models\Repository\ConfigRepository;

#[ORM\Entity(repositoryClass: ConfigRepository::class), ORM\Table(name: 'simpco_configuration')]
class Configuration
{
    #[ORM\Id, ORM\Column(type: "string", length: 250)]
    private string $code;

    #[ORM\Column(type: "string", length: 250)]
    private string $value;

    /**
     * Get the value of code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @return  self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get the value of value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set the value of value
     *
     * @return  self
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
