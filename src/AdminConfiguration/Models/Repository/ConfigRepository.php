<?php

namespace Simpco\AdminConfiguration\Models\Repository;

use Doctrine\ORM\EntityRepository;
use Simpco\AdminConfiguration\Models\Configuration;

class ConfigRepository extends EntityRepository
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getClassMetadata(Configuration::class));
    }
}
