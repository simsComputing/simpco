<?php

namespace Simpco\AdminConfiguration;

use Simpco\AdminConfiguration\Forms\ConfigFormProvider;
use Simpco\AdminConfiguration\Forms\Processors\WebsiteNameProcessor;
use Simpco\AdminConfiguration\Forms\Validators\WebsiteNameValidator;
use Simpco\AdminConfiguration\Routes\View;
use Simpco\Form\Form;
use Simpco\Form\Module as FormModule;
use Simpco\Framework\DependencyInjection\ServiceFactory;
use Simpco\Framework\Module\EmptyModuleTrait;
use Simpco\Framework\ModuleInterface;
use Simpco\Framework\RequestInterface;
use Simpco\Utils\Routes\ModifyDefinition;

class Module implements ModuleInterface
{
    use EmptyModuleTrait;

    public function getServices(): array
    {
        return ServiceFactory::bashFactory([
            [Form::class, [['main' => ConfigFormProvider::class], [WebsiteNameValidator::class], [WebsiteNameProcessor::class]], 'admin.configuration.form'],
            [View::class, ['form' => 'admin.configuration.form'], View::class]
        ]);
    }

    public function getModelsDir(): ?string
    {
        return __DIR__ . '/Models';
    }

    public function getRoutes(): array
    {
        return [
            RequestInterface::GET =>
            ModifyDefinition::prefix('/admin/configuration', [
                '/' => View::class
            ]),
            RequestInterface::POST => ModifyDefinition::prefix('/admin/configuration', [
                '/' => View::class
            ])
        ];
    }

    public function getViewDir(): ?string
    {
        return __DIR__ . '/view';
    }

    public function getDependencies(): array
    {
        return [
            FormModule::class
        ];
    }
}
