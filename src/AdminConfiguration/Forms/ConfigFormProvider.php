<?php

namespace Simpco\AdminConfiguration\Forms;

use Simpco\AdminConfiguration\Models\Configuration;
use Simpco\AdminConfiguration\Models\Repository\ConfigRepository;
use Simpco\Form\Api\ProviderInterface;

class ConfigFormProvider implements ProviderInterface
{
    private ConfigRepository $configRepository;

    public function __construct(
        ConfigRepository $configRepository
    ) {
        $this->configRepository = $configRepository;
    }

    public function getData(): mixed
    {
        /** @var Configuration $config */
        $config = $this->configRepository->findOneByCode('website/name');

        return ['website/name' => $config === null ? $config : $config->getValue()];
    }

    /**
     * @return mixed
     */
    public function getModel(): mixed
    {
        /** @var Configuration $config */
        $config = $this->configRepository->findOneByCode('website/name');

        if ($config === null) {
            return new Configuration();
        }

        return $config;
    }
}
