<?php

namespace Simpco\AdminConfiguration\Forms\Processors;

use Doctrine\ORM\EntityManagerInterface;
use Simpco\Form\Api\ProcessorInterface;
use Simpco\Utils\Data\DataObjectInterface;

class WebsiteNameProcessor implements ProcessorInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function process(array $data, DataObjectInterface $providersRegistry): mixed
    {
        $config = $providersRegistry->getData('main')->getModel();
        $config->setValue($data['website/name']);
        $config->setCode('website/name');

        $this->entityManager->persist($config);
        $this->entityManager->flush();

        return true;
    }
}
