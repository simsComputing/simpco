<?php

namespace Simpco\AdminConfiguration\Routes;

use Simpco\AdminConfiguration\Input\ConfigInput;
use Simpco\Form\FormInterface;
use Simpco\Framework\RequestInterface;
use Simpco\MithrilIntegration\Route\MithrilRoute;

class View extends MithrilRoute
{
    private FormInterface $form;

    public function __construct(
        FormInterface $form,
        \Simpco\Framework\RequestInterface $request,
        \Simpco\Framework\ViewFactory $viewFactory,
        \Simpco\Framework\ResponseInterface $response
    ) {
        $this->form = $form;
        parent::__construct($request, $viewFactory, $response);
    }

    public function getData(): array
    {
        $success = false;
        if ($this->request->getMethod() === RequestInterface::POST && $this->form->validate($this->request)) {
            $this->form->post($this->request);
            $success = true;
        }
        $result = $this->form->getData($this->request);

        if ($success === true) {
            $result['simpco_form_result_message'] = 'Configuration sauvegardée !';
        }

        return $result;
    }
}
