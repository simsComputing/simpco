<?php

namespace Simpco\Console;

interface CommandPoolInterface
{
    /**
     * @param string $name
     * @return CommandInterface|null
     */
    public function getCommand(string $name): ?CommandInterface;

    public function getCommands(): array;
}