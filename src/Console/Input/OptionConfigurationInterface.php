<?php

namespace Simpco\Console\Input;

interface OptionConfigurationInterface extends InputConfigurationInterface
{
    const TYPE_ARRAY = 0;
    const TYPE_STRING = 1;

    public function getType(): int;
}