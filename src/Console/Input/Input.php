<?php

namespace Simpco\Console\Input;

class Input implements InputInterface
{
    private array $argumentsMapping;
    private array $optionsMapping;

    public function __construct(
        array $argumentsMapping,
        array $optionsMapping
    ) {
        $this->argumentsMapping = $argumentsMapping;
        $this->optionsMapping = $optionsMapping;
    }

    public function getArgument(string $argName)
    {
        return isset($this->argumentsMapping[$argName]) ? $this->argumentsMapping[$argName] : null;
    }

    public function getOption(string $optionName)
    {
        return isset($this->optionsMapping[$optionName]) ? $this->optionsMapping[$optionName] : null;
    }
}