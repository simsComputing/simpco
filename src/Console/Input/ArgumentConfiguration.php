<?php

namespace Simpco\Console\Input;

class ArgumentConfiguration implements ArgumentConfigurationInterface
{
    private int $sortOrder;

    private string $name;

    private int $required;

    public function __construct(
        string $name,
        int $sortOrder,
        int $required
    ) {
        $this->name = $name;
        $this->sortOrder = $sortOrder;
        $this->required = $required;
    }

    public function getSortOrder(): int
    {
        return $this->sortOrder;
    }


    public function getName(): string
    {
        return $this->name;
    }

    public function getRequired(): int
    {
        return $this->required;
    }
}