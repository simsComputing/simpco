<?php

namespace Simpco\Console\Input;

use Simpco\Console\CommandConfigurationInterface;

interface InputFactoryInterface
{
    public function create(
        CommandConfigurationInterface $commandConfiguration,
        array $argsList
    ): InputInterface;
}
