<?php

namespace Simpco\Console\Input;

class OptionConfiguration implements OptionConfigurationInterface
{
    private int $type;

    private string $name;

    private int $required;

    public function __construct(
        string $name,
        int $type,
        int $required
    ) {
        $this->name = $name;
        $this->type = $type;
        $this->required = $required;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getRequired(): int
    {
        return $this->required;
    }
}