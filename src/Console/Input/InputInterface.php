<?php

namespace Simpco\Console\Input;

interface InputInterface
{
    public function getArgument(string $argName);

    public function getOption(string $optionName);
}