<?php

namespace Simpco\Console\Input;

interface ArgumentConfigurationInterface extends InputConfigurationInterface
{
    public function getSortOrder(): int;
}