<?php

namespace Simpco\Console\Input;

interface InputConfigurationInterface
{
    const IS_REQUIRED = 0;

    const IS_OPTIONAL = 1;
    
    public function getName(): string;

    public function getRequired(): int;
}
