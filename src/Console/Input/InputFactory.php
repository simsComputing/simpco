<?php

namespace Simpco\Console\Input;

use Simpco\Console\CommandConfigurationInterface;

class InputFactory implements InputFactoryInterface
{
    public function create(
        CommandConfigurationInterface $commandConfiguration,
        array $argsList
    ): InputInterface {
        $argumentsMapping = $this->createArgumentsMapping($commandConfiguration, $argsList);
        $optionsMapping = $this->createOptionsMapping($commandConfiguration, $argsList);
        return new Input($argumentsMapping, $optionsMapping);
    }

    private function createOptionsMapping(CommandConfigurationInterface $configuration, array $argsList): array
    {
        $options = $configuration->getOptionsConfiguration();
        $cli = implode(' ', $argsList);
        $optionsMapping = [];
        /** @var OptionConfigurationInterface $option */
        foreach ($options as $option) {
            $matches = [];
            $regEx = '/--' . $option->getName() . '\s+(\w+){1}/';
            preg_match_all($regEx, $cli, $matches);

            if ($option->getType() === OptionConfigurationInterface::TYPE_ARRAY) {
                $optionsMapping[$option->getName()] = $matches[1];
            } else {
                $optionsMapping[$option->getName()] = isset($matches[1][0]) ? $matches[1][0] : null;
            }
        }

        return $optionsMapping;
    }

    private function createArgumentsMapping(CommandConfigurationInterface $configuration, array $argsList): array
    {
        $arguments = $configuration->getArgumentsConfiguration();
        usort($arguments, [$this, 'sortArgs']);

        $regEx = '/(';
        $regEx .= $configuration->getName();
        $regEx .= '){1}';
        
        foreach ($arguments as $argument) {
            $regEx .= '\s{1}(\w+)';
        }
        $regEx .= '/';
        $matches = [];
        preg_match($regEx, implode(' ', $argsList), $matches);

        $argumentsMapping = [];
        $index = 2;
        foreach ($arguments as $argument) {
            $argumentsMapping[$argument->getName()] = isset($matches[$index]) ? $matches[$index] : null;
            $index++;
        }

        return $argumentsMapping;
    }

    /**
     * @param ArgumentConfigurationInterface $arg1
     * @param ArgumentConfigurationInterface $arg2
     * @return integer
     */
    private function sortArgs(
        ArgumentConfigurationInterface $arg1,
        ArgumentConfigurationInterface $arg2
    ): int {
        return $arg1->getSortOrder() - $arg2->getSortOrder();
    }
}
