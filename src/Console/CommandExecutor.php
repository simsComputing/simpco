<?php

namespace Simpco\Console;

use Simpco\Console\Exception\InputException;
use Simpco\Console\Exception\MissingCommandException;
use Simpco\Console\Input\InputFactoryInterface;
use Simpco\Console\Output\OutputFactoryInterface;

class CommandExecutor implements CommandExecutorInterface
{
    private CommandPoolInterface $commandPool;

    private InputFactoryInterface $inputFactory;

    private OutputFactoryInterface $outputFactory;

    public function __construct(
        CommandPoolInterface $commandPool,
        InputFactoryInterface $inputFactory,
        OutputFactoryInterface $outputFactory
    ) {
        $this->commandPool = $commandPool;
        $this->inputFactory = $inputFactory;
        $this->outputFactory = $outputFactory;
    }

    public function run(array $argsList): void
    {
        $command = $this->getCommandForArgs($argsList);
        $input = $this->inputFactory->create($command->getConfiguration(), $argsList);
        $output = $this->outputFactory->create();
        $command->execute($input, $output);
    }

    private function getCommandForArgs(array $argsList): CommandInterface
    {
        if (!isset($argsList[1])) {
            throw new InputException('Missing command name');
        }

        $command = $this->commandPool->getCommand($argsList[1]);

        if ($command === null) {
            throw new MissingCommandException('Could not find command for name : ' . $argsList[1]);
        }

        return $command;
    }
}