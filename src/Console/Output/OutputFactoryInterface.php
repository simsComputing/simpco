<?php

namespace Simpco\Console\Output;

interface OutputFactoryInterface
{
    public function create(): OutputInterface;
}