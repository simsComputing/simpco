<?php

namespace Simpco\Console\Output;

class Output implements OutputInterface
{
    private $stderr;

    public function __construct()
    {
        $this->stderr = fopen('php://stderr', 'w');

    }

    public function write(string $message)
    {
        fwrite($this->stderr, $message . PHP_EOL);
    }

    public function __destruct()
    {
        fclose($this->stderr);
    }
}