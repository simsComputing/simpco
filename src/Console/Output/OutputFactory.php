<?php

namespace Simpco\Console\Output;

class OutputFactory implements OutputFactoryInterface
{
    public function create(): OutputInterface
    {
        return new Output();
    }
}