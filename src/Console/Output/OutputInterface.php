<?php

namespace Simpco\Console\Output;

interface OutputInterface
{
    public function write(string $message);
}