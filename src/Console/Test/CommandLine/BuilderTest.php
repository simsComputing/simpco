<?php

namespace Simpco\Console\Test\CommandLine;

use Simpco\Console\CommandLine\Builder;
use PHPUnit\Framework\TestCase;
use Simpco\Console\Exception\CommandLineException;

/**
 * @covers \Simpco\Console\CommandLine\Builder
 */
class BuilderTest extends TestCase 
{
    public function testCommandLineBuildingSimpleCommand(): void
    {
        $builder = new Builder();
        $builder->setCommandName('bin/simpco');
        $builder->addArgument('compile:javascript');
        $builder->addOption('entry-dir', 'index.js');
        $builder->addOption('output-dir', 'output.js');

        $cli = $builder->build();

        $expected = 'bin/simpco compile:javascript --entry-dir index.js --output-dir output.js ;';

        $this->assertEquals($expected, $cli);
    }

    public function testWithOptionValueNull(): void
    {
        $builder = new Builder();
        $builder->setCommandName('bin/simpco');
        $builder->addArgument('compile:javascript');
        $builder->addOption('entry-dir', null);

        $cli = $builder->build();

        $expected = 'bin/simpco compile:javascript --entry-dir ;';

        $this->assertEquals($expected, $cli);
    }

    public function testCommandLineBuildingMultipleArguments(): void
    {
        $builder = new Builder();
        $builder->setCommandName('bin/simpco');
        $builder->addArgument('after', 2);
        $builder->addArgument('compile:javascript', 1);
        $builder->addOption('entry-dir', 'index.js');
        $builder->addOption('output-dir', 'output.js');

        $cli = $builder->build();

        $expected = 'bin/simpco compile:javascript after --entry-dir index.js --output-dir output.js ;';

        $this->assertEquals($expected, $cli);
    }

    public function testExceptionIfNoCommandName(): void
    {
        $builder = new Builder();
        $this->expectException(CommandLineException::class);
        $builder->build();
    }

    public function testBuildMultiWithFailOnPrevious(): void
    {
        $builder = new Builder();

        $builder
            ->setCommandName('cd')
            ->addArgument('/root/of/computer')
            ->next(true)
            ->setCommandName('bin/simpco')
            ->addArgument('compile:javascript');

        $cli = $builder->build();

        $expected = 'cd /root/of/computer && bin/simpco compile:javascript ;';

        $this->assertEquals($expected, $cli);
    }

    public function testFailOnNextWithNoName(): void
    {
        $builder = new Builder();
        $this->expectException(CommandLineException::class);        
        $builder
            ->next(true);
    }
}
