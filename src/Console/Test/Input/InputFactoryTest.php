<?php

namespace Simpco\Console\Test\Input;
use PHPUnit\Framework\TestCase;
use Simpco\Console\CommandConfigurationInterface;
use Simpco\Console\Input\ArgumentConfigurationInterface;
use Simpco\Console\Input\InputFactory;
use Simpco\Console\Input\OptionConfigurationInterface;

/**
 * @covers \Simpco\Console\Input\InputFactory
 * @uses \Simpco\Console\Input\Input
 * @uses \Simpco\Console\Test\Input\InputFactoryTest
 */
class InputFactoryTest extends TestCase
{
    public static function getWorkingCasesDataSet(): array
    {
        return [
            self::makeDataSetForCompleteWorkingUseCase(),
            self::makeDataSetForWorkingCaseWithOnlyArguments(),
            self::makeDataSetForCompleteWorkingUseCaseWithOnlyOptions(),
            self::makeDataWithRequestNonConfiguredValues(),
            self::makeDataWithRequestConfiguredButUnsetValues()
        ];
    }

    public static function makeArgumentDataSet(string $name, int $sortOrder)
    {
        return ['sortOrder' => $sortOrder, 'argName' => $name];
    }

    public static function makeOptionDataSet(string $name, int $type)
    {
        return ['type' => $type, 'name' => $name];
    }

    public static function makeDataWithTwoArgumentsButOnlyOneIsSet(): array
    {
        return [
            [
                'arguments' => [
                    self::makeArgumentDataSet('arg1', 0),
                    self::makeArgumentDataSet('arg2', 1),
                ],
                'options' => [
                ],
                'name' => 'this:method:test'
            ],
            [
                'php',
                'bin/simpco',
                'this:method:test',
                'value2',
            ],
            ['arg1' => 'value2', 'arg2' => null],
            [],
        ];
    }

    public static function makeDataWithRequestConfiguredButUnsetValues(): array
    {
        return [
            [
                'arguments' => [
                    self::makeArgumentDataSet('arg1', 0),
                ],
                'options' => [
                    self::makeOptionDataSet('first', OptionConfigurationInterface::TYPE_STRING)
                ],
                'name' => 'this:method:test'
            ],
            [
                'php',
                'bin/simpco',
                'this:method:test'
            ],
            ['arg1' => null],
            ['first' => null],
        ];
    }

    public static function makeDataWithRequestNonConfiguredValues(): array
    {
        return [
            [
                'arguments' => [
                ],
                'options' => [
                ],
                'name' => 'this:method:test'
            ],
            [
                'php',
                'bin/simpco',
                'this:method:test',
                'arg1value',
                '--first',
                'arrayvalue1',
            ],
            ['arg1' => null],
            ['first' => null],
        ];
    }

    public static function makeDataSetForCompleteWorkingUseCase(): array
    {
        return [
            [
                'arguments' => [
                    self::makeArgumentDataSet('arg1', 0),
                    self::makeArgumentDataSet('arg2', 1),
                ],
                'options' => [
                    self::makeOptionDataSet('first', OptionConfigurationInterface::TYPE_ARRAY),
                    self::makeOptionDataSet('second', OptionConfigurationInterface::TYPE_STRING),
                ],
                'name' => 'this:method:test'
            ],
            [
                'php',
                'bin/simpco',
                'this:method:test',
                'arg1value',
                'arg2value',
                '--first',
                'arrayvalue1',
                '--second',
                'optionvalue2',
                '--first',
                'arrayvalue2'
            ],
            ['arg1' => 'arg1value', 'arg2' => 'arg2value'],
            ['first' => ['arrayvalue1', 'arrayvalue2'], 'second' => 'optionvalue2'],
        ];
    }

    public static function makeDataSetForCompleteWorkingUseCaseWithOnlyOptions(): array
    {
        return [
            [
                'arguments' => [
                ],
                'options' => [
                    self::makeOptionDataSet('first', OptionConfigurationInterface::TYPE_ARRAY),
                    self::makeOptionDataSet('second', OptionConfigurationInterface::TYPE_STRING),
                ],
                'name' => 'this:method:test'
            ],
            [
                'php',
                'bin/simpco',
                'this:method:test',
                '--first',
                'arrayvalue1',
                '--second',
                'optionvalue2',
                '--first',
                'arrayvalue2'
            ],
            [],
            ['first' => ['arrayvalue1', 'arrayvalue2'], 'second' => 'optionvalue2'],
        ];
    }

    public static function makeDataSetForWorkingCaseWithOnlyArguments()
    {
        return [
            [
                'arguments' => [
                    self::makeArgumentDataSet('arg1', 0),
                    self::makeArgumentDataSet('arg3', 2),
                    self::makeArgumentDataSet('arg2', 1),
                ],
                'options' => [],
                'name' => 'this:method:test'
            ],
            [
                'php',
                'bin/simpco',
                'this:method:test',
                'arg1value',
                'arg2value',
                'arg3value'
            ],
            ['arg1' => 'arg1value', 'arg2' => 'arg2value', 'arg3' => 'arg3value'],
            [],
        ];
    }

    private function createArgs(array $argsConfiguration)
    {
        $argsObj = [];
        foreach ($argsConfiguration as $argConfiguration) {
            $argument = $this->createStub(ArgumentConfigurationInterface::class);
            $argument->method('getSortOrder')->willReturn($argConfiguration['sortOrder']);
            $argument->method('getName')->willReturn($argConfiguration['argName']);
            $argsObj[] = $argument;
        }

        return $argsObj;
    }

    private function createOptions(array $optionsConfiguration)
    {
        $optionsObj = [];
        foreach ($optionsConfiguration as $optionConfiguration)
        {
            $option = $this->createStub(OptionConfigurationInterface::class);
            $option->method('getName')->willReturn($optionConfiguration['name']);
            $option->method('getType')->willReturn($optionConfiguration['type']);
            $optionsObj[] = $option;
        }

        return $optionsObj;
    }

    /**
     * @dataProvider getWorkingCasesDataSet
     */
    public function testInputFactoryWorkingCases($configurationData, $argsList, $expectedArgs, $expectedOptions)
    {
        $configuration = $this->createStub(CommandConfigurationInterface::class);
        $argsConfiguration = $this->createArgs($configurationData['arguments']);
        $optionsConfiguration = $this->createOptions($configurationData['options']);
        $configuration->method('getOptionsConfiguration')->willReturn($optionsConfiguration);
        $configuration->method('getArgumentsConfiguration')->willReturn($argsConfiguration);
        $configuration->method('getName')->willReturn($configurationData['name']);
        $factory = new InputFactory();
        $input = $factory->create($configuration, $argsList);

        foreach ($expectedArgs as $expectedName => $expectedValue) {
            $this->assertEquals($expectedValue, $input->getArgument($expectedName));
        }

        foreach ($expectedOptions as $expectedName => $expectedValue) {
            $this->assertEquals($expectedValue, $input->getOption($expectedName));
        }
    }
}