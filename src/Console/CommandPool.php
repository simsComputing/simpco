<?php

namespace Simpco\Console;

class CommandPool implements CommandPoolInterface
{
    private array $commands = [];

    public function __construct(array $commands)
    {
        array_map([$this, 'registerCommandIndexedByName'], $commands);
    }

    /**
     * @param CommandInterface $command
     * @return void
     */
    private function registerCommandIndexedByName(CommandInterface $command): void
    {
        $this->commands[$command->getConfiguration()->getName()] = $command;
    }

    /**
     * @param string $name
     * @return CommandInterface|null
     */
    public function getCommand(string $name): ?CommandInterface
    {
        if (!isset($this->commands[$name])) {
            return null;
        }

        return $this->commands[$name];
    }

    public function getCommands(): array
    {
        return $this->commands;
    }
}