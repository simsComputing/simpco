<?php

namespace Simpco\Console;

use Simpco\Console\Input\InputInterface;
use Simpco\Console\Output\OutputInterface;

interface CommandInterface
{
    public function execute(InputInterface $input, OutputInterface $output): void;

    /**
     * @return CommandConfigurationInterface
     */
    public function getConfiguration(): CommandConfigurationInterface;
}
