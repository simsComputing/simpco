<?php

namespace Simpco\Console;

use Simpco\Console\CommandLine\BuilderFactory;
use Simpco\Console\CommandLine\BuilderFactoryInterface;
use Simpco\Console\Input\InputFactory;
use Simpco\Console\Input\InputFactoryInterface;
use Simpco\Console\Output\OutputFactory;
use Simpco\Console\Output\OutputFactoryInterface;
use Simpco\Framework\DependencyInjection\ServiceFactory;
use Simpco\Framework\Module\EmptyModuleTrait;
use Simpco\Framework\ModuleInterface;

class Module implements ModuleInterface
{
    use EmptyModuleTrait;

    public function getServices(): array
    {
        return ServiceFactory::bashFactory(
            [
                [CommandPool::class, [], CommandPoolInterface::class],
                [CommandExecutor::class, [], CommandExecutorInterface::class],
                [InputFactory::class, [], InputFactoryInterface::class],
                [OutputFactory::class, [], OutputFactoryInterface::class],
                [BuilderFactory::class, [], BuilderFactoryInterface::class]
            ]
        );
    }
}
