<?php

namespace Simpco\Console;

class CommandConfiguration implements CommandConfigurationInterface
{
    private string $name;

    private array $argumentsConfiguration;

    private array $optionsConfiguration;

    public function __construct(
        string $name,
        array $argumentsConfiguration,
        array $optionsConfiguration
    ) {
        $this->name = $name;
        $this->argumentsConfiguration = $argumentsConfiguration;
        $this->optionsConfiguration = $optionsConfiguration;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return ArgumentConfigurationInterface[]
     */
    public function getArgumentsConfiguration(): array
    {
        return $this->argumentsConfiguration;
    }

    /**
     * @return OptionConfigurationInterface[]
     */
    public function getOptionsConfiguration(): array
    {
        return $this->optionsConfiguration;
    }
}
