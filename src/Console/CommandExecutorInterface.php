<?php

namespace Simpco\Console;

interface CommandExecutorInterface
{
    public function run(array $argsList): void;
}
