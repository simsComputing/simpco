<?php

namespace Simpco\Console;
use Simpco\Console\Exception\CommandConfigurationException;
use Simpco\Console\Input\ArgumentConfiguration;
use Simpco\Console\Input\OptionConfiguration;

abstract class AbstractCommand implements CommandInterface
{
    private ?string $name = null;

    private array $options = [];

    private array $arguments = [];

    private ?CommandConfigurationInterface $finalConfiguration = null;

    protected function setName(string $name)
    {
        if ($this->name !== null) {
            throw new CommandConfigurationException('Name is already set to ' . $this->name);
        }

        $this->name = $name;
    }

    protected function addOption(string $name, int $type, int $required)
    {
        if (isset($this->options[$name])) {
            throw new CommandConfigurationException('Option already set : ' . $name);
        }

        $option = new OptionConfiguration($name, $type, $required);

        $this->options[$name] = $option;
    }

    protected function addArgument(string $name, int $required)
    {
        if (isset($this->arguments[$name])) {
            throw new CommandConfigurationException('Argument already set : ' . $name);
        }

        $argument = new ArgumentConfiguration($name, count($this->arguments), $required);

        $this->arguments[$name] = $argument;
    }

    public function getConfiguration(): CommandConfigurationInterface
    {
        if ($this->finalConfiguration !== null) {
            return $this->finalConfiguration;
        }
        $this->configure();
        $this->finalConfiguration = new CommandConfiguration($this->name, $this->arguments, $this->options);

        return $this->finalConfiguration;
    }

    abstract protected function configure(): void;

}
