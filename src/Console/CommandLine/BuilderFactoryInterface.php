<?php

namespace Simpco\Console\CommandLine;

interface BuilderFactoryInterface
{
    public function create(): BuilderInterface;
}