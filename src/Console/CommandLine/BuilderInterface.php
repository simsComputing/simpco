<?php

namespace Simpco\Console\CommandLine;

interface BuilderInterface
{
    public function setCommandName(string $name): self;

    public function addArgument(string $argument, int $sortOrder = 100): self;

    public function addOption(string $name, string $value = null, bool $doubleDash = true): self;

    public function next(bool $breakOnPreviousFail = false): self;

    public function build(): string;
}