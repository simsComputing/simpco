<?php

namespace Simpco\Console\CommandLine;

class BuilderFactory implements BuilderFactoryInterface
{
    public function create(): BuilderInterface
    {
        return new Builder();
    }
}
