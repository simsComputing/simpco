<?php

namespace Simpco\Console\CommandLine;

use Simpco\Console\Exception\CommandLineException;

class Builder implements BuilderInterface
{
    private $commands = [];

    private ?string $name = null;

    private array $arguments = [];

    private array $options = [];

    public function setCommandName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function addArgument(string $argument, int $sortOrder = 100): self
    {
        $this->arguments[] = [
            'value' => $argument,
            'sortOrder' => $sortOrder
        ];

        usort($this->arguments, function ($a, $b) {
            return $a['sortOrder'] > $b['sortOrder'] ? 1 : -1;
        });

        return $this;
    }

    public function addOption(string $name, string $value = null, bool $doubleDash = true): self
    {
        $this->options[] = [
            'name' => $name,
            'value' => $value,
            'doubleDash' => $doubleDash
        ];

        return $this;
    }

    public function next(bool $breakOnPreviousFail = false): self
    {
        if ($this->name === null) {
            throw new CommandLineException('Missing command name');
        }

        $line = [$this->name];

        foreach ($this->arguments as $argument) {
            $line[] = $argument['value'];
        }

        foreach ($this->options as $option) {
            $dash = $option['doubleDash'] ? '--' : '-';
            $line[] = $dash . $option['name'];
            if ($option['value'] !== null) {
                $line[] = $option['value'];
            }
        }

        $endChar = ';';
        if ($breakOnPreviousFail === true) {
            $endChar = '&&';
        }

        $line[] = $endChar;

        $command = implode(' ', $line);

        $this->commands[] = $command;

        $this->name = null;
        $this->options = [];
        $this->arguments = [];

        return $this;
    }

    public function build(): string
    {
        if ($this->name !== null) {
            $this->next(false);
        }

        if (count($this->commands) === 0) {
            throw new CommandLineException('You did not defined any commands');
        }

        return implode(' ' , $this->commands);
    }
}