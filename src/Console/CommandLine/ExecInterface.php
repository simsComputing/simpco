<?php

namespace Simpco\Console\CommandLine;

interface ExecInterface
{
    public function exec(string $cli);
}