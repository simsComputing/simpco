<?php

namespace Simpco\Console\CommandLine;

class Exec implements ExecInterface
{
    public function exec(string $cli)
    {
        exec($cli);
    }    
}