<?php

namespace Simpco\Console;

interface CommandConfigurationInterface
{
    public function getName(): string;

    /**
     * @return ArgumentConfigurationInterface[]
     */
    public function getArgumentsConfiguration(): array;

    /**
     * @return OptionConfigurationInterface[]
     */
    public function getOptionsConfiguration(): array;
}
