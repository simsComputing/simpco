const config = {
    collectCoverageFrom: [
        'src/**/view/templates/js/**/*'
    ],
    coverageDirectory: './coverage-front',
    collectCoverage: true,
    coveragePathIgnorePatterns: [
        'index.js$',
        '__tests__'
    ]
}

module.exports = config;