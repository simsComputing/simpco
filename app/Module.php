<?php

namespace App;

use App\Console\JustOutputCommand;
use App\Router\RealRedirectionModifier;
use App\Router\RedirectionMappingModifier;
use App\Routes\Index;
use App\Routes\Test;
use Simpco\Console\CommandPoolInterface;
use Simpco\Framework\DependencyInjection\ParamFactory;
use Simpco\Framework\DependencyInjection\ServiceArgumentModifier\ArrayPush;
use Simpco\Framework\DependencyInjection\ServiceFactory;
use Simpco\Framework\Module\EmptyModuleTrait;
use Simpco\Framework\ModuleInterface;
use Simpco\Framework\RequestInterface;
use Simpco\Framework\RouterInterface;
use Simpco\Framework\View\HeadTag;
use Simpco\Framework\View\HeadTagPoolInterface;

class Module implements ModuleInterface
{
    use EmptyModuleTrait;

    public function getServices(): array
    {
        return ServiceFactory::bashFactory(
            [
                [Index::class],
                [Test::class]
            ]
        );
    }

    public function getParams(): array
    {
        return [
            ParamFactory::create('doctrine.config', [
                'driver' => 'pdo_mysql',
                'user' => 'root',
                'password' => 'example',
                'dbname' => 'simpco',
                'host' => 'db',
                'port' => '3306'
            ]),
            ParamFactory::create('app.root', __DIR__ . '/..'),
            ParamFactory::create('app.security.hash.secret', 'fozejfzoejfi98989jdz787ADIuh')
        ];
    }

    public function getRoutes(): array
    {
        $routes = [];
        for ($i = 0; $i < 10000; $i++) {
            $routes['/' . $i] = Index::class;
        }
        return [
            RequestInterface::GET => [
                '/' => Index::class,
                '/test' => Test::class,
                ...$routes
            ]
        ];
    }

    public function getViewDir(): ?string
    {
        return __DIR__ . '/view';
    }

    public function getServiceModifiers(): array
    {
        return [
            RouterInterface::class => [
                ArrayPush::create('routeModifiers', [
                    RedirectionMappingModifier::class,
                    RealRedirectionModifier::class
                ])
            ],
            CommandPoolInterface::class => [
                ArrayPush::create('commands', [
                    JustOutputCommand::class
                ])
            ],
            HeadTagPoolInterface::class => [
                ArrayPush::create('headTags', [
                    HeadTag::create(
                        'link', 
                        [
                            'rel' => 'stylesheet',
                            'href' => 'https://cdn.jsdelivr.net/npm/purecss@3.0.0/build/pure-min.css',
                            'integrity' => 'sha384-X38yfunGUhNzHpBaEBsWLO+A0HDYOQi8ufWDkZ0k9e0eXz/tH3II7uKZ9msv++Ls',
                            'crossorigin' => 'anonymous'
                        ], 
                        true
                    )
                ])
            ]
        ];
    }
}
