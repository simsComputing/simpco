import { simpcoPushArrayOfBlocks } from "simpco-mithril/base";
import { m } from "mithril";

const SearchComponent = () => {
  var formV = null;
  return {
    oninit: () => {},
    view: function () {
      return m("form", [
        m("input", {
          type: "text",
          placeholder: "Search...",
          onchange: (v) => (formV = v.target.value),
        }),
        m("div", formV),
      ]);
    },
  };
};

simpcoPushArrayOfBlocks(
  "app.header.right",
  "search",
  () => m(SearchComponent),
  10,
);
