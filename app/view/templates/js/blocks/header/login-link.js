import { simpcoPushArrayOfBlocks } from "simpco-mithril/base";
import { m } from "mithril";

const LoginLink = {
  view: () => m("a", { href: "#!/login" }, "Connexion"),
};

simpcoPushArrayOfBlocks(
  "app.header.right",
  "login-link",
  () => m(LoginLink),
  9,
);
