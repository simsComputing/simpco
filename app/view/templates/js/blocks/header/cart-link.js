import { simpcoPushArrayOfBlocks } from "simpco-mithril/base";
import { m } from "mithril";

const CartLink = {
  view: () => m("a", { href: "#!/cart" }, "Panier"),
};

simpcoPushArrayOfBlocks("app.header.right", "cart-link", () => m(CartLink), 8);
