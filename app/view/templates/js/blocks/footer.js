import { simpcoRegisterBlock } from "simpco-mithril/base";
import { m } from "mithril";

const footer = ({}) => {
  return m("div", [m("div", { class: "footer" }, "This is my footer")]);
};

simpcoRegisterBlock("app.footer", footer);
