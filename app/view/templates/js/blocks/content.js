import { simpcoRegisterBlock } from 'simpco-mithril/base';
import { m } from 'mithril';

const content = ({}) => {
  return m('div', [m('div', { class: 'content' }, 'This is my content')]);
};

simpcoRegisterBlock('app.content', content);
