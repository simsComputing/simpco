import {
  simpcoRegisterBlock,
  simpcoGetArrayOfBlocks,
} from "simpco-mithril/base";
import { display } from "simpco-mithril/utils";
import { m } from "mithril";
import "./header.scss";

const HeaderComponent = (node) => {
  const headerRight = simpcoGetArrayOfBlocks("app.header.right").map(
    (block) => {
      return m(
        "div",
        { class: "pure-u pure-u-auto", style: { padding: "1rem" } },
        [display(block)],
      );
    },
  );

  return m("div", [
    m("div", { class: "header pure-g", style: { height: "8rem" } }, [
      m("div", { class: "pure-u-1-5 text-center relative" }, [
        m("div", { class: "vertical-center" }, node.websiteName),
      ]),
      m("div", { class: "pure-u-4-5 fh relative" }, [
        m(
          "div",
          { class: "pure-g-align-center pure-g pure-g-right fh" },
          headerRight,
        ),
      ]),
    ]),
  ]);
};

simpcoRegisterBlock("app.header", HeaderComponent);
