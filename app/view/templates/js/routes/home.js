import {
  simpcoDisplayBlock,
  simpcoRegisterBlock,
  simpcoRegisterRoute,
} from "simpco-mithril/base";
import { m } from "mithril";

const home = {
  view: function (vnode) {
    return m("div", [
      simpcoDisplayBlock("app.header", { websiteName: vnode.attrs.website }),
      m(
        "div",
        "This website contains " + vnode.attrs.modulesCount + " modules",
      ),
      m("a", { href: "/test" }, "Click"),
      simpcoDisplayBlock("app.content"),
      simpcoDisplayBlock("app.footer"),
    ]);
  },
};

simpcoRegisterRoute("/", "app.home");
simpcoRegisterBlock("app.home", home);
