<?php

namespace App\Routes;

use Simpco\MithrilIntegration\Route\MithrilRoute;

class Test extends MithrilRoute
{
    protected function getData(): array
    {
        return ['callee' => 'baby !'];
    }
}