<?php

namespace App\Routes;

use Simpco\Framework\Module\Registry;
use Simpco\Framework\RequestInterface;
use Simpco\Framework\ResponseInterface;
use Simpco\Framework\ViewFactory;
use Simpco\MithrilIntegration\Route\MithrilRoute;

class Index extends MithrilRoute
{
    private Registry $registry;

    /**
     * @param RequestInterface $request
     * @param ViewFactory $viewFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        RequestInterface $request,
        ViewFactory $viewFactory,
        ResponseInterface $response,
        Registry $registry
    ) {
        parent::__construct(
            $request,
            $viewFactory,
            $response
        );

        $this->registry = $registry;
    }

    protected function getData(): array
    {
        return [
            'website' => 
            'Simpco Official Website',
            'modulesCount' => count($this->registry->getList())
        ];
    }
}
