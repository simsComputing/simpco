<?php

namespace App\Router;
use Simpco\Framework\Router\RouteModifierInterface;

class RedirectionMappingModifier implements RouteModifierInterface
{
    const REDIRECTION_MAPPING = [
        '/pourquoi/pas' => '/test'
    ];

    public function modify(string $requestedPath): ?string
    {
        if (!isset(self::REDIRECTION_MAPPING[$requestedPath])) {
            return null;
        }

        return self::REDIRECTION_MAPPING[$requestedPath];
    }
}
