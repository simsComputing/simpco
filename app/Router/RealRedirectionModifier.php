<?php

namespace App\Router;
use Simpco\Framework\Exception\RedirectionException;
use Simpco\Framework\Router\RouteModifierInterface;

class RealRedirectionModifier implements RouteModifierInterface
{
    const REDIRECTION_MAPPING = [
        '/pourquoi/pas/lol' => '/test'
    ];

    public function modify(string $requestedPath): ?string
    {
        if (!isset(self::REDIRECTION_MAPPING[$requestedPath])) {
            return null;
        }

        throw new RedirectionException(self::REDIRECTION_MAPPING[$requestedPath]);

    }
}
