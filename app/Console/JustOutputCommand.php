<?php

namespace App\Console;

use Simpco\Console\AbstractCommand;
use Simpco\Console\Input\ArgumentConfigurationInterface;

class JustOutputCommand extends AbstractCommand
{
    protected function configure(): void
    {
        $this->setName('just:output:command');
        $this->addArgument('try', ArgumentConfigurationInterface::IS_OPTIONAL);
    }

    public function execute(\Simpco\Console\Input\InputInterface $input, \Simpco\Console\Output\OutputInterface $output): void
    {
        $output->write('Voici la valeur en input : ');
        sleep(10);
        $output->write($input->getArgument('try'));
    }
}