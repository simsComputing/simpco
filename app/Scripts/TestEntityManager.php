<?php

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Console\EntityManagerProvider\SingleManagerProvider;
use Simpco\Framework\Bootstrap;

require_once __DIR__ . '/../../vendor/autoload.php';

$app = Bootstrap::http(__DIR__ . '/../../html/modules.php');

$app->init();

/** @var EntityManagerInterface $em */
$em = $app->getDi()->getService(EntityManagerInterface::class);

ConsoleRunner::run(
    new SingleManagerProvider($em),
    []
);
