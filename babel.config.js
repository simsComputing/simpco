/** @type {import('jest').Config} */
module.exports = {
  presets: [["@babel/preset-env", { targets: { node: "current" } }]],
};
