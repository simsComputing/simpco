<?php

use App\Module;
use Simpco\Framework\FrameworkModule;
use Simpco\MithrilIntegration\Module as MithrilModule;
use Simpco\Console\Module as ConsoleModule;
use Simpco\Admin\Module as AdminModule;
use Simpco\AdminConfiguration\Module as AdminConfigurationModule;
use Simpco\Form\Module as FormModule;
use Simpco\Grid\Module as GridModule;
use Simpco\StoryBook\Module as StoryBookModule;

return [
    new FrameworkModule(),
    new ConsoleModule(),
    new MithrilModule(),
    new Module(),
    new StoryBookModule(),
    new AdminModule(),
    new AdminConfigurationModule(),
    new FormModule(),
    new GridModule(),
];
