<?php

use Simpco\Framework\Bootstrap;

require '../vendor/autoload.php';

Bootstrap::http(__DIR__ . '/modules.php')
    ->init()
    ->run()
    ->send();
